package ru.brainworm.factory.widget.table.client.pager.factory;

import com.google.gwt.inject.client.GinModules;
import ru.brainworm.factory.generator.injector.client.FactoryInjector;

/**
 *
 */
@GinModules( PagerClientModule.class )
public interface PagerClientFactory extends FactoryInjector, AbstractPagerFactory {
}
