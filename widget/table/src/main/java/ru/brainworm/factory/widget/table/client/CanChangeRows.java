package ru.brainworm.factory.widget.table.client;

/**
 * Интерфейс, который должен реализовывать ColumnHeader, если он хочет изменять
 * значения в строках таблицы
 *
 * T - тип значения, которое ColumnHeader будет передавать в ColumnValue
 */
public interface CanChangeRows<T> {

    public static interface RowChangeHandler<T> {
        void changeAllRows( T changeValue );
    }

    public void setRowChangeHandler( RowChangeHandler<T> rowChangeHandler );
}
