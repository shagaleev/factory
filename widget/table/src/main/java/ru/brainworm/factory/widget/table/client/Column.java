package ru.brainworm.factory.widget.table.client;

import javax.validation.constraints.NotNull;

/**
 * Автор :          dima
 * Дата создания :  25.05.15 17:12
 */
public class Column implements AbstractColumn, Comparable<Column> {

    Column( @NotNull TableWidget tableWidget, int columnIndex ) {
        this.columnIndex = columnIndex;
        this.table = tableWidget;
    }

    @Override
    public void setVisibility( boolean visible ) {
        setColumnVisibility( visible );
        if ( table != null ) {
            table.refresh();
            table.resetHorizontalScroll();
        }
    }

    @Override
    public void setColumnVisibility( boolean visible ) {
        if ( table != null ) {
            table.setColumnVisible( columnIndex, visible );
        }
    }

    public boolean isVisible() {
        return table.isColumnVisible( columnIndex );
    }

    @Override
    public boolean equals( Object o ) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;

        Column column = (Column) o;

        if ( columnIndex != column.columnIndex ) return false;
        if ( !table.equals( column.table ) ) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = columnIndex;
        result = 31 * result + table.hashCode();
        return result;
    }

    @Override
    public int compareTo( Column o ) {
        return compare( this.columnIndex, o.columnIndex );
    }

    // copy-paste from JDK 1.7
    public int compare( int x, int y ) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }

    private int columnIndex;
    private TableWidget table;
}
