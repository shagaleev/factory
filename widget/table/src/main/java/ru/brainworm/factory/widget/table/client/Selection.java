package ru.brainworm.factory.widget.table.client;


import java.util.Set;

public interface Selection {
    /**
     * Интерфейс, который должен реализовывать ColumnHeader, если он хочет выделять строки таблицы
     */
    public interface CanSelectAllRows {

        public static interface SelectAllRowsHandler<T> {
            void setAllRowsSelected( boolean value );
            boolean isAllRowsSelected( Set<T> selected );
        }

        public void setSelectAllRowsHandler( SelectAllRowsHandler selectAllRowsHandler );
    }

    /**
     * Интерфейс, который должен реализовывать ColumnValue, если он хочет выделять строку таблицы
     */
    public interface CanSelectRow<T> {

        public static interface SelectRowHandler<T> {
            void setRowSelected( T row, boolean value );
        }

        public void setSelectRowHandler( SelectRowHandler<T> selectRowHandler );
    }
}
