package ru.brainworm.factory.widget.table.client.columns;

import com.google.gwt.user.client.ui.IsWidget;
import ru.brainworm.factory.widget.table.client.ColumnHeader;
import ru.brainworm.factory.widget.table.client.ColumnValue;
import ru.brainworm.factory.widget.table.client.helper.AbstractColumnHandler;

/**
 * Автор :          dima
 * Дата создания :  22.05.15 17:20
 *
 * Абстракция виджета столбца таблицы.
 */
public interface AbstractColumnWidget<T> extends IsWidget {

    /**
     * Задает имя столбца.
     */
    void setColumnName( String name );

    /**
     * Возвращает имя столбца.
     */
    String getColumnName();

    ColumnHeader getColumnHeader();

    ColumnValue<T> getColumnValues();

    /**
     * Задает имя поля класса, которое отображает данный столбец.
     */
    void setFieldName( String fieldName );

    /**
     * Возвращает имя поля класса, которое отображает данный столбец.
     */
    String getFieldName();

    /**
     * Задает видимость столбца по умолчанию (показывается ли при первом отображении таблицы).
     */
    void setVisibleByDefault( boolean visibleByDefault );

    /**
     * Возвращает видимость столбца по умолчанию (показывается ли при первом отображении таблицы).
     */
    Boolean isVisibleByDefault();

    /**
     * Задает обработчик столбца.
     */
    void setHandler( AbstractColumnHandler<T> handler );

}
