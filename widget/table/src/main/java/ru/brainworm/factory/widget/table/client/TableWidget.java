package ru.brainworm.factory.widget.table.client;

import com.google.gwt.dom.client.BrowserEvents;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.TableCellElement;
import com.google.gwt.dom.client.TableRowElement;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.*;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.HandlerRegistration;
import ru.brainworm.factory.widget.table.client.columns.AbstractColumnWidget;
import ru.brainworm.factory.widget.table.client.internal.RowInfo;

import java.util.*;

/**
 * Виджет таблицы
 */
public class TableWidget<T> extends ComplexPanel implements Window.ScrollHandler {

    public TableWidget() {
        scrollContainer = DOM.createDiv();
        scrollLeft = DOM.createDiv();
        scrollRight = DOM.createDiv();
        tableContainer = DOM.createDiv();
        table = DOM.createTable();

        setElement( scrollContainer );
        DOM.appendChild( scrollContainer, scrollLeft );
        DOM.appendChild( scrollContainer, scrollRight );
        DOM.appendChild( scrollContainer, tableContainer );
        DOM.appendChild( tableContainer, table );

        scrollContainer.setClassName( "table-widget-container" );
        scrollLeft.setClassName( "scroller left" );
        scrollRight.setClassName( "scroller right" );
        tableContainer.setClassName( "table-container" );
        table.setClassName( "table" );

        body = DOM.createElement( "tbody" );
        Event.sinkEvents( body, Event.ONCLICK
                        | Event.ONDBLCLICK
                        | Event.ONCHANGE
                        | Event.ONKEYUP
                        | Event.ONMOUSEOUT
        );
        DOM.sinkBitlessEvent( body, BrowserEvents.DRAGSTART );
        DOM.sinkBitlessEvent( body, BrowserEvents.DRAGENTER );
        DOM.sinkBitlessEvent( body, BrowserEvents.DRAGLEAVE );
        DOM.sinkBitlessEvent( body, BrowserEvents.DRAGEND );
        DOM.sinkBitlessEvent( body, BrowserEvents.DROP );

        Event.setEventListener( body, new EventListener() {
            @Override
            public void onBrowserEvent( Event event ) {
                onBodyEvent( event );
            }
        } );
        DOM.appendChild( table, body );
    }

    public void add(Widget child) {
        tryToAddColumn( child );
    }

    public void add(IsWidget child) {
        tryToAddColumn( child );
    }

    private void tryToAddColumn( IsWidget child ) {
        boolean isInstanceOf = child instanceof AbstractColumnWidget;
        if ( !isInstanceOf ) {
            return;
        }

        AbstractColumnWidget columnWidget = (AbstractColumnWidget) child;
        addColumn( columnWidget.getColumnHeader(), columnWidget.getColumnValues(), columnWidget.isVisibleByDefault() );
    }

    public List<AbstractColumn> getColumnList() {
        return columnList;
    }

    public int getRowCount() {
        return hasDetailsRow ? rows.size()/2 : rows.size();
    }

    public List<String> getHeaderValues() {
        return headerValues;
    }

    public List<List<String>> getRowsValues() {
        return rowsValues;
    }

    public void setHeaderStyle( String headerStyle ) {
        this.headerStyle = headerStyle;
    }

    public void setRowSelectionStyle( String rowSelectionStyle ) {
        this.rowSelectionStyle = rowSelectionStyle;
    }

    public void setEndOfTableHandler( EndOfTableHandler endOfTableHandler ) {
        this.endOfTableHandler = endOfTableHandler;
    }

    /**
     * Sets possibility of dragging rows.
     *
     * @param draggableRows true, if dragging is needed; false by default.
     */
    public void setDraggableRows( boolean draggableRows ) {
        this.draggableRows = draggableRows;
    }

    /**
     * Устанавливает идентификатор на элемент table
     */
    public void setEnsureDebugId( String debugId ) {
        table.setId( "gwt-debug-" + debugId );
    }

    /**
     * Добавляет возможность горизонтального скролла, в случае, если таблица не умещается на страницу
     * @param horizontalScrollable
     */
    public void setHorizontalScroll( boolean horizontalScrollable ) {
        this.horizontalScrollable = horizontalScrollable;
        updateHorizontalScrollability();
    }

    /**
     * Сбрасывает позицию горизонтального скоролла
     */
    public void resetHorizontalScroll() {
        table.getStyle().clearRight();
        updateHorizontalScrollers();
    }

    @Override
    public void onWindowScroll( Window.ScrollEvent event ) {
        Integer tableEnd = getAbsoluteTop() + getOffsetHeight();
        Integer currentEnd = Window.getClientHeight() + event.getScrollTop();

        if ( tableEnd - currentEnd < 50 ) {
            if ( endOfTableHandler != null ) {
                endOfTableHandler.onEndOfTable();
            }
        }
    }

    public AbstractColumn addColumn( ColumnHeader columnHeader, ColumnValue<T> columnValue ) {
        return addColumn( columnHeader, columnValue, true );
    }

    /**
     * Добавляет столбец в таблицу.
     *
     * @param columnHeader      заголовок столбца
     * @param columnValue       абстракция ячейки со значением
     * @param visibleByDefault  видимость столбца по умолчанию
     */
    public AbstractColumn addColumn( ColumnHeader columnHeader, ColumnValue<T> columnValue, Boolean visibleByDefault ) {
        if ( columnHeader != null ) {
            if ( rowHeader == null ) {
                createRowHeader();
            }

            if ( columnValue instanceof IsDetailsRow ) {
                hasDetailsRow = true;
            } else {
                Element element = DOM.createElement( "th" );
                setElementVisibility( element, visibleByDefault );
                rowHeader.appendChild( element );

                columnHeaders.add( columnHeader );
                columnHeader.fillHeader( element );
                headerValues.add( element.getInnerText() );
            }

            if ( columnHeader instanceof CanChangeRows && columnValue instanceof CanAcceptChanges ) {
                CanChangeRows changer = (CanChangeRows) columnHeader;
                final CanAcceptChanges acceptor = (CanAcceptChanges) columnValue;
                final Integer colNumber = hasDetailsRow ? columnValues.size() - 1 : columnValues.size();

                changer.setRowChangeHandler( new CanChangeRows.RowChangeHandler() {
                    @Override
                    public void changeAllRows( Object changeValue ) {
                        List<Map.Entry<Element, T>> entries = new ArrayList<Map.Entry<Element, T>>( rows.entrySet() );
                        for ( Map.Entry<Element, T> entry : entries ) {
                            if ( entry.getValue() == null ) {
                                continue;
                            }

                            if ( hasDetailsRow && entries.indexOf( entry ) % 2 - 1 == 0 ) {
                                continue;
                            }

                            Element element = entry.getKey().getFirstChildElement().cast();
                            for ( int i = 0; i < colNumber; ++i ) {
                                element = element.getNextSiblingElement().cast();
                            }
                            acceptor.performChanges( changeValue, entry.getValue(), element );
                        }
                    }
                } );
            }

                if ( columnHeader instanceof Selection.CanSelectAllRows ) {
                Selection.CanSelectAllRows selector = (Selection.CanSelectAllRows) columnHeader;
                selector.setSelectAllRowsHandler( new Selection.CanSelectAllRows.SelectAllRowsHandler<T>() {
                    @Override
                    public void setAllRowsSelected( boolean value ) {
                        List<Element> elements = new ArrayList<Element>( rows.keySet() );
                        for ( Element row : elements ) {
                            if ( hasDetailsRow && elements.indexOf( row ) % 2 - 1 == 0 ) {
                                continue;
                            }
                            setRowSelection( row, value );
                        }
                    }

                    @Override
                    public boolean isAllRowsSelected(Set<T> selected) {
                        for (T row : rows.values()) {
                            if (row != null && !selected.contains( row )) {
                                return false;
                            }
                        }
                        return true;
                    }
                } );
            }

            if ( columnValue instanceof Selection.CanSelectRow ) {
                Selection.CanSelectRow selector = (Selection.CanSelectRow) columnValue;
                selector.setSelectRowHandler( new Selection.CanSelectRow.SelectRowHandler() {
                    @Override
                    public void setRowSelected( Object row, boolean value ) {
                        List<Map.Entry<Element, T>> entries = new ArrayList<Map.Entry<Element, T>>( rows.entrySet() );
                        for ( Map.Entry<Element, T> entry : entries ) {
                            if ( entry.getValue() == null ) {
                                continue;
                            }
                            // todo
                            if ( entry.getValue().equals( row ) ) {
                                setRowSelection( entry.getKey(), value );
                            }
                        }
                    }
                } );
            }


            if ( columnValue instanceof NeedLevelProvider ) {
                NeedLevelProvider<T> acceptor = (NeedLevelProvider<T>) columnValue;
                acceptor.setLevelProvider( new NeedLevelProvider.LevelProvider<T>() {
                    @Override
                    public int getLevel( T row ) {
                        RowInfo info = rowInfoMap.get( row );
                        return info == null ? 0 : info.level;
                    }
                } );
            }

            if ( columnValue instanceof NeedRowRemove ) {
                NeedRowRemove<T> rowRemoveHandler = (NeedRowRemove<T>) columnValue;
                rowRemoveHandlers.add( rowRemoveHandler );
            }

            if ( draggableRows && columnValue instanceof CanDragRow ) {
                canDragStartColumnIndex = columnValues.size();
            }


        }

        columnValues.add( columnValue );
        columnsVisibility.put( lastColumnIndex, visibleByDefault );

        AbstractColumn column = new Column( this, lastColumnIndex++ );

        columnList.add( column );

        return column;
    }

    public void clearRows() {
        for ( Element element : rows.keySet() ) {
            DOM.removeChild( body, element );
        }
        rows.clear();
        values.clear();
        rowInfoMap.clear();
        rowsValues.clear();
    }

    public void clearColumns() {
        clearRows();

        columnList.clear();
        columnHeaders.clear();
        columnValues.clear();
        headerValues.clear();
        columnsVisibility.clear();
        lastColumnIndex = 0;

        if ( rowHeader != null ) {
            rowHeader.removeAllChildren();
        }
        if ( body != null ) {
            body.removeAllChildren();
        }
    }

    public void refresh() {
        List<T> saveRows = new ArrayList<T>( values );
        clearRows();
        for ( T saveRow : saveRows ) {
            addRow( saveRow );
        }
    }

    public Element addRow( T row ) {
        return addRow( row, null );
    }

    public Element addRow( T row, String styleName ) {
        Element elemRow = DOM.createElement( "tr" );
        if (styleName != null) {
            elemRow.addClassName(styleName);
        }
        Element detailsElemRow = null;

        if ( draggableRows ) {
            elemRow.setDraggable( Element.DRAGGABLE_TRUE );
        }
        DOM.appendChild( body, elemRow );
        rows.put( elemRow, row );
        values.add( row );

        if ( hasDetailsRow ) {
            detailsElemRow = DOM.createElement( "tr" );
            DOM.insertBefore( body, detailsElemRow, DOM.getNextSibling( elemRow ) );
            rows.put( detailsElemRow, row );
        }

        fillRow( elemRow, detailsElemRow, row );
        return elemRow;
    }


    public void addChildRow( T parentRow, T childRow ) {
        Element elemRow = DOM.createElement( "tr" );
        Element parentElement = null;
        for ( Map.Entry<Element, T> rowEntry : rows.entrySet() ) {
            if ( rowEntry.getValue() == null ) {
                continue;
            }

            if ( rowEntry.getValue() == parentRow ) {
                parentElement = rowEntry.getKey();
                break;
            }
        }
        if ( parentElement == null ) {
            addRow( childRow );
            return;
        }

        RowInfo parentInfo = rowInfoMap.get( parentRow );
        if ( parentInfo == null ) {
            parentInfo = new RowInfo();
            rowInfoMap.put( parentRow, parentInfo );
        }
        parentInfo.children.put( childRow, elemRow );

        for ( int i = 0; i < parentInfo.getChildCount(); ++i ) {
            parentElement = DOM.getNextSibling( parentElement );
        }
        DOM.insertBefore( body, elemRow, parentElement );

        int parentPos = values.indexOf( parentRow );
        values.add( parentPos + parentInfo.getChildCount(), childRow );

        rows.put( elemRow, childRow );
//        values.add( childRow );

        RowInfo childInfo = new RowInfo();
        childInfo.level = parentInfo.level + 1;
        rowInfoMap.put( childRow, childInfo );

        fillRow( elemRow, null, childRow );
    }

    public void removeRow( T row ) {
        // try to locate <tr> element of row to be removed
        boolean hasRow = values.remove( row );
        List<Element> rowElements = new ArrayList< Element >();
        if ( hasRow ) {
            for ( Map.Entry<Element, T> rowEntry : rows.entrySet() ) {
                if ( rowEntry.getValue() == null ) {
                    continue;
                }

                if ( rowEntry.getValue().equals( row ) ) {
                    rowElements.add( rowEntry.getKey() );
                    break;
                }
            }
        }

        // try to find all parents, that may contain this row
        for ( Map.Entry<T, RowInfo> tRowInfoEntry : rowInfoMap.entrySet() ) {
            RowInfo<T> tRowInfo = tRowInfoEntry.getValue();
            if ( tRowInfo.children == null ) {
                continue;
            }

            tRowInfo.children.remove( row );
        }

        // try to remove all possible children
        RowInfo<T> rowInfo = rowInfoMap.remove( row );
        if ( rowInfo != null ) {
            for ( Map.Entry<T, Element> childEntry : rowInfo.children.entrySet() ) {
                removeRow( childEntry.getKey() );
            }
        }

        // remove element from DOM
        for ( Element rowElement : rowElements ) {
            if ( rowElement == null ) {
                return;
            }

            rows.remove( rowElement );
            DOM.removeChild( body, rowElement );
            rowElement.removeFromParent();
        }

        for ( NeedRowRemove<T> rowRemoveHandler : rowRemoveHandlers ) {
            rowRemoveHandler.onRowRemoved( row );
        }
    }

    public void removeChildren( T row ) {
        RowInfo<T> rowInfo = rowInfoMap.get( row );
        if ( rowInfo != null ) {
            ArrayList<T> children = new ArrayList<T>( rowInfo.children.keySet() );
            for ( T child : children ) {
                removeRow( child );
            }
            rowInfo.children.clear();
        }
    }

    public void updateRow( T row ) {
        List<Element> rowElements = new ArrayList< Element >();
        for ( Map.Entry<Element, T> entry : rows.entrySet() ) {
            if ( entry.getValue() == null ) {
                continue;
            }

            if ( !entry.getValue().equals( row ) ) {
                continue;
            }

            Element rowElement = entry.getKey();
            if ( rowElement == null ) {
                return;
            }

            rowElement.removeAllChildren();

            rows.put( rowElement, row );
            rowElements.add( rowElement );
        }

        prepareToFillRow( row, rowElements );
    }

    public void updateRow(T row, Comparator comparator) {

        if (comparator == null){
            return;
        }

        List<Element> rowElements = new ArrayList< Element >();
        for ( Map.Entry<Element, T> entry : rows.entrySet() ) {
            if ( entry.getValue() == null ) {
                continue;
            }

            if ( comparator.compare(entry.getValue(), row) != 0 ) {
                continue;
            }

            Element rowElement = entry.getKey();
            if ( rowElement == null ) {
                return;
            }

            rowElement.removeAllChildren();

            rows.put( rowElement, row );
            rowElements.add( rowElement );
        }

        prepareToFillRow( row, rowElements );
    }

    private void prepareToFillRow(T row, List<Element> rowElements){
        int size = rowElements.size();
        if ( size == 0 ) {
            return;
        }

        fillRow( rowElements.get( 0 ), size > 1 ? rowElements.get( 1 ) : null, row );

        int pos = values.indexOf( row );
        if ( pos >= 0 && pos < values.size() ) {
            values.remove( pos );
            values.add( pos, row );
        }
    }

    public int getRowOffsetTop( T value ) {
        List<Element> rowElements = new ArrayList<Element>();
        for ( Map.Entry<Element, T> entry : rows.entrySet() ) {
            if ( entry.getValue() == null ) {
                continue;
            }

            if ( entry.getValue().equals( value ) ) {
                rowElements.add( entry.getKey() );
            }
        }
        if ( rowElements.isEmpty() ) {
            return -1;
        }

        return rowElements.get( 0 ).getOffsetTop();
    }

    public boolean isColumnVisible( int column ) {
        Boolean value = columnsVisibility.get( column );

        return value != null && value;
    }

    public void addCustomRow( Element element, String cellStyleName, String rowStyleName ) {
        Element elemRow = DOM.createElement( "tr" );
        if ( rowStyleName != null ) {
            elemRow.setClassName( rowStyleName );
        }

        if ( draggableRows ) {
            elemRow.setDraggable( Element.DRAGGABLE_TRUE );
        }
        DOM.appendChild( body, elemRow );
        rows.put( elemRow, null );

        Element cell = DOM.createElement( "td" );
        if ( cellStyleName != null ) {
            cell.setClassName( cellStyleName );
        }
        cell.setAttribute( "colspan", String.valueOf( columnValues.size() ) );
        DOM.appendChild( elemRow, cell );

        DOM.appendChild( cell, element );
    }

    public void addRowStyle( T value, String styleName ) {
        Element element = findRowElementByValue( value );
        addStyleToElement( element, styleName );
    }

    public void removeRowStyle( T value, String styleName ) {
        Element element = findRowElementByValue( value );
        removeStyleFromElement( element, styleName );
    }

    @Override
    protected void onLoad() {
        scrollRegistration = Window.addWindowScrollHandler( this );

        if ( horizontalScrollable ) {
            resizeRegistration = Window.addResizeHandler( new ResizeHandler() {
                @Override
                public void onResize( ResizeEvent resizeEvent ) {
                    updateHorizontalScrollers();
                }
            } );

            updateHorizontalScrollers();
        }
    }

    @Override
    protected void onUnload() {
        scrollRegistration.removeHandler();

        if ( horizontalScrollable && resizeRegistration != null ) {
            resizeRegistration.removeHandler();
        }
    }

    protected void setColumnVisible( int column, boolean visible ) {
        columnsVisibility.put( column, visible );

        Element columnHeader = rowHeader.getChild( column ).cast();
        setElementVisibility( columnHeader, visible );
    }

    private void createRowHeader() {
        Element header = DOM.createElement( "thead" );
        header.setClassName( headerStyle );
        Event.sinkEvents( header, Event.ONCLICK | Event.ONCHANGE );
        Event.setEventListener( header, new EventListener() {
            @Override
            public void onBrowserEvent( Event event ) {
                onHeaderEvent( event );
            }
        } );
        DOM.insertBefore( table, header, body );

        rowHeader = DOM.createElement( "tr" );
        DOM.appendChild( header, rowHeader );
    }

    protected void fillRow( Element mainRow, Element detailsRow, T value ) {
        List<String> rowValues = new ArrayList<String>();
        for ( int i = 0; i < columnValues.size(); i++ ) {
            ColumnValue<T> columnValue = columnValues.get( i );

            Element cell = DOM.createElement( "td" );
            setElementVisibility( cell, isColumnVisible( i ) );

            if ( columnValue instanceof IsDetailsRow ) {
                if ( detailsRow != null && ( (IsDetailsRow) columnValue ).isExpanded( value ) ) {
                    cell.setAttribute( "colspan", String.valueOf( columnValues.size() - 1 ) );
                    DOM.appendChild( detailsRow, cell );
                    columnValue.fillValue( cell, value );
                }
            } else {
                DOM.appendChild( mainRow, cell );
                columnValue.fillValue( cell, value );
            }
            rowValues.add( cell.getInnerText() );
        }
        rowsValues.add( rowValues );
    }

    private void onHeaderEvent( Event event ) {
        Element targetElement = event.getEventTarget().cast();
        TableCellElement cell = findParent( targetElement ).cast();
        if ( cell == null ) {
            return;
        }

        columnHeaders.get( cell.getCellIndex() ).handleEvent( event );
    }

    protected void onBodyEvent( Event event ) {
        Element targetElement = event.getEventTarget().cast();
        Element cellOrRow = findParent( targetElement );

        TableRowElement row = null;
        TableCellElement cell = null;
        int cellIndex = canDragStartColumnIndex;
        if ( cellOrRow != null && cellOrRow.getTagName().equalsIgnoreCase( "tr" ) ) {
            row = cellOrRow.cast();
        } else {
            cell = findParent( targetElement ).cast();
            if ( cell == null ) {
                return;
            }
            row = cell.getParentElement().cast();
            cellIndex = cell.getCellIndex();
        }

        if ( draggableRows && ( event.getType().startsWith( "drag" ) || event.getType().startsWith( "drop" ) ) && canDragStartColumnIndex != -1 ) {
            cellIndex = canDragStartColumnIndex;
        }

        if ( cellIndex < 0 ) {
            return;
        }

        ColumnValue<T> columnValue = columnValues.get( cellIndex );
        if ( columnValue == null ) {
            return;
        }

        T value = rows.get( row );
        if ( value != null ) {
            columnValue.handleEvent( event, value );
        }
    }

    protected int getRowIndex( TableRowElement row ) {
        int index = row.getRowIndex();
        if ( hasDetailsRow ) {
            index = index/2 + index%2;
        }
        return index;
    }

    protected Element findParent( Element elem ) {
        while ( ( elem != null ) && ( elem != table ) ) {
            String tagName = elem.getTagName();
            if ( "td".equalsIgnoreCase( tagName ) || "th".equalsIgnoreCase( tagName ) || "tr".equalsIgnoreCase( tagName ) ) {
                return elem.cast();
            }
            elem = elem.getParentElement().cast();
        }
        return null;
    }

    private void setRowSelection( Element row, boolean value ) {
        if ( value ) {
            row.addClassName( rowSelectionStyle );
            return;
        }
        row.removeClassName( rowSelectionStyle );
    }

    private void setElementVisibility( Element element, boolean visible ) {
        Style style = element.getStyle();
        if ( visible ) {
            style.clearDisplay();
        } else {
            style.setDisplay( Style.Display.NONE );
        }
    }


    /** ===== Horizontal scroll feature ===== **/

    private void updateHorizontalScrollability() {
        if ( horizontalScrollable ) {
            initHorizontalScroll();
        } else {
            resetScrollMode();
        }
    }

    private void resetScrollMode() {
        scrollContainer.removeClassName( "scrollable" );
        resetHorizontalScroll();
    }

    private void initHorizontalScroll() {
        scrollContainer.addClassName( "scrollable" );

        Event.sinkEvents( scrollLeft, Event.ONMOUSEOVER | Event.ONMOUSEOUT );
        Event.sinkEvents( scrollRight, Event.ONMOUSEOVER | Event.ONMOUSEOUT );
        DOM.sinkBitlessEvent( scrollLeft, BrowserEvents.MOUSEOVER );
        DOM.sinkBitlessEvent( scrollLeft, BrowserEvents.MOUSEOUT );
        DOM.sinkBitlessEvent( scrollRight, BrowserEvents.MOUSEOVER );
        DOM.sinkBitlessEvent( scrollRight, BrowserEvents.MOUSEOUT );
        Event.setEventListener( scrollLeft, new EventListener() {
            @Override
            public void onBrowserEvent( Event event ) {
                onScrollLeftEvent( event );
            }
        } );
        Event.setEventListener( scrollRight, new EventListener() {
            @Override
            public void onBrowserEvent( Event event ) {
                onScrollRightEvent( event );
            }
        } );

        resetHorizontalScroll();
    }

    private void updateHorizontalScrollers() {
        updateScrollLeft();
        updateScrollRight();
    }

    private void updateTableScroll( int signum ) {
        int sign = (int) Math.signum( signum );

        if (sign == 0) {
            return;
        }

        Style style = table.getStyle();

        int scrollPosition = getScrollPosition();
        int border = sign < 0 ? 0 : getRightBorder();

        if ( sign * scrollPosition < border ) {
            style.setRight( scrollPosition + sign * SCROLL_STEP, Style.Unit.PX );
        } else if ( sign * scrollPosition >= border ) {
            style.setRight( border, Style.Unit.PX );
        }
    }

    private void updateScrollLeft() {
        int scrollPosition = getScrollPosition();

        if ( scrollPosition <= 0 || !horizontalScrollable ) {
            scrollLeft.removeClassName( "visible" );
        } else if ( scrollPosition > 0) {
            scrollLeft.addClassName( "visible" );
        }
    }

    private void updateScrollRight() {
        int scrollPosition = getScrollPosition();
        int rightBorder = getRightBorder();

        if ( scrollPosition >= rightBorder || !horizontalScrollable ) {
            scrollRight.removeClassName( "visible" );
        } else if ( scrollPosition < rightBorder ) {
            scrollRight.addClassName( "visible" );
        }
    }

    private void onScrollLeftEvent( Event event ) {
        if ( !horizontalScrollable || event.getTypeInt() == Event.ONMOUSEOUT ) {
            scrollLeftTimer.cancel();
            return;
        }
        scrollLeftTimer.scheduleRepeating( SCROLL_DELAY );
    }

    private void onScrollRightEvent( Event event ) {
        if ( !horizontalScrollable || event.getTypeInt() == Event.ONMOUSEOUT ) {
            scrollRightTimer.cancel();
            return;
        }
        scrollRightTimer.scheduleRepeating( SCROLL_DELAY );
    }

    private int getScrollPosition() {
        String rawRight = table.getStyle().getRight();
        MatchResult result = RegExp.compile( "\\d*" ).exec( rawRight );
        int scrollPosition = 0;
        try {
            scrollPosition = Integer.valueOf( result.getGroup( 0 ) );
        } catch ( NumberFormatException e ) {
            //probably empty string; returning zero
        }
        return scrollPosition;
    }

    private int getRightBorder() {
        int scrollContainerWidth = scrollContainer.getClientWidth();
        int tableWidth = table.getClientWidth();
        return tableWidth - scrollContainerWidth;
    }

    private void addStyleToElement( Element element, String styleName ) {
        if ( element == null ) {
            return;
        }
        if ( !element.hasClassName( styleName ) ) {
            element.addClassName( styleName );
        }
    }

    private void removeStyleFromElement( Element element, String styleName ) {
        if ( element == null ) {
            return;
        }
        if ( element.hasClassName( styleName ) ) {
            element.removeClassName( styleName );
        }
    }

    private Element findRowElementByValue( T value ) {
        if ( rows == null || value == null ) {
            return null;
        }
        for (Map.Entry<Element, T> entry : rows.entrySet()) {
            if ( value.equals( entry.getValue() ) ) {
                return entry.getKey();
            }
        }
        return null;
    }

    List<ColumnHeader> columnHeaders = new ArrayList<ColumnHeader>();
    List<ColumnValue<T>> columnValues = new ArrayList<ColumnValue<T>>();
    Map<Element, T> rows = new HashMap<Element, T>();
    Map<T, RowInfo> rowInfoMap = new HashMap<T, RowInfo>();
    List<T> values = new ArrayList<T>();

    List<String> headerValues = new ArrayList<String>();
    List<List<String>> rowsValues = new ArrayList<List<String>>();

    int canDragStartColumnIndex = -1;

    List<NeedRowRemove<T>> rowRemoveHandlers = new ArrayList<NeedRowRemove<T>>();

    Element rowHeader;
    Element body;
    Element table;

    Element tableContainer;
    Element scrollContainer;
    Element scrollLeft;
    Element scrollRight;

    HandlerRegistration scrollRegistration;
    EndOfTableHandler endOfTableHandler;

    HandlerRegistration resizeRegistration;

    String headerStyle = "";
    String rowSelectionStyle = "selected";

    boolean draggableRows;
    boolean horizontalScrollable = false;
    boolean hasDetailsRow = false;

    Timer scrollLeftTimer = new Timer() {
        @Override
        public void run() {
            updateTableScroll( -1 );
            updateScrollLeft();
            updateScrollRight();
        }
    };
    Timer scrollRightTimer = new Timer() {
        @Override
        public void run() {
            updateTableScroll( 1 );
            updateScrollLeft();
            updateScrollRight();
        }
    };
    final int SCROLL_DELAY = 1;
    int SCROLL_STEP = 4;


    int lastColumnIndex = 0;
    List<AbstractColumn> columnList = new ArrayList<AbstractColumn>();
    Map<Integer, Boolean> columnsVisibility = new HashMap<Integer, Boolean>();

    public static interface EndOfTableHandler {
        void onEndOfTable();
    }
}
