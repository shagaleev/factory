package ru.brainworm.factory.widget.table.client.helper;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import ru.brainworm.factory.widget.table.client.ColumnHeader;
import ru.brainworm.factory.widget.table.client.ColumnValue;
import ru.brainworm.factory.widget.table.client.Selection;

/**
 * Стандартная колонка с текстовым заполнением c возможностью выбора по клику
 */
public abstract class ClickColumn<T> {

    public interface Handler<T> extends AbstractColumnHandler<T> {
        void onItemClicked( T value );
    }

    public ColumnHeader header = new ColumnHeader() {
        @Override
        public void handleEvent( Event event ) {}

        @Override
        public void fillHeader( Element columnHeader ) {
            fillColumnHeader( columnHeader );
        }
    };

    public ColumnValue<T> values = new SelectionRow();

    public class SelectionRow implements ColumnValue<T>, Selection.CanSelectRow<T> {
        @Override
        public void setSelectRowHandler( SelectRowHandler< T > selectRowHandler ) {
            this.selectRowHandler = selectRowHandler;
        }

        @Override
        public void handleEvent( Event event, T value ) {
            if ( !"click".equalsIgnoreCase( event.getType() ) ) {
                return;
            }

            event.preventDefault();
            if ( selectRowHandler == null ) {
                return;
            }
            selectRowHandler.setRowSelected( selectedRow, false );
            if ( value != selectedRow ) {
                selectRowHandler.setRowSelected( value, true );
                selectedRow = value;
            } else {
                selectedRow = null;
            }

            if ( handler != null ) {
                handler.onItemClicked( selectedRow );
            }
        }

        @Override
        public void fillValue( Element cell, T value ) {
            cell.getStyle().setCursor( Style.Cursor.POINTER );
            fillColumnValue( cell, value );
            if ( selectRowHandler != null && selectedRow != null && selectedRow.equals( value ) ) {
                selectRowHandler.setRowSelected( value, true );
            }
        }

        protected SelectRowHandler<T> selectRowHandler;
    }

    protected abstract void fillColumnHeader(Element columnHeader);

    public abstract void fillColumnValue( Element cell, T value );

    public void setHandler( Handler<T> handler ) {
        this.handler = handler;
    }

    public void setSelectedRow( T value ) {
        this.selectedRow = value;
    }

    T selectedRow;
    Handler<T> handler;
}
