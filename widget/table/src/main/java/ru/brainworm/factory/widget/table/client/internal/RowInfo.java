package ru.brainworm.factory.widget.table.client.internal;

import com.google.gwt.user.client.Element;

import java.util.HashMap;
import java.util.Map;

/**
 * Информация об одной строчке таблицы
 */
public class RowInfo<T> {

    /**
     * Уровень, на котором строка отображается
     */
    public int level = 0;

    /**
     * Потомки
     */
    public Map< T, Element> children = new HashMap<T, Element>();

    public int getChildCount() {
        return children.size();
    }
}
