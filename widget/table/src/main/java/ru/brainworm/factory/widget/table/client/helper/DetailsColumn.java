package ru.brainworm.factory.widget.table.client.helper;

import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import ru.brainworm.factory.widget.table.client.*;


/**
 * Раскрывающаяся колонка детализации
 */
public abstract class DetailsColumn<T>{

    public void setRowModeProvider( RowModeColumnProvider< T > provider ) {
        this.provider = provider;
    }

    public abstract void render( T row, Element cell );

    public class Header implements ColumnHeader {
        @Override
        public void handleEvent( Event event ) {}

        @Override
        public void fillHeader( Element columnHeader ) {
        }
    }

    public Header header = new Header();
    public ColumnValue<T> values = new Values();

    public class Values implements ColumnValue<T>, NeedRowRemove<T>, IsDetailsRow<T> {
        @Override
        public void handleEvent( Event event, T value ) {}

        @Override
        public void fillValue( Element cell, T value ) {
            render( value, cell );
        }

        @Override
        public void onRowRemoved( T row ) {
            provider.remove( row );
        }

        @Override
        public boolean isExpanded( T row ) {
            return provider.isExpandedRow( row );
        }
    }

    RowModeColumnProvider< T > provider;
}
