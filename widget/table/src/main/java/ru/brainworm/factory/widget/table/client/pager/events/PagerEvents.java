package ru.brainworm.factory.widget.table.client.pager.events;

import com.google.gwt.user.client.ui.HasWidgets;

/**
 * События пагинатора
 */
public class PagerEvents {
    /**
     * Событие инициализации пагинаотра
     */
    public static class Show {
        /**
         * @param parent контейнер для размещения пагинатора
         * @param limit  количество записей на страницу
         * @param total  общее количество записей
         */
        public Show( HasWidgets parent, Integer limit, Integer total ) {
            this.parent = parent;
            this.limit = limit;
            this.total = total;
        }

        public HasWidgets parent;
        public Integer limit;
        public Integer total;
    }

    /**
     * Пользователь изменил интервал в пагинаторе
     */
    public static class RangeChanged {
        public RangeChanged( HasWidgets parent, Integer start, Integer limit ) {
            this.parent = parent;
            this.start = start;
            this.limit = limit;
        }

        public HasWidgets parent;
        public Integer start;
        public Integer limit;
    }

    /**
     * Изменение данных вне пагинатора
     */
    public static class Change {
        /**
         *
         * @param parent контейнер для размещения пагинатора
         * @param start  номер текущей первой записи
         * @param total  общее количество записей
         */
        public Change(HasWidgets parent, Integer start, Integer total ) {
            this.parent = parent;
            this.start = start;
            this.total = total;
        }

        public HasWidgets parent;
        public Integer start;
        public Integer total;
    }
}
