package ru.brainworm.factory.widget.table.client.pager.activity;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * Абстрактный вью пагинатора
 */
public interface AbstractPagerView extends IsWidget {
    void setActivity( PagerActivity pagerActivity );

    /**
     * Отобразить текущее значение интервала
     *
     * @param start     номер текущей первой записи на странице
     * @param end     номер текущей последне записи на странице
     * @param total     всего записей
     */
    void setRange( Integer start, Integer end, Integer total );
}
