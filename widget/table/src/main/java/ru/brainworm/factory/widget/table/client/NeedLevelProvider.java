package ru.brainworm.factory.widget.table.client;

/**
 * Интерфейс, который должен быть реализован колонкой (ColumnValue), если она хочет быть
 * раскрываемой (дерево)
 */
public interface NeedLevelProvider<T> {

    /**
     * Интерфейс, предоставляющий информацию об уровне вложенности строки
     */
    public static interface LevelProvider<T> {

        /**
         * Возвращает уровень вложенности строки
         */
        int getLevel( T row );
    }

    /**
     * Задает объект, предоставляющий информацию об уровне вложенности строки
     */
    void setLevelProvider( LevelProvider<T> levelProvider );
}
