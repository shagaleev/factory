package ru.brainworm.factory.widget.table.client.helper;

import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import ru.brainworm.factory.widget.table.client.ColumnHeader;
import ru.brainworm.factory.widget.table.client.ColumnValue;

/**
 * Стандартная колонка с тектовым заполнением c возможностью выбора по dblclick
 */
public abstract class DblClickColumn<T> {
    public interface DblClickColumnHandler<T> extends AbstractColumnHandler<T> {
        void onItemSelected( T value );
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName( String name ) {
        columnName = name;
    }


    public ColumnHeader header = new ColumnHeader() {
        @Override
        public void handleEvent( Event event ) {}

        @Override
        public void fillHeader( Element columnHeader ) {
            fillColumnHeader( columnHeader );
        }
    };


    public ColumnValue<T> values = new ColumnValue<T>() {

        @Override
        public void handleEvent( Event event, T value ) {
            if ( !"dblclick".equalsIgnoreCase( event.getType() ) ) {
                return;
            }

            event.preventDefault();
            if ( handler != null ) {
                handler.onItemSelected(value);
            }
        }

        @Override
        public void fillValue( Element cell, T value ) {
            fillColumnValue( cell, value );
        }
    };

    protected abstract void fillColumnHeader(Element columnHeader);
    public abstract void fillColumnValue( Element cell, T value );

    public void setHandler( DblClickColumnHandler<T> handler ) {
        this.handler = handler;
    }

    String columnName;
    DblClickColumnHandler<T> handler;
}
