package ru.brainworm.factory.widget.table.client.pager.activity;


import ru.brainworm.factory.widget.table.client.pager.view.PagerView;

/**
 * Абстрактное активити пагинатора
 */
public interface AbstractPagerActivity {
    /**
     * Кликнута кнопка назад
     *
     * @param view для получения модели соответсвующей вью.
     */
    void onBackClicked( PagerView view );

    /**
     * Кликнута кнопка вперед
     *
     * @param view для получения модели соответсвующей вью.
     */
    void onForwardClicked( PagerView view );

    /**
     * Кликнута кнопка перейти к первой странице
     *
     * @param view для получения модели соответсвующей вью.
     */
    void onFirstPageClicked( PagerView view );

    /**
     * Кликнута кнопка перейти к последней странице
     *
     * @param view для получения модели соответсвующей вью.
     */
    void onLastPageClicked( PagerView view );
}
