package ru.brainworm.factory.widget.table.client.pager.activity;

import com.google.gwt.user.client.ui.HasWidgets;

/**
 * Модель вью пагинатора
 */
public class PagerViewModel {
    /**
     *
     * @param parent    контейнер для вью / ключ для идентификации пагианаотра на странице
     * @param view      вью пагинатора
     * @param limit     количество на страницу
     * @param start     номер текущего первого элемента
     * @param total     всего элементов
     */
    public PagerViewModel(HasWidgets parent
            , AbstractPagerView view
            , Integer limit
            , Integer start
            , Integer total
    ) {
        this.parent = parent;
        this.view = view;
        this.limit = limit;
        this.start = start;
        this.total = total;
    }

    public HasWidgets parent;
    public AbstractPagerView view;
    public Integer limit;
    public Integer start;
    public Integer total;
}
