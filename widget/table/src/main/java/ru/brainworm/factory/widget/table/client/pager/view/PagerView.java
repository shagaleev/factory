package ru.brainworm.factory.widget.table.client.pager.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import ru.brainworm.factory.widget.table.client.pager.activity.AbstractPagerView;
import ru.brainworm.factory.widget.table.client.pager.activity.PagerActivity;


/**
 * Вью пагинатора
 */
public class PagerView extends Composite implements AbstractPagerView {

    public PagerView() {
        initWidget( ourUiBinder.createAndBindUi( this ) );
    }


    @Override
    public void setActivity( PagerActivity activity ) {
        this.activity = activity;
    }

    @Override
    public void setRange( Integer start, Integer end, Integer total ) {
        range.setText( start + " - " + end + " of " + total );
    }

    @UiHandler("back")
    public void onBack( ClickEvent event ) {
        if ( null != activity ) {
            activity.onBackClicked( this );
        }
    }


    @UiHandler("firstpage")
    public void onFirstPage( ClickEvent event ) {
        if ( null != activity ) {
            activity.onFirstPageClicked( this );
        }
    }

    @UiHandler("forward")
    public void onForward( ClickEvent event ) {
        if ( null != activity ) {
            activity.onForwardClicked( this );
        }
    }


    @UiHandler("lastpage")
    public void onLastpagePage( ClickEvent event ) {
        if ( null != activity ) {
            activity.onLastPageClicked( this );
        }
    }


    @UiField
    Label back;
    @UiField
    Label firstpage;
    @UiField
    Label forward;
    @UiField
    Label lastpage;
    @UiField
    Label range;


    private PagerActivity activity;

    interface PagerViewUiBinder extends UiBinder<Widget, PagerView> {
    }

    private static PagerViewUiBinder ourUiBinder = GWT.create( PagerViewUiBinder.class );


}