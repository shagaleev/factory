package ru.brainworm.factory.widget.table.client.pager.factory;


import ru.brainworm.factory.widget.table.client.pager.activity.AbstractPagerView;

/**
 * Абстрактная фабрика пагинатора
 */
public interface AbstractPagerFactory {

    AbstractPagerView getAbstractPagerView();

}
