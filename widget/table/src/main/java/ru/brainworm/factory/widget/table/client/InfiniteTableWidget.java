package ru.brainworm.factory.widget.table.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.TableCellElement;
import com.google.gwt.dom.client.TableRowElement;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by shagaleev on 17/09/16.
 */
public class InfiniteTableWidget<T> extends TableWidget<T> implements DataCache.DataCacheHandler<T> {

    public interface PagerListener {
        void onPageChanged( int pageNumber );
    }

    public interface SeparatorProvider<T> {
        void fillSeparatorValue( Element separator, int pageNum, InfiniteTableWidget<T> table );
    }

    public void setLoadHandler(final InfiniteLoadHandler<T> outer ) {
        cache.setLoadHandler( outer );
    }

    @Override
    public void onDataCacheChanged() {
//        log("onDataCacheChanged(): ");
        repaint();
    }

    public void setPagerListener( PagerListener pagerListener ) {
        this.pagerListener = pagerListener;
    }

    public void setSeparatorProvider( SeparatorProvider separatorProvider ) {
        this.separatorProvider = separatorProvider;
    }

    // sets records count on page without separator
    public void setPageSize( int pageSize ) {
        PAGE_SIZE = pageSize;
    }

    // returns records count on page without separator
    public int getPageSize() {
        return PAGE_SIZE;
    }

    public int getPageCount() {
        int fullPages = totalRecords / PAGE_SIZE;
        int rowsOnLastPage = totalRecords - fullPages * PAGE_SIZE;
        int halfPages = rowsOnLastPage > 0 ? 1 : 0;
        return fullPages + halfPages;
    }

    public InfiniteTableWidget() {
        super();

        fakeTopRow = DOM.createElement( "tr" );
        DOM.appendChild( body, fakeTopRow );

        fakeBottomRow = DOM.createElement( "tr" );
        DOM.appendChild( body, fakeBottomRow );
    }

    public void setChunkSize( int chunkSize ) {
        cache.setChunkSize( chunkSize );
    }

    public void setPrefetchThreshold( int threshold ) {
        cache.setPrefetchThreshold( threshold );
    }

    @Override
    protected void onAttach() {
        super.onAttach();
        absoluteTableTop = null;
        topBodyOffset = body.getAbsoluteTop();

        scrollRegistration = Window.addWindowScrollHandler( new Window.ScrollHandler() {
            @Override
            public void onWindowScroll( Window.ScrollEvent scrollEvent ) {
                repaint();
            }
        } );
    }

    @Override
    protected void onDetach() {
        super.onDetach();

        scrollRegistration.removeHandler();
    }

    @Override
    protected void onBodyEvent( Event event ) {
        Element targetElement = event.getEventTarget().cast();
        Element cellOrRow = findParent( targetElement );

        TableRowElement row = null;
        int cellIndex = canDragStartColumnIndex;
        if ( cellOrRow != null && cellOrRow.getTagName().equalsIgnoreCase( "tr" ) ) {
            row = cellOrRow.cast();
        } else {
            TableCellElement cell = findParent( targetElement ).cast();
            if ( cell == null ) {
                return;
            }
            row = cell.getParentElement().cast();
            cellIndex = cell.getCellIndex();
        }

        if ( draggableRows && ( event.getType().startsWith( "drag" ) || event.getType().startsWith( "drop" ) ) && canDragStartColumnIndex != -1 ) {
            cellIndex = canDragStartColumnIndex;
        }

        if ( cellIndex < 0 ) {
            return;
        }

        ColumnValue<T> columnValue = columnValues.get( cellIndex );
        if ( columnValue == null ) {
            return;
        }

        int index = getRowIndex( row );
        if ( values.size() > index-1 ) {
            columnValue.handleEvent( event, values.get( index - 1 ) );
        }
    }

    public void scrollToPage( int pageNumber ) {
        int firstRowOnPage = pageNumber * PAGE_SIZE;
        int rowPixelsOffset = getRowOffsetTop( firstRowOnPage );

        Window.scrollTo( 0, rowPixelsOffset );
    }

    public int getActivePage() {
        return visibleFrom / PAGE_SIZE;
    }

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords( int value ) {
        totalRecords = value;
        log( "setTotalRecords(): totalRecords=" + totalRecords );

        updatePager( getActivePage() );
        repaint();
    }

    public void setTopGap( int rowsBeforeFirst ) {
        int rowHeight = getRowHeight();
        fakeTopRow.getStyle().setHeight( rowHeight * rowsBeforeFirst, Style.Unit.PX );
    }

    public void setBottomGap( int rowsAfterLast ) {
        int rowHeight = getRowHeight();
        fakeBottomRow.getStyle().setHeight( rowHeight * rowsAfterLast, Style.Unit.PX );
    }


    public int getRowNumber( int pixelsOffsetFromTableTop ) {
        if ( pixelsOffsetFromTableTop <= 0 ) {
            return 0;
        }

        int pixelsOffsetFromBodyStart = pixelsOffsetFromTableTop - (getAbsoluteBodyTop() - getAbsoluteTop());
        if ( pixelsOffsetFromBodyStart <= 0 ) {
            return 0;
        }

        return pixelsOffsetFromBodyStart/getRowHeight();
    }

    public int getRowOffsetTop( int rowNumber ) {
        return getAbsoluteBodyTop() + rowNumber * getRowHeight();
    }

    public void clearCache() {
        cache.clearCache();
    }

    @Override
    public void clearRows() {
        clearSeparators();
        setTopGap( 0 );
        setBottomGap( 0 );

        super.clearRows();
    }

    private int getRowHeight() {
        if ( rowHeight != null ) {
            return rowHeight;
        }

        if ( rows.size() == 0 ) {
            return 56;//default height
        }

        double allVisibleRowsHeight = 0;
        for ( Element element : rows.keySet() ) {
            allVisibleRowsHeight += element.getOffsetHeight();
        }
        for ( Element element : separatorElements.values() ) {
            allVisibleRowsHeight += element.getOffsetHeight();
        }
        double oneRowHeightApprox = allVisibleRowsHeight/(rows.size()+separatorElements.size());

        rowHeight = new Double( oneRowHeightApprox ).intValue();
        return rowHeight;
    }

    public void repaint() {
        int visibleRowNumber = getVisibleRowNumber();

        updatePager( calcPage(visibleRowNumber) );

        if ( visibleRowNumber > totalRecords ) {
            log.info( "repaint() visibleRowNumber > totalRecords visibleRowNumber=" + visibleRowNumber + " totalRecords=" + totalRecords );
            visibleRowNumber = totalRecords;
        }

//        prof.start();
        fillTable( visibleRowNumber );
//        prof.check();
    }

    private int calcPage(int row) {
        int page;
        if (totalRecords - row < PAGE_SIZE) {
            page = getPageCount()-1;
        } else {
            page = row / PAGE_SIZE;
        }
        if (page < 0) {
            page = 0;
        }
        return page;
    }

    void fillTable(final int visibleFrom ) {
        Map<T, Element> renderedRows = getRenderedRows();

        clearSeparators();
        super.clearRows();
        int topGapDelta = 0;

        int to = visibleFrom + PAGE_SIZE;
        if ( to > totalRecords ) to = totalRecords;

        for (int rowNum = visibleFrom; rowNum < to; rowNum++) {
            addSeparator( rowNum );

            T rowData = cache.get( rowNum );
            if ( rowData != null ) {
                addRow( rowData, renderedRows );
            }
            else if ( rowData == null && values.isEmpty() ) {
                topGapDelta++;
            }
        }

        renderedRows.clear();
        setTopGap( visibleFrom + topGapDelta );
        setBottomGap( totalRecords - (visibleFrom + topGapDelta) - (body.getChildCount()-2) );
    }

    private void addSeparator( int rowNum ) {
        if ( separatorsEnabled() ) {
            if ( rowNum % PAGE_SIZE == 0 ) {
                addSeparatorRow( rowNum / PAGE_SIZE );
            }
        }
    }

    private Map<T, Element> getRenderedRows() {
        Map<T, Element> prev = new HashMap<T, Element>( values.size() );
        for (T value : values) {
            for (Map.Entry<Element, T> entry : rows.entrySet()) {
                if ( value.equals( entry.getValue() ) ) {
                    prev.put( value, entry.getKey() );
                    break;
                }
            }
        }
        return prev;
    }

    public Element addRow( T row ) {
        return addRow( row, null, null );
    }

    public Element addRow( T row, Map<T, Element> prev ) {
        return addRow( row, null, prev );
    }

    public Element addRow( T row, String styleName, Map<T, Element> prev ) {
        Element elemRow;
        if ( prev != null && prev.containsKey( row ) ) {
            elemRow = prev.get( row );
            DOM.insertBefore( body, elemRow, fakeBottomRow );
            rows.put( elemRow, row );
            values.add( row );
        }
        else {
            elemRow = DOM.createElement( "tr" );
            if (styleName != null) {
                elemRow.addClassName(styleName);
            }
            Element detailsElemRow = null;

            if ( draggableRows ) {
                elemRow.setDraggable( Element.DRAGGABLE_TRUE );
            }
            DOM.insertBefore( body, elemRow, fakeBottomRow );
            rows.put( elemRow, row );//с целью доступа к созданной строке таблицы при заполении ячеек, добавить до заполнеия ячеек
            values.add( row );

            if ( hasDetailsRow ) {
                detailsElemRow = DOM.createElement( "tr" );
                DOM.insertBefore( body, detailsElemRow, DOM.getNextSibling( elemRow ) );
                rows.put( detailsElemRow, row );
            }

            fillRow( elemRow, detailsElemRow, row );
        }

        return elemRow;
    }


    private void log( String str ) {
        log.info( "IT: " + str );
    }

    private int getVisibleRowNumber() {
        int scrollTop = Window.getScrollTop();
        int tableTop = getAbsoluteTop();
        int visibleRowNumber = getRowNumber( scrollTop - tableTop );
//        log.info( "getVisibleRowNumber() visibleRowNumber=" + visibleRowNumber+ " scrollTop=" + scrollTop + " tableTop=" + tableTop );
        return visibleRowNumber;
    }

    private void updatePager( int pageNumber ) {
        if ( pagerListener != null ) {
            pagerListener.onPageChanged( pageNumber );
        }
    }

    private void addSeparatorRow( int pageNum ) {
//        GWT.log( "addSeparatorRow: "+pageNum );
        Element elemRow = separatorElements.get( pageNum );
        if ( elemRow != null ) {
            elemRow.removeFromParent();
        }

        elemRow = DOM.createElement( "tr" );

        if ( draggableRows ) {
            elemRow.setDraggable( Element.DRAGGABLE_TRUE );
        }
        DOM.insertBefore( body, elemRow, fakeBottomRow );

        Element cell = DOM.createElement( "td" );
        cell.setAttribute( "colspan", String.valueOf( columnValues.size() ) );
        DOM.appendChild( elemRow, cell );
        if ( separatorProvider != null ) {
            separatorProvider.fillSeparatorValue( cell, pageNum, this );
        }

        separatorElements.put( pageNum, elemRow );
    }

    private void clearSeparators() {
        for ( Map.Entry< Integer, Element > entry : separatorElements.entrySet() ) {
            Element rowElement = entry.getValue();
            if ( rowElement != null && rowElement.getParentElement() != null ) {
                rowElement.removeFromParent();
            }
        }
        separatorElements.clear();
    }


    @Override
    protected int getRowIndex( TableRowElement row ) {
        Element rowElement = row.cast();
        Element tableBody = row.getParentElement().cast();
        int index = 0;
        for ( int i=0; i < tableBody.getChildCount(); ++i ) {
            Element child = tableBody.getChild( i ).cast();
            if ( child == rowElement ) {
                return index;
            }
            if ( separatorElements.containsValue( child ) ) {
                continue;
            }
            ++index;
        }

        log.severe( "getRowIndex(): impossible situation" );
        return 0;
    }

    private boolean separatorsEnabled() {
        return separatorProvider != null;
    }

    @Override
    public int getAbsoluteTop() {
        if ( absoluteTableTop == null ) {
            absoluteTableTop = super.getAbsoluteTop();
        }

        return absoluteTableTop;
    }

    private int getAbsoluteBodyTop() {
        if ( topBodyOffset == null ) {
            topBodyOffset = body.getAbsoluteTop();
        }
        return topBodyOffset;
    }

    private Integer absoluteTableTop = null;
    Integer topBodyOffset = null;

    Element fakeTopRow;
    Element fakeBottomRow;

    int PAGE_SIZE = 30;
    int totalRecords = 0;

    int visibleFrom = 0;


    DataCache<T> cache = new DataCache<T>( this );

    HandlerRegistration scrollRegistration = null;

    PagerListener pagerListener;

    SeparatorProvider separatorProvider;

    Map<Integer, Element > separatorElements = new HashMap<Integer, Element>();

    Integer rowHeight = null;

    AveragingProfiler prof = new AveragingProfiler( new AveragingProfiler.Appender() {
        private long min;
        private long max;

        @Override
        public void average( long time ) {
            GWT.log( "average: " + time + " " + min + "-" + max );
        }

        @Override
        public void max( long max ) {
            this.max = max;
        }

        @Override
        public void min( long min ) {
            this.min = min;
        }
    } );

    private static final Logger log = Logger.getLogger( InfiniteTableWidget.class.getName() );
}

/***
 * Возвращает усредненное значение по числу произведенных замеров
 * (до 100 последних замеров)
 */
class AveragingProfiler {

    interface Appender {
        void average( long time );

        void max( long max );

        void min( long min );
    }

    public Appender appender = new Appender() {
        public void average( long time ) {
            GWT.log( "average: " + time );
        }

        @Override
        public void max( long max ) {
            GWT.log( "max: " + max );
        }

        @Override
        public void min( long min ) {
            GWT.log( "min: " + min );
        }
    };

    public AveragingProfiler( Appender appender ) {
        this.appender = appender;
    }

    public void start() {
        currentTimeMillis = System.currentTimeMillis();
    }

    public void check() {
        timeIntervals[i] = (System.currentTimeMillis() - currentTimeMillis);
        if ( ++i >= SAMPLE_SIZE ) {
            i = 0;
        }
        if ( count < i ) {
            count = i;
        }
        long summ = 0;
        for (int j = 0; j < count; j++) {
            summ += timeIntervals[j];
        }

        long average = summ / count;
        if ( average < min ) min = average;
        if ( max < average ) max = average;
        appender.min( min );
        appender.max( max );
        appender.average( average );
    }

    int i = 0;
    int count = 0;
    long[] timeIntervals = new long[SAMPLE_SIZE];

    long currentTimeMillis;
    public long min = Long.MAX_VALUE;
    public long max = 0;

    //количество усредняемых измерений
    private static final int SAMPLE_SIZE = 100;
}