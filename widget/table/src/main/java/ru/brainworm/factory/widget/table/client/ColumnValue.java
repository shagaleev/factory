package ru.brainworm.factory.widget.table.client;

import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;

/**
 * Абстракция ячейки со значением
 */
public interface ColumnValue<T> {
    /**
     * Вызываем обработчик события
     */
    void handleEvent( Event event, T value );

    /**
     * Заполняет ячейку
     * @param cell     ячейка
     * @param value    значение
     */
    void fillValue( Element cell, T value );
}
