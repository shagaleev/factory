package ru.brainworm.factory.widget.table.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

/**
* Created by shagaleev on 17/09/16.
*/
public interface InfiniteLoadHandler<T> {
    void loadData( int offset, int limit, AsyncCallback<List<T>> handler );
}
