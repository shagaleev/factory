package ru.brainworm.factory.widget.table.client.pager;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import ru.brainworm.factory.widget.table.client.pager.factory.PagerClientFactory;

/**
 * Запуск пейджера
 */
public class PagerEntryPoint implements EntryPoint {
    @Override
    public void onModuleLoad() {
        factory = GWT.create( PagerClientFactory.class );

    }

    PagerClientFactory factory;
}
