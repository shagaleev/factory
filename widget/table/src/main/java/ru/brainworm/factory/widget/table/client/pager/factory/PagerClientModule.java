package ru.brainworm.factory.widget.table.client.pager.factory;

import com.google.gwt.inject.client.AbstractGinModule;
import ru.brainworm.factory.widget.table.client.pager.activity.AbstractPagerView;
import ru.brainworm.factory.widget.table.client.pager.activity.PagerActivity;
import ru.brainworm.factory.widget.table.client.pager.view.PagerView;

/**
 * Описание классов фабрики
 */
public class PagerClientModule extends AbstractGinModule {
    @Override
    protected void configure() {
        bind( AbstractPagerView.class ).to( PagerView.class );
        bind( PagerActivity.class ).asEagerSingleton();
        bind( AbstractPagerFactory.class ).to( PagerClientFactory.class ).asEagerSingleton();

    }
}
