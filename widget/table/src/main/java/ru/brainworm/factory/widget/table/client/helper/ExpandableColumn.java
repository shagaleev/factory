package ru.brainworm.factory.widget.table.client.helper;

import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import ru.brainworm.factory.widget.table.client.ColumnHeader;
import ru.brainworm.factory.widget.table.client.ColumnValue;

import java.util.HashMap;
import java.util.Map;

/**
 * Колонка, раскрываемая как дерево
 */
public abstract class ExpandableColumn<T> {

    /**
     * Обработчик событий колонки
     * @param <T>
     */
    public interface Handler<T> extends AbstractColumnHandler<T> {
        /**
         * Вызывается, когда узел открывается и надо заполнить потомками таблицу
         */
        public void onExpand( T row );

        /**
         * Вызывается, когда узел закрывается и надо удалить потомков из таблицы
         */
        public void onCollapse( T row );

        /**
         * Вызывается, когда кликнули на ссылке на дочерний элемент
         * @param row        строка
         * @param childId    идентификатор дочернего элемента
         */
        public void onChildClicked( T row, Long childId );
    }

    /**
     * Задает обработчика событий колонки
     * @param eventHandler
     */
    public void setEventHandler( Handler<T> eventHandler ) {
        this.eventHandler = eventHandler;
    }

    /**
     * Возвращает обработчика событий колонки
     */
    public Handler<T> getEventHandler() { return eventHandler; }

    /**
     * Режим колонки
     */
    public static enum Mode {
        /**
         * Есть потомки и они видны
         */
        EXPANDED,

        /**
         * Есть потомки и они спрятаны
         */
        COLLAPSED,

        /**
         * Нет потомков
         */
        EMPTY
    }

    /**
     * Возвращает наличие потомков у данной строки
     * @param value    строка
     * @return наличие потомков
     */
    public abstract boolean hasChildren( T value );

    /**
     * Рендерит ячейку
     * @param row      строка
     * @param cell     элемент ячейки
     * @param mode     текущий режим
     */
    public abstract void render( T row, Element cell, Mode mode );

    /**
     * Заполнение заголовка столбца
     * @param columnHeader  заголовок
     */
    public abstract void renderHeader( Element columnHeader );

    public class Header implements ColumnHeader {
        @Override
        public void handleEvent( Event event ) {}

        @Override
        public void fillHeader( Element columnHeader ) {
            renderHeader( columnHeader );
        }
    }

    public Header header = new Header();

    public void clear(){
        if ( values != null ) {
            values.clear();
        }
    }

    public class Values implements ColumnValue<T> {
        @Override
        public void handleEvent( Event event, T value ) {
            if ( !"click".equals( event.getType() ) ) {
                return;
            }
            event.preventDefault();

            Mode rowMode = rowModes.get( value );
            if ( rowMode == null ) {
                return;
            }

            if ( rowMode == Mode.EMPTY ) {
                return;
            }

            Element cell = event.getEventTarget().cast();
            Long targetId = null;
            if ( cell.getId() != null ) {
                try {
                    targetId = Long.parseLong( cell.getId() );
                }
                catch ( Exception e ) {}
            }
            if ( targetId != null ) {
                eventHandler.onChildClicked( value, targetId );
                return;
            }

            while ( !cell.getTagName().equalsIgnoreCase( "td" ) ) {
                cell = cell.getParentElement().cast();
            }

            while ( cell.getChildCount() > 0 ) {
                Element child = cell.getChild( 0 ).cast();
                if ( child == null ) {
                    break;
                }

                cell.removeChild( child );
                child.removeFromParent();
            }

            if ( rowMode == Mode.COLLAPSED ) {
                rowModes.put( value, Mode.EXPANDED );
                if ( eventHandler != null ) {
                    eventHandler.onExpand( value );
                }
            }
            else if ( rowMode == Mode.EXPANDED ) {
                rowModes.put( value, Mode.COLLAPSED );
                if ( eventHandler != null ) {
                    eventHandler.onCollapse( value );
                }
            }

            fillValue( cell, value );
        }

        @Override
        public void fillValue( Element cell, T value ) {
            Mode rowMode = rowModes.get( value );
            if ( rowMode == null ) {
                rowMode = Mode.EMPTY;
                if ( hasChildren( value ) ) {
                    rowMode = Mode.COLLAPSED;
                }
                rowModes.put( value, rowMode );
            }

            render( value, cell, rowMode );
        }

        public void clear(){
            rowModes.clear();
        }

        private Map<T, Mode> rowModes = new HashMap<T, Mode>();
    }
    public Values values = new Values();

    private Handler<T> eventHandler;
}
