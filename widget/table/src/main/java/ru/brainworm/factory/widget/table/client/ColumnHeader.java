package ru.brainworm.factory.widget.table.client;

import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;

/**
 * Абстрактный заголовок для одной колонки
 */
public interface ColumnHeader {
    /**
     * Вызываем обработчик события
     */
    void handleEvent( Event event );

    /**
     * Заполняет заголовок
     */
    void fillHeader( Element columnHeader );
}
