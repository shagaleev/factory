package ru.brainworm.factory.widget.table.client.columns;

import com.google.gwt.user.client.ui.Widget;
import ru.brainworm.factory.widget.table.client.ColumnHeader;
import ru.brainworm.factory.widget.table.client.ColumnValue;
import ru.brainworm.factory.widget.table.client.helper.AbstractColumnHandler;
import ru.brainworm.factory.widget.table.client.helper.SelectionColumn;

public class SelectionColumnWidget<T> extends SelectionColumn<T> implements AbstractColumnWidget<T> {

    @Override
    public ColumnHeader getColumnHeader() {
        return header;
    }

    @Override
    public ColumnValue<T> getColumnValues() {
        return values;
    }

    @Override
    public void setFieldName( String fieldName ) {
        //do nothing
    }

    @Override
    public String getFieldName() {
        return null;
    }

    @Override
    public void setVisibleByDefault( boolean visibleByDefault ) {
        this.visibleByDefault = visibleByDefault;
    }

    @Override
    public Boolean isVisibleByDefault() {
        return visibleByDefault;
    }

    @Override
    public void setHandler( AbstractColumnHandler<T> handler ) {
        if ( handler instanceof SelectionHandler ) {
            super.setSelectionHandler( (SelectionHandler<T>) handler );
        }
    }

    @Override
    public Widget asWidget() {
        return null;
    }

    protected Boolean visibleByDefault = true;
}
