package ru.brainworm.factory.widget.table.client.helper;

import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import ru.brainworm.factory.widget.table.client.ColumnHeader;
import ru.brainworm.factory.widget.table.client.ColumnValue;

/**
 * Логин пользователя
 */
public abstract class StaticColumn<T> {

    public StaticColumn( String columnName ) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }

    public ColumnHeader header = new ColumnHeader() {
        @Override
        public void handleEvent( Event event ) {}

        @Override
        public void fillHeader( Element columnHeader ) {
            columnHeader.setInnerText( columnName );
        }
    };

    public ColumnValue<T> values = new ColumnValue<T>() {
        @Override
        public void handleEvent( Event event, T value ) {}

        @Override
        public void fillValue( Element cell, T value ) {
            fillColumnValue( cell, value );
        }
    };

    public abstract void fillColumnValue( Element cell, T value );

    String columnName;
}
