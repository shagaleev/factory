package ru.brainworm.factory.widget.table.client;

import com.google.gwt.user.client.Element;

/**
 * Интерфейс, который должен реализовывать ColumnValue, если он хочет быть
 * измененным извне (например заголовком)
 *
 * T - тип объекта с изменениями, который прокидывает заголовок
 */
public interface CanAcceptChanges<ChangeType, ObjectType > {
    public void performChanges( ChangeType changeValue, ObjectType objectValue, Element element );
}
