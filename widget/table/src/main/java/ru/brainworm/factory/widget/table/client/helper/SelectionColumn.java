package ru.brainworm.factory.widget.table.client.helper;

import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import ru.brainworm.factory.widget.table.client.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Колонка с галочками
 */
public class SelectionColumn<T>  {

    public interface SelectionHandler<T> extends AbstractColumnHandler<T> {
        void onTableRowSelectionChanged(T object, Boolean value);
    }

    public void setSelectionHandler( SelectionHandler<T> selectionHandler ) {
        this.selectionHandler = selectionHandler;
    }

    public void setWidthColumn( double value, Style.Unit unit ) {
        headerElement.getStyle().setWidth( value, unit );
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName( String name ) {
        columnName = name;
    }

    public class Header implements ColumnHeader, CanChangeRows<Boolean>, Selection.CanSelectAllRows {
        @Override
        public void setRowChangeHandler( RowChangeHandler<Boolean> rowChangeHandler1 ) {
            rowChangeHandler = rowChangeHandler1;
        }

        @Override
        public void setSelectAllRowsHandler( SelectAllRowsHandler selectAllRowsHandler1 ) {
            selectAllRowsHandler = selectAllRowsHandler1;
        }

        @Override
        public void handleEvent( Event event ) {
            if ( !"click".equalsIgnoreCase( event.getType() ) ) {
                return;
            }

            EventTarget target = event.getEventTarget();
            if (!InputElement.is( target )) {
                return;
            }

            allSelectedClicked = true;
            setAllSelected( headerCheckbox.isChecked() );
        }

        @Override
        public void fillHeader( Element columnHeader ) {
            Element input = DOM.createInputCheck();
            columnHeader.appendChild( input );
            headerElement = columnHeader;
            headerCheckbox = input.cast();
        }

        public void setAllSelected( boolean value ) {
            headerCheckbox.setChecked( value );

            if ( rowChangeHandler != null ) {
                rowChangeHandler.changeAllRows( value );
                selectAllRowsHandler.setAllRowsSelected( value );
            }
        }

        public boolean isAllSelected() {
            return headerCheckbox.isChecked();
        }

        /**
         * Изменялось ли значение флага selectAll с предыдущей проверки
         */
        public boolean wasAllSelectedClicked() {
            boolean result = allSelectedClicked;
            allSelectedClicked = false;
            return result;
        }

    }

    CanChangeRows.RowChangeHandler<Boolean> rowChangeHandler;
    Selection.CanSelectAllRows.SelectAllRowsHandler selectAllRowsHandler;

    public ColumnHeader header = new Header();

    public class SelectionRow implements ColumnValue<T>, CanAcceptChanges< Boolean, T >, Selection.CanSelectRow<T> {

        @Override
        public void performChanges( Boolean changeValue, T objectValue, Element element ) {
            if ( changeValue ) {
                selectedValues.add( objectValue );
            }
            else {
                selectedValues.remove( objectValue );
            }

            InputElement input = element.getFirstChildElement().cast();
            input.setChecked( changeValue );

            if ( selectionHandler != null ) {
                selectionHandler.onTableRowSelectionChanged( objectValue, changeValue );
            }
            if ( selectRowHandler != null ) {
                selectRowHandler.setRowSelected( objectValue, changeValue );
            }
        }

        @Override
        public void handleEvent( Event event, T value ) {
            if ( !"click".equalsIgnoreCase( event.getType() ) ) {
                return;
            }

            EventTarget target = event.getEventTarget();
            if (!InputElement.is( target )) {
                return;
            }

            InputElement checkbox = target.cast();
            if ( checkbox.isChecked() ) {
                selectedValues.add( value );
                if ( selectAllRowsHandler != null && selectAllRowsHandler.isAllRowsSelected( selectedValues )) {
                    headerCheckbox.setChecked( true );
                }
                if ( selectionHandler != null ) {
                    selectionHandler.onTableRowSelectionChanged( value, true );
                }
                if ( selectRowHandler != null ) {
                    selectRowHandler.setRowSelected( value, true );
                }
            }
            else {
                selectedValues.remove( value );
                headerCheckbox.setChecked( false );
                if ( selectionHandler != null ) {
                    selectionHandler.onTableRowSelectionChanged( value, false );
                }
                if ( selectRowHandler != null ) {
                    selectRowHandler.setRowSelected( value, false );
                }
            }
        }

        @Override
        public void fillValue( Element cell, T value ) {
            Element input = DOM.createInputCheck();
            cell.appendChild( input );

            if ( ! selectedValues.contains( value ) ) {
                headerCheckbox.setChecked( false );
            }
            else {
                InputElement checkbox = input.cast();
                checkbox.setChecked( true );
                if ( selectAllRowsHandler != null && selectAllRowsHandler.isAllRowsSelected( selectedValues )) {
                    headerCheckbox.setChecked( true );
                }
            }

            if ( selectRowHandler != null ) {
                selectRowHandler.setRowSelected( value, selectedValues.contains( value ) );
            }
        }

        @Override
        public void setSelectRowHandler( SelectRowHandler<T> selectRowHandler ) {
            this.selectRowHandler = selectRowHandler;
        }

        protected SelectRowHandler<T> selectRowHandler;
    }

    public void clear() {
        selectedValues.clear();
        headerCheckbox.setChecked( false );
        rowChangeHandler.changeAllRows( false );
        selectAllRowsHandler.setAllRowsSelected( false );
    }

    public SelectionRow values = new SelectionRow();
    public SelectionHandler<T> selectionHandler;

    public Set<T> selectedValues = new HashSet<T>();

    private Element headerElement;
    String columnName;
    private boolean allSelectedClicked;

    private InputElement headerCheckbox;
}
