package ru.brainworm.factory.widget.table.client;

/**
 * Автор :          dima
 * Дата создания :  22.05.15 17:20
 *
 * Абстракция столбца таблицы.
 */
public interface AbstractColumn {

    /**
     * Устанавливает видимость столбца.  Перерисовывает таблицу.
     * Если необходимо изменить видимость нескольких столбцов
     * использовать {@link #setColumnVisibility(boolean)}
     * с дальнейшим вызовом перерисовки таблицы вручную.
     */
    void setVisibility( boolean visible );

    void setColumnVisibility( boolean visible );

    /**
     * Возвращает текущее состояние видимости столбца.
     */
    boolean isVisible();

}
