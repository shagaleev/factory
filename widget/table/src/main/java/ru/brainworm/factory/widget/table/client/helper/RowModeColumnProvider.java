package ru.brainworm.factory.widget.table.client.helper;


import java.util.HashMap;
import java.util.Map;

/**
 * Провайдер раскрывающегося состояния колонки в таблице
 */
public class RowModeColumnProvider<T> {

    /**
     * Режим колонки
     */
    public static enum Mode {
        /**
         * Есть потомки и они видны
         */
        EXPANDED,

        /**
         * Есть потомки и они спрятаны
         */
        COLLAPSED
    }

    public void changeRowMode( T value ) {
        Mode rowMode = rowModes.get( value );
        if ( rowMode == Mode.COLLAPSED ) {
            rowModes.put( value, Mode.EXPANDED );
        }
        else if ( rowMode == Mode.EXPANDED ) {
            rowModes.put( value, Mode.COLLAPSED );
        }
    }

    public void remove( T row ) {
        rowModes.remove( row );
    }

    public void clear() {
        rowModes.clear();
    }

    public boolean isExpandedRow( T row ) {
        Mode rowMode = rowModes.get( row );
        if ( rowMode != null && rowMode == Mode.EXPANDED ) {
            return true;
        }

        return false;
    }

    public void initRowMode( T value ) {
        Mode rowMode = rowModes.get( value );
        if ( rowMode == null ) {
            rowModes.put( value, Mode.COLLAPSED );
        }
    }

    protected Map<T, Mode> rowModes = new HashMap<T, Mode>();
}
