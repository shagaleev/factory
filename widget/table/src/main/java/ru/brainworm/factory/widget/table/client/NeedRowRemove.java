package ru.brainworm.factory.widget.table.client;

/**
 * Интерфейс, который должен быть реализован колонкой (ColumnValue), если она хочет
 * получать уведомления об удалении строк
 */
public interface NeedRowRemove<T> {

    /**
     * Вызыввается, когда из таблицы удаляется строка
     */
    void onRowRemoved( T row );
}
