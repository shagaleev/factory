package ru.brainworm.factory.widget.table.client.helper;

import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import ru.brainworm.factory.widget.table.client.ColumnHeader;

/**
 * Статический заголовок
 */
public class StaticHeader implements ColumnHeader {
    public StaticHeader( String header ) {
        this.header = header;
    }

    @Override
    public void handleEvent( Event event ) {}

    @Override
    public void fillHeader( Element columnHeader ) {
        columnHeader.setInnerText( header );
    }

    String header;
}
