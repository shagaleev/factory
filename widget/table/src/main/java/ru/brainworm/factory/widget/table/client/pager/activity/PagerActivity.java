package ru.brainworm.factory.widget.table.client.pager.activity;

import com.google.inject.Inject;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;
import ru.brainworm.factory.widget.table.client.pager.events.PagerEvents;
import ru.brainworm.factory.widget.table.client.pager.factory.AbstractPagerFactory;
import ru.brainworm.factory.widget.table.client.pager.view.PagerView;

import java.util.HashMap;
import java.util.Map;

/**
 * Активити пагинатора
 */
public abstract class PagerActivity implements Activity, AbstractPagerActivity {

    @Event
    public void onShow( PagerEvents.Show eventShow ) {
        AbstractPagerView view = viewFactory.getAbstractPagerView();
        view.setActivity( this );

        eventShow.parent.clear();
        eventShow.parent.add( view.asWidget() );

        PagerViewModel model = new PagerViewModel( eventShow.parent, view, eventShow.limit, 0, eventShow.total );
        showRange( model );

        viewToModel.put( view, model );
    }


    @Event
    public void onChanged( PagerEvents.Change event ) {
        for (PagerViewModel model : viewToModel.values()) {
            if ( event.parent != model.parent ) continue;

            //  Убрать виджет загрузки
            model.parent.clear();
            model.parent.add( model.view.asWidget() );

            //  При изменении диапазона (применении фильтров) начало, может смещаться на первую запись (start = 0)
            model.start = event.start;
            model.total = event.total;

            showRange( model );
        }
    }

    @Override
    public void onBackClicked( PagerView view ) {
        if ( !viewToModel.containsKey( view ) ) return;
        PagerViewModel model = viewToModel.get( view );

        int st = model.start - model.limit;
        if ( st < 0 ) {
            return;
        }
        model.start = st;

        fireEvent( new PagerEvents.RangeChanged( model.parent, model.start, model.limit ) );
    }


    @Override
    public void onForwardClicked( PagerView view ) {
        if ( !viewToModel.containsKey( view ) ) return;
        PagerViewModel model = viewToModel.get( view );

        int st = model.start + model.limit;
        if ( st >= model.total ) {
            return;
        }
        model.start = st;

        fireEvent( new PagerEvents.RangeChanged( model.parent, model.start, model.limit ) );
    }


    @Override
    public void onFirstPageClicked( PagerView view ) {
        if ( !viewToModel.containsKey( view ) ) return;
        PagerViewModel model = viewToModel.get( view );

        if ( model.start < 1 ) return;

        model.start = 0;

        fireEvent( new PagerEvents.RangeChanged( model.parent, model.start, model.limit ) );
    }

    @Override
    public void onLastPageClicked( PagerView view ) {
        if ( !viewToModel.containsKey( view ) ) return;
        PagerViewModel model = viewToModel.get( view );

        if ( model.limit < 1 ) return;
        int st = model.total % model.limit;
        //  Количество записей кратно числу записей на страницу.
        if(st == 0) st = model.limit;

        st = model.total - st;
        if ( st <= 0 || st == model.start) {
            return;
        }
        model.start = st;

        fireEvent( new PagerEvents.RangeChanged( model.parent, model.start, model.limit ) );
    }


    private void showRange( PagerViewModel model ) {
        int end = model.start + model.limit;
        if ( end > model.total ) end = model.total;
        if ( end > 0 ) {
            model.view.setRange( model.start + 1, end, model.total );
        } else {
            model.view.setRange( model.start, end, model.total );
        }
    }


    @Inject
    AbstractPagerFactory viewFactory;

    private Map<AbstractPagerView, PagerViewModel> viewToModel = new HashMap<AbstractPagerView, PagerViewModel>();
}
