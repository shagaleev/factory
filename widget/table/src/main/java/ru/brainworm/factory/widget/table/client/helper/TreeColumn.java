package ru.brainworm.factory.widget.table.client.helper;

import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import ru.brainworm.factory.widget.table.client.ColumnHeader;
import ru.brainworm.factory.widget.table.client.ColumnValue;
import ru.brainworm.factory.widget.table.client.NeedLevelProvider;
import ru.brainworm.factory.widget.table.client.NeedRowRemove;

import java.util.HashMap;
import java.util.Map;

/**
 * Колонка, раскрываемая как дерево
 */
public abstract class TreeColumn<T> {

    /**
     * Обработчик событий колонки
     * @param <T>
     */
    public interface Handler<T> extends AbstractColumnHandler<T> {
        /**
         * Вызывается, когда узел открывается и надо заполнить потомками таблицу
         */
        public void onExpand( T row );

        /**
         * Вызывается, когда узел закрывается и надо удалить потомков из таблицы
         */
        public void onCollapse( T row );
    }

    /**
     * Задает обработчика событий колонки
     * @param eventHandler
     */
    public void setEventHandler( Handler<T> eventHandler ) {
        this.eventHandler = eventHandler;
    }

    /**
     * Режим колонки
     */
    public static enum Mode {
        /**
         * Есть потомки и они видны
         */
        EXPANDED,

        /**
         * Есть потомки и они спрятаны
         */
        COLLAPSED,

        /**
         * Нет потомков
         */
        EMPTY
    }

    /**
     * Возвращает наличие потомков у данной строки
     * @param value    строка
     * @return наличие потомков
     */
    public abstract boolean hasChildren( T value );

    /**
     * Рендерит ячейку
     * @param row      строка
     * @param cell     элемент ячейки
     * @param level    уровень вложенности
     * @param mode     текущий режим
     */
    public abstract void render( T row, Element cell, int level, Mode mode );

    /**
     * Заполнение заголовка столбца
     * @param columnHeader  заголовок
     */
    public abstract void renderHeader( Element columnHeader );

    public class Header implements ColumnHeader {
        @Override
        public void handleEvent( Event event ) {}

        @Override
        public void fillHeader( Element columnHeader ) {
            renderHeader( columnHeader );
        }
    }

    public Header header = new Header();

    public void clearRowModes(){
        if ( values != null ){
            values.rowModes.clear();
        }
    }

    public class Values implements ColumnValue<T>, NeedLevelProvider<T>, NeedRowRemove<T> {
        @Override
        public void handleEvent( Event event, T value ) {
            if ( !"click".equals( event.getType() ) ) {
                return;
            }

            Mode rowMode = rowModes.get( value );
            if ( rowMode == null ) {
                return;
            }

            if ( rowMode == Mode.EMPTY ) {
                return;
            }

            Element cell = event.getEventTarget().cast();
            while ( !cell.getTagName().equalsIgnoreCase( "td" ) ) {
                cell = cell.getParentElement().cast();
            }

            while ( cell.getChildCount() > 0 ) {
                Element child = cell.getChild( 0 ).cast();
                if ( child == null ) {
                    break;
                }

                cell.removeChild( child );
                child.removeFromParent();
            }

            if ( rowMode == Mode.COLLAPSED ) {
                rowModes.put( value, Mode.EXPANDED );
                if ( eventHandler != null ) {
                    eventHandler.onExpand( value );
                }
            }
            else if ( rowMode == Mode.EXPANDED ) {
                rowModes.put( value, Mode.COLLAPSED );
                if ( eventHandler != null ) {
                    eventHandler.onCollapse( value );
                }
            }

            fillValue( cell, value );
        }

        @Override
        public void fillValue( Element cell, T value ) {
            Mode rowMode = rowModes.get( value );
            if ( rowMode == null ) {
                rowMode = Mode.EMPTY;
                if ( hasChildren( value ) ) {
                    rowMode = Mode.COLLAPSED;
                }
                rowModes.put( value, rowMode );
            }

            int level = levelProvider.getLevel( value );

            render( value, cell, level, rowMode );
        }

        @Override
        public void setLevelProvider( LevelProvider<T> levelProvider ) {
            this.levelProvider = levelProvider;
        }

        @Override
        public void onRowRemoved( T row ) {
            rowModes.remove( row );
        }

        private LevelProvider<T> levelProvider;

        protected Map<T, Mode> rowModes = new HashMap<T, Mode>();
    }
    public Values values = new Values();

    private Handler<T> eventHandler;
}
