package ru.brainworm.factory.widget.table.client;

/**
 * Интерфейс, который должен быть реализован колонкой (ColumnValue), если она представляет собой
 * раскрывающуюся на всю ширину строку
 */
public interface IsDetailsRow<T> {
    /**
     * Находится ли колонка в открытом состоянии
     */
    boolean isExpanded( T row );
}
