package ru.brainworm.factory.widget.dataset.client;

import com.google.gwt.user.client.Element;
import ru.brainworm.factory.widget.table.client.TableWidget;
import ru.brainworm.factory.widget.table.client.internal.RowInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Класс представляющий собой набор строк в таблице объединенный с фильтром
 */
public class Dataset<T> {

    public List<T> getRows() {
        return values;
    }

    public int getRowCount() {
        return values.size();
    }

    public void clearRows() {
        if ( table != null ) {
            table.clearRows();
        }
        values.clear();
        rowInfoMap.clear();
    }

    public void refresh() {
        table.clearRows();

        for ( T row : values ) {
            T parent = null;
            for ( Map.Entry<T, RowInfo> rowInfoEntry : rowInfoMap.entrySet() ) {
                if ( rowInfoEntry.getValue().children == null || rowInfoEntry.getValue().children.isEmpty() ) {
                    continue;
                }

                parent = rowInfoEntry.getKey();
                break;
            }

            if ( parent == null ) {
                if ( filter == null || (filter != null && filter.displayRow( row ) ) ) {
                    table.addRow( row );
                }
            }
            else {
                if ( filter == null || ( filter != null && filter.displayRow( parent ) && filter.displayRow( row ) ) ) {
                    table.addChildRow( parent, row );
                }
            }
        }
    }

    public void addRow(T row) {
        values.add( row );

        if ( filter != null ) {
            if ( filter.displayRow( row ) ) {
                table.addRow( row );
            }
        }
        else {
            table.addRow( row );
        }
    }

    public void addChildRow( T parentRow, T childRow ) {
        if ( !values.contains( parentRow ) ) {
            return;
        }

        RowInfo<T> rowInfo = rowInfoMap.get( parentRow );
        if ( rowInfo == null ) {
            rowInfo = new RowInfo<T>();
            rowInfoMap.put( parentRow, rowInfo );
        }
        rowInfo.children.put( childRow, null );

        int parentPos = values.indexOf( parentRow );
        values.add( parentPos + rowInfo.getChildCount(), childRow );

        if ( table != null ) {
            if ( filter == null ) {
                table.addChildRow( parentRow, childRow );
            }
            else if ( filter.displayRow( parentRow ) && filter.displayRow( childRow ) ) {
                table.addChildRow( parentRow, childRow );
            }
        }
    }

    public void removeRow( T row ) {
        RowInfo<T> rowInfo = rowInfoMap.remove( row );
        if ( rowInfo != null ) {
            for ( Map.Entry<T, Element> childEntry : rowInfo.children.entrySet() ) {
                removeRow( childEntry.getKey() );
            }
        }

        List<T> removeRowInfoList = new ArrayList<T>();
        for ( Map.Entry<T, RowInfo> tRowInfoEntry : rowInfoMap.entrySet() ) {
            RowInfo<T> tRowInfo = tRowInfoEntry.getValue();
            if ( tRowInfo.children == null ) {
                continue;
            }

            tRowInfo.children.remove( row );

            if ( tRowInfo.getChildCount() == 0 ) {
                removeRowInfoList.add( tRowInfoEntry.getKey() );
            }
        }

        for ( T t : removeRowInfoList ) {
            rowInfoMap.remove( t );
        }

        values.remove( row );
        if ( table != null ) {
            table.removeRow( row );
        }
    }

    public void removeChildren( T row ) {
        RowInfo<T> rowInfo = rowInfoMap.get( row );
        if ( rowInfo != null ) {
            for ( Map.Entry<T, Element> childEntry : rowInfo.children.entrySet() ) {
                removeRow( childEntry.getKey() );
            }
        }
        rowInfo.children.clear();
    }

    public void updateRow( T row ) {
        if ( filter != null ) {
            if ( filter.displayRow( row ) ) {
                table.updateRow( row );
            }
        }
        else {
            table.updateRow( row );
        }

        int pos = values.indexOf( row );
        if ( pos >= 0 && pos < values.size() ) {
            values.remove( pos );
            values.add( pos, row );
        }
    }

    public void setTable( TableWidget<T> table ) {
        this.table = table;
    }

    public void setFilter( AbstractFilter<T> filter ) {
        this.filter = filter;
    }

    Map<T, RowInfo> rowInfoMap = new HashMap<T, RowInfo>();
    List<T> values = new ArrayList<T>();

    TableWidget<T> table;
    AbstractFilter<T> filter = null;
}
