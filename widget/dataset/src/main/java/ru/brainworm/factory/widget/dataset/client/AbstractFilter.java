package ru.brainworm.factory.widget.dataset.client;

/**
 * Абстрактный фильтр для строк в таблице
 */
public interface AbstractFilter<T> {
    /**
     * Метод должен возвращать true - если строка должна быть показана
     * @param value    строка
     */
    boolean displayRow( T value );
}
