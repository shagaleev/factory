package ru.brainworm.factory.widget.leaflet.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class MapClickEvent extends GwtEvent<MapClickHandler> {
    private static Type<MapClickHandler> TYPE;
    private double lat, lng;

    @Override
    public Type<MapClickHandler> getAssociatedType() {
        return TYPE;
    }

    public static Type<MapClickHandler> getType() {
        if ( TYPE == null ) {
            TYPE = new Type<>();
        }
        return TYPE;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public MapClickEvent( double lat, double lng ) {
        this.lat = lat;
        this.lng = lng;
    }

    @Override
    protected void dispatch( MapClickHandler handler ) {
        handler.onClick( this );
    }
}
