package ru.brainworm.factory.widget.leaflet.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class MapResourcesReadyEvent extends GwtEvent<MapResourcesReadyHandler> {
    private static Type<MapResourcesReadyHandler> TYPE;

    @Override
    public Type<MapResourcesReadyHandler> getAssociatedType() {
        return TYPE;
    }

    public static Type<MapResourcesReadyHandler> getType() {
        if ( TYPE == null ) {
            TYPE = new Type<>();
        }
        return TYPE;
    }

    @Override
    protected void dispatch( MapResourcesReadyHandler handler ) {
        handler.onResourcesReady( this );
    }
}
