package ru.brainworm.factory.widget.leaflet.client.event.lasso;

import com.google.gwt.event.shared.GwtEvent;

public class LassoSelectionEnabledEvent extends GwtEvent<LassoSelectionEnabledHandler> {

    @Override
    public Type<LassoSelectionEnabledHandler> getAssociatedType() {
        return TYPE;
    }

    public static Type<LassoSelectionEnabledHandler> getType() {
        if (TYPE == null) {
            TYPE = new Type<LassoSelectionEnabledHandler>();
        }
        return TYPE;
    }


    @Override
    protected void dispatch(LassoSelectionEnabledHandler handler) {
        handler.onSelectEnabled( this );
    }

    private static Type<LassoSelectionEnabledHandler> TYPE;
}