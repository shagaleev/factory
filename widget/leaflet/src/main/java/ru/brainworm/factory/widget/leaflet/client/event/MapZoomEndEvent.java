package ru.brainworm.factory.widget.leaflet.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class MapZoomEndEvent extends GwtEvent<MapZoomEndHandler> {
    private static Type<MapZoomEndHandler> TYPE;

    @Override
    public Type<MapZoomEndHandler> getAssociatedType() {
        return TYPE;
    }

    public static Type<MapZoomEndHandler> getType() {
        if ( TYPE == null ) {
            TYPE = new Type<>();
        }
        return TYPE;
    }

    @Override
    protected void dispatch( MapZoomEndHandler handler ) {
        handler.onZoomEnd( this );
    }
}
