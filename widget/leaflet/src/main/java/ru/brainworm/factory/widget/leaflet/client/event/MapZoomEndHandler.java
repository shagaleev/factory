package ru.brainworm.factory.widget.leaflet.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface MapZoomEndHandler extends EventHandler {
    void onZoomEnd(MapZoomEndEvent event);
}
