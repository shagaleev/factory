package ru.brainworm.factory.widget.leaflet.client.event.lasso;

import com.google.gwt.event.shared.EventHandler;

public interface LassoSelectionDisabledHandler extends EventHandler {
    void onSelectDisabled(LassoSelectionDisabledEvent event);
}
