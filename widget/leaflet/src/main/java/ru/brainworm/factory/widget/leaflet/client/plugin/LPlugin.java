package ru.brainworm.factory.widget.leaflet.client.plugin;

import com.gwidgets.api.leaflet.LatLng;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;
import ru.brainworm.factory.widget.leaflet.client.plugin.lasso.LassoControl;
import ru.brainworm.factory.widget.leaflet.client.plugin.semicircle.SemiCircle;
import ru.brainworm.factory.widget.leaflet.client.plugin.semicircle.SemiCircleOptions;

/**
 * @see com.gwidgets.api.leaflet.L
 */
@JsType(
        isNative = true,
        name = "L",
        namespace = JsPackage.GLOBAL
)
public class LPlugin {

    public static native SemiCircle semiCircle(LatLng latlng, SemiCircleOptions options);

    @JsType(
            isNative = true,
            name = "L.control",
            namespace = JsPackage.GLOBAL
    )
    public static class LControlPlugin {

        public static native LassoControl lasso();
    }
}
