package ru.brainworm.factory.widget.leaflet.client.event.lasso;

import com.google.gwt.event.shared.EventHandler;

public interface LassoSelectionFinishedHandler extends EventHandler {
    void onSelectFinished(LassoSelectionFinishedEvent event);
}
