package ru.brainworm.factory.widget.leaflet.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface MapMoveEndHandler extends EventHandler {
    void onMoveEnd(MapMoveEndEvent event);
}
