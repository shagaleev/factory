package ru.brainworm.factory.widget.leaflet.client;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.gwidgets.api.leaflet.L;
import com.gwidgets.api.leaflet.Map;
import com.gwidgets.api.leaflet.TileLayer;
import com.gwidgets.api.leaflet.events.EventTypes;
import com.gwidgets.api.leaflet.events.MouseEvent;
import com.gwidgets.api.leaflet.options.MapOptions;
import ru.brainworm.factory.widget.leaflet.client.event.*;
import ru.brainworm.factory.widget.leaflet.client.event.lasso.*;
import ru.brainworm.factory.widget.leaflet.client.plugin.LeafletResources;
import ru.brainworm.factory.widget.leaflet.client.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Виджет карты
 */
public class LeafletWidget extends ComplexPanel implements HasHandlers {
    // Used to generate unique element ids for Ace widgets.
    private static int nextId = 0;
    private final String elementId;
    private double lat = 0.0;
    private double lng = 0.0;
    private double zoom, minZoom, maxZoom;
    private String mapUrl = "";
    private boolean isDebug = false;
    private List<Plugin> pluginList = new ArrayList<Plugin>();

    private Map map;

    private final Timer windowResizeTimer = new Timer() {
        @Override
        public void run() {
            invalidateSize();
        }
    };

    @SuppressWarnings( "deprecation" )
    public LeafletWidget() {
        Element mapContainer = DOM.createDiv();
        setElement( mapContainer );
        elementId = "_leafletMap" + nextId;
        nextId++;
        Element element = DOM.createDiv();
        element.getStyle().setHeight( 100, Style.Unit.PCT );
        element.getStyle().setWidth( 100, Style.Unit.PCT );
        element.setId( elementId );
        mapContainer.appendChild( element );
        Window.addResizeHandler( event -> {
            windowResizeTimer.cancel();
            windowResizeTimer.schedule(150);
        });

    }

    public HandlerRegistration addMapClickHandler( MapClickHandler handler ) {
        return addHandler( handler, MapClickEvent.getType() );
    }

    public HandlerRegistration addMapZoomEndHandler(MapZoomEndHandler handler) {
        return addHandler(handler, MapZoomEndEvent.getType());
    }

    public HandlerRegistration addMapMoveEndHandler(MapMoveEndHandler handler) {
        return addHandler(handler, MapMoveEndEvent.getType());
    }

    public HandlerRegistration addMapResourcesReadyHandler(MapResourcesReadyHandler handler) {
        return addHandler(handler, MapResourcesReadyEvent.getType());
    }

    public HandlerRegistration addMapResourcesReadyHandler(LassoSelectionFinishedHandler handler) {
        return addHandler(handler, LassoSelectionFinishedEvent.getType());
    }

    public HandlerRegistration addMapResourcesReadyHandler(LassoSelectionEnabledHandler handler) {
        return addHandler(handler, LassoSelectionEnabledEvent.getType());
    }

    public HandlerRegistration addMapResourcesReadyHandler(LassoSelectionDisabledHandler handler) {
        return addHandler(handler, LassoSelectionDisabledEvent.getType());
    }

    public void setLat( double lat ) {
        this.lat = lat;
    }

    public void setLng( double lng ) {
        this.lng = lng;
    }

    public void setMapUrl( String mapUrl ) {
        this.mapUrl = mapUrl;
    }

    public void setZoom( double zoom ) {
        this.zoom = zoom;
    }

    public void setMinZoom( double minZoom ) {
        this.minZoom = minZoom;
    }

    public void setMaxZoom( double maxZoom ) {
        this.maxZoom = maxZoom;
    }

    public void setDebug(boolean debug) {
        isDebug = debug;
    }

    public void setEnablePlugins(String plugins) {
        pluginList.clear();
        String[] pluginsArray = plugins.split("[, ]+");
        for (String plugin : pluginsArray) {
            pluginList.add(Plugin.valueOf(plugin));
        }
    }

    public boolean containsPlugin(Plugin plugin) {

        if (pluginList == null || pluginList.isEmpty()){
            return false;
        }

        return pluginList.contains(plugin);
    }

    public void setEnablePlugins(Set<Plugin> plugins) {
        pluginList.clear();
        pluginList.addAll(plugins);
    }

    public void invalidateSize() {
        if ( map != null )
            map.invalidateSize( true );
    }

    public Map getMapElement() {
        return map;
    }

    public void setView( double lat, double lng ) {
        this.lat = lat;
        this.lng = lng;

        if ( isMapLoaded() ) {
            map.setView( L.latLng( lat, lng ), zoom, null );
        }
    }

    @Override
    protected void onLoad() {
        initMap();
    }

    private void initMap() {
        destroyMap();
        LeafletResources.load(isDebug, pluginList, () -> {
            //Map options required values: center point, zoom, and a min zoom
            MapOptions options = new MapOptions.Builder( L.latLng( lat, lng ), zoom, minZoom )
                    .maxZoom( maxZoom ).build();

            map = L.map( elementId, options );
            map.setView( L.latLng( lat, lng ), zoom, null );

            TileLayer tLayer = L.tileLayer( mapUrl, null );
            tLayer.addTo( map );

            map.on( EventTypes.MapEvents.CLICK, ( event ) -> {
                MouseEvent me = ( MouseEvent ) event;
                fireEvent(new MapClickEvent(me.getLatlng().lat, me.getLatlng().lng));
            } );

            map.on( EventTypes.MapEvents.ZOOMEND, event -> fireEvent(new MapZoomEndEvent()));
            map.on(EventTypes.MapEvents.MOVEEND, event -> fireEvent(new MapMoveEndEvent()));

            if (pluginList.contains(Plugin.LASSO)){
                sibscribeLassoEvents();
            }

            fireEvent(new MapResourcesReadyEvent());
        });
        windowResizeTimer.cancel();
        windowResizeTimer.schedule(500);
    }

    private void sibscribeLassoEvents() {
        map.on(EventType.LassoEvents.FINISHED_EVENT,
                event -> fireEvent(new LassoSelectionFinishedEvent( ((LassoSelectionFinishedJsEvent)event).getLayers() )));

        map.on( EventType.LassoEvents.ENABLED_EVENT,
                event -> fireEvent(new LassoSelectionEnabledEvent()));

        map.on( EventType.LassoEvents.DISABLED_EVENT,
                event -> fireEvent(new LassoSelectionDisabledEvent()));
    }

    private void destroyMap() {
        if ( map == null ) {
            return;
        }
        map.off();
        map.remove();
    }

    private boolean isMapLoaded() {
        return map != null;
    }
}
