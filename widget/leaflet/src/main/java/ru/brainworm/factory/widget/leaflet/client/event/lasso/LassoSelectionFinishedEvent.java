package ru.brainworm.factory.widget.leaflet.client.event.lasso;

import com.google.gwt.event.shared.GwtEvent;
import com.gwidgets.api.leaflet.Marker;

public class LassoSelectionFinishedEvent extends GwtEvent<LassoSelectionFinishedHandler> {

    public LassoSelectionFinishedEvent(Marker[] layers) {
        this.layers = layers;
    }

    @Override
    public Type<LassoSelectionFinishedHandler> getAssociatedType() {
        return TYPE;
    }

    public static Type<LassoSelectionFinishedHandler> getType() {
        if (TYPE == null) {
            TYPE = new Type<LassoSelectionFinishedHandler>();
        }
        return TYPE;
    }


    @Override
    protected void dispatch(LassoSelectionFinishedHandler handler) {
        handler.onSelectFinished( this );
    }

    public Marker[] getLayers() {
        return layers;
    }

    private static Type<LassoSelectionFinishedHandler> TYPE;
    private Marker[] layers;
}
