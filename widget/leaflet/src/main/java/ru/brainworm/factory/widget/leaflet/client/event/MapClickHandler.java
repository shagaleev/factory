package ru.brainworm.factory.widget.leaflet.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface MapClickHandler extends EventHandler {
    void onClick( MapClickEvent event );
}
