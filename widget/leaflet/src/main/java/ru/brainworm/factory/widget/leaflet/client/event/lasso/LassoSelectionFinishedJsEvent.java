package ru.brainworm.factory.widget.leaflet.client.event.lasso;

import com.gwidgets.api.leaflet.LatLng;
import com.gwidgets.api.leaflet.Marker;
import com.gwidgets.api.leaflet.events.Event;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(isNative=true, name="Object", namespace=jsinterop.annotations.JsPackage.GLOBAL)
public class LassoSelectionFinishedJsEvent extends Event {

    protected LassoSelectionFinishedJsEvent() {}

    @JsProperty
    public final native Marker[] getLayers();

    @JsProperty
    public final native LatLng[] getLatLngs();

}
