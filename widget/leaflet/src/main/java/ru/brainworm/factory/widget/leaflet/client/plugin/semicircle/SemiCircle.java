package ru.brainworm.factory.widget.leaflet.client.plugin.semicircle;

import com.gwidgets.api.leaflet.Circle;
import jsinterop.annotations.JsType;

/**
 * @see SemiCircleOptions
 * @see Circle
 */
@JsType(isNative = true)
public class SemiCircle extends Circle {

    protected SemiCircle() {
        super();
    }
}
