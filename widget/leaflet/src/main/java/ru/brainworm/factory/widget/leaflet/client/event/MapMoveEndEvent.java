package ru.brainworm.factory.widget.leaflet.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class MapMoveEndEvent extends GwtEvent<MapMoveEndHandler> {
    private static Type<MapMoveEndHandler> TYPE;

    @Override
    public Type<MapMoveEndHandler> getAssociatedType() {
        return TYPE;
    }

    public static Type<MapMoveEndHandler> getType() {
        if ( TYPE == null ) {
            TYPE = new Type<>();
        }
        return TYPE;
    }

    @Override
    protected void dispatch( MapMoveEndHandler handler ) {
        handler.onMoveEnd( this );
    }
}
