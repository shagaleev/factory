package ru.brainworm.factory.widget.leaflet.client.event;

public class EventType {

    public static class LassoEvents {
        public static final String ENABLED_EVENT = "lasso.enabled";
        public static final String DISABLED_EVENT = "lasso.disabled";
        public static final String FINISHED_EVENT = "lasso.finished";
    }
}
