package ru.brainworm.factory.widget.leaflet.client.plugin;

public enum Plugin {
    SEMICIRCLE("/leaflet/plugin/leaflet-plugin-semicircle.js"),
    LASSO("/leaflet/plugin/leaflet-lasso.umd.js"),
    ;

    Plugin(String relativePath) {
        this.relativePath = relativePath;
    }

    private final String relativePath;

    public String getRelativePath() {
        return relativePath;
    }
}
