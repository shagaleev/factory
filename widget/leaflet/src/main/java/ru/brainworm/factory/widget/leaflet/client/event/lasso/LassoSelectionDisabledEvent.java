package ru.brainworm.factory.widget.leaflet.client.event.lasso;

import com.google.gwt.event.shared.GwtEvent;

public class LassoSelectionDisabledEvent extends GwtEvent<LassoSelectionDisabledHandler> {

    @Override
    public Type<LassoSelectionDisabledHandler> getAssociatedType() {
        return TYPE;
    }

    public static Type<LassoSelectionDisabledHandler> getType() {
        if (TYPE == null) {
            TYPE = new Type<LassoSelectionDisabledHandler>();
        }
        return TYPE;
    }


    @Override
    protected void dispatch(LassoSelectionDisabledHandler handler) {
        handler.onSelectDisabled( this );
    }

    private static Type<LassoSelectionDisabledHandler> TYPE;
}
