package ru.brainworm.factory.widget.leaflet.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface MapResourcesReadyHandler extends EventHandler {
    void onResourcesReady(MapResourcesReadyEvent event);
}
