package ru.brainworm.factory.widget.leaflet.client.plugin;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.ScriptInjector;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LeafletResources {

    public static void load(boolean debug, List<Plugin> pluginList, Runnable onLoaded) {
        com.gwidgets.api.leaflet.utils.LeafletResources.whenReady(debug, ev -> {
            loadPlugins(pluginList, onLoaded);
            return null;
        });
    }

    public static void loadPlugins(List<Plugin> pluginList, final Runnable onLoaded) {

        if (pluginList == null || pluginList.isEmpty()) {
            onLoaded.run();
            return;
        }

        final Map<String, Boolean> pluginLoadState = new HashMap<String, Boolean>();

        for (final Plugin plugin : pluginList) {
            pluginLoadState.put(plugin.getRelativePath(), false);
        }

        for (final Plugin plugin : pluginList) {
            ScriptInjector.fromUrl(GWT.getModuleName() + plugin.getRelativePath())
                    .setCallback(new Callback<Void, Exception>() {
                        @Override
                        public void onFailure(Exception e) {
                            consoleError("Failed to load leaflet '" + plugin.toString().toLowerCase() + "' plugin");
                        }
                        @Override
                        public void onSuccess(Void aVoid) {
                            pluginLoadState.put(plugin.getRelativePath(), true);
                            for (Boolean state : pluginLoadState.values()) {
                                if (!state) {
                                    return;
                                }
                            }
                            onLoaded.run();
                        }
                    })
                    .setRemoveTag(false)
                    .setWindow(getWindow())
                    .inject();
        }
    }

    public static native JavaScriptObject getWindow() /*-{
        return $wnd;
    }-*/;

    public static native void consoleError(String message)/*-{
        console.error(message);
    }-*/;
}
