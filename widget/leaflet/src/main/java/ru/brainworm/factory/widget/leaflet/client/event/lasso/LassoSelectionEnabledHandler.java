package ru.brainworm.factory.widget.leaflet.client.event.lasso;

import com.google.gwt.event.shared.EventHandler;

public interface LassoSelectionEnabledHandler extends EventHandler {
    void onSelectEnabled(LassoSelectionEnabledEvent event);
}
