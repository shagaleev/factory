package ru.brainworm.factory.test.runner.server;

import com.google.inject.Binding;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.internal.Errors;
import org.jukito.All;
import org.jukito.GuiceUtils;
import org.jukito.InjectedFrameworkMethod;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class InjectedStatement extends Statement {

    private final FrameworkMethod method;
    private final Object test;
    private final Injector injector;

    public InjectedStatement( FrameworkMethod method, Object test, Injector injector ) {
        this.method = method;
        this.test = test;
        this.injector = injector;
    }

    @Override
    public void evaluate() throws Throwable {
        Method javaMethod = method.getMethod();

        Errors errors = new Errors(javaMethod);
        List<Key<?>> keys = GuiceUtils.getMethodKeys( javaMethod, errors );
        errors.throwConfigurationExceptionIfErrorsExist();

        Iterator<Binding<?>> bindingIter;
        if (InjectedFrameworkMethod.class.isAssignableFrom(method.getClass())) {
            bindingIter = ((InjectedFrameworkMethod) method).getBindingsToUseForParameters().iterator();
        } else {
            bindingIter = new ArrayList<Binding<?>>().iterator();
        }

        List<Object> injectedParameters = new ArrayList<Object>();
        for (Key<?> key : keys) {
            if (!All.class.equals(key.getAnnotationType())) {
                injectedParameters.add(injector.getInstance(key));
            } else {
                if (!bindingIter.hasNext()) {
                    throw new AssertionError("Expected more bindings to fill @All parameters.");
                }
                injectedParameters.add(injector.getInstance(bindingIter.next().getKey()));
            }
        }

        method.invokeExplosively(test, injectedParameters.toArray());
    }
}