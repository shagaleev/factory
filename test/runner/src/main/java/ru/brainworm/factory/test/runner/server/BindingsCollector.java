package ru.brainworm.factory.test.runner.server;

import com.google.inject.AbstractModule;
import com.google.inject.Binding;
import com.google.inject.Key;
import com.google.inject.Scope;
import com.google.inject.spi.*;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

public class BindingsCollector {

    private final AbstractModule module;
    private final List<BindingInfo> bindingsObserved = new ArrayList<BindingInfo>();

    public BindingsCollector( AbstractModule module ) {
        this.module = module;
    }

    public void collectBindings() {
        GuiceElementVisitor visitor = new GuiceElementVisitor();
        visitor.visitElements( Elements.getElements( module ));

        // TODO report errors?
    }

    public List<BindingInfo> getBindingsObserved() {
        return bindingsObserved;
    }

    /**
     * Information on a binding, used by Jukito to identify provided keys and needed keys.
     */
    public static class BindingInfo {

        public static BindingInfo create(Binding<?> binding, Key<?> boundKey,
                                         Object instance) {
            BindingInfo bindingInfo = new BindingInfo();
            bindingInfo.key = binding.getKey();
            bindingInfo.boundKey = boundKey;
            bindingInfo.boundInstance = instance;
            bindingInfo.scope = binding.acceptScopingVisitor(new GuiceScopingVisitor());
            return bindingInfo;
        }

        public Key<?> key;
        public Key<?> boundKey;
        public Object boundInstance;
        public String scope;

        @Override
        public String toString() {
            return "BindingInfo{" +
                    "key=" + key +
                    ", boundKey=" + boundKey +
                    ", boundInstance=" + boundInstance +
                    ", scope='" + scope + '\'' +
                    '}';
        }
    }

    private final List<Message> messages = new ArrayList<Message>();

    /**
     * This visitor collects all information on various guice elements.
     */
    public class GuiceElementVisitor extends DefaultElementVisitor<Void> {

        private void visitElements(List<Element> elements) {
            for (Element element : elements) {
                element.acceptVisitor(this);
            }
        }

        @Override
        public <T> Void visit(Binding<T> command) {
            GuiceBindingVisitor<T> bindingVisitor = new GuiceBindingVisitor<T>();
            command.acceptTargetVisitor(bindingVisitor);
            return null;
        }

        @Override
        public Void visit(Message message) {
            messages.add(message);
            return null;
        }
    }

    /**
     * This visitor collects all information on guice bindings.
     */
    public class GuiceBindingVisitor<T> extends DefaultBindingTargetVisitor<T, Void> {

        private Void addBindingInfo(Binding<? extends T> binding, Key<?> boundKey, Object instance) {
            bindingsObserved.add( BindingInfo.create( binding, boundKey, instance ));
            return null;
        }

        private Void addBinding(Binding<? extends T> binding) {
            return addBindingInfo(binding, binding.getKey(), null);
        }

        private Void addBindingKey(Binding<? extends T> binding, Key<?> boundKey) {
            return addBindingInfo(binding, boundKey, null);
        }

        private Void addBindingInstance(Binding<? extends T> binding, Object instance) {
            return addBindingInfo(binding, null, instance);
        }

        @Override
        public Void visit(ProviderBinding<? extends T> providerBinding) {
            return addBindingKey(providerBinding, providerBinding.getProvidedKey());
        }

        @Override
        public Void visit(ProviderKeyBinding<? extends T> providerKeyBinding) {
            return addBindingKey(providerKeyBinding, providerKeyBinding.getProviderKey());
        }

        @Override
        public Void visit(ProviderInstanceBinding<? extends T> providerInstanceBinding) {
            return addBindingInstance(providerInstanceBinding,
                    providerInstanceBinding.getProviderInstance());
        }

        @Override
        public Void visit(InstanceBinding<? extends T> instanceBinding) {
            return addBindingInstance(instanceBinding, instanceBinding.getInstance());
        }

        @Override
        public Void visit(ConvertedConstantBinding<? extends T> constantBinding) {
            return addBindingInstance(constantBinding, constantBinding.getValue());
        }

        @Override
        public Void visit(UntargettedBinding<? extends T> untargettedBinding) {
            return addBinding(untargettedBinding);
        }

        @Override
        public Void visit(LinkedKeyBinding<? extends T> linkedKeyBinding) {
            return addBindingKey(linkedKeyBinding, linkedKeyBinding.getLinkedKey());
        }

        @Override
        public Void visit(ConstructorBinding<? extends T> constructorBinding) {
            return addBinding(constructorBinding);
        }
    }

    /**
     * This visitor collects all information on guice scopes associated to the bindings.
     */
    public static class GuiceScopingVisitor extends DefaultBindingScopingVisitor<String> {

        @Override
        public String visitEagerSingleton() {
            return "EagerSingleton";
        }

        @Override
        public String visitScope(Scope scope) {
            return scope.toString();
        }

        @Override
        public String visitScopeAnnotation(Class<? extends Annotation> scopeAnnotation) {
            return scopeAnnotation.getCanonicalName();
        }
    }

}
