package ru.brainworm.factory.test.runner.server;

import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.Scope;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shagaleev
 * Date: 7/21/14
 * Time: 9:53 AM
 * To change this template use File | Settings | File Templates.
 */
public class TestScope {

    private static class Singleton implements Scope {
        private final String simpleName;

        private final Map<Key<?>, Object> backingMap = new HashMap<Key<?>, Object>();

        public Singleton(String simpleName) {
            this.simpleName = simpleName;
        }

        public void clear() {
            backingMap.clear();
        }

        @Override
        public <T> Provider<T> scope(final Key<T> key, final Provider<T> unscoped) {
            return new Provider<T>() {
                @SuppressWarnings("unchecked")
                public T get() {

                    Object o = backingMap.get(key);

                    if (o == null) {
                        o = unscoped.get();
                        backingMap.put(key, o);
                    }
                    return (T) o;
                }
            };
        }

        public String toString() {
            return simpleName;
        }
    }

    /**
     * Test-scoped singletons are typically used in test cases for objects that
     * correspond to singletons in the application. Your JUnit test case must use
     * {@link org.jukito.JukitoRunner} on its {@code @RunWith} annotation so that
     * test-scoped singletons are reset before every test case.
     * <p />
     * If you want your singleton to be instantiated automatically with each new
     * test, use {@link #EAGER_SINGLETON}.
     */
    public static final Singleton SINGLETON = new Singleton("TestSingleton");

    /**
     * Eager test-scoped singleton are similar to test-scoped {@link #SINGLETON}
     * but they get instantiated automatically with each new test.
     */
    public static final Singleton EAGER_SINGLETON = new Singleton("EagerTestSingleton");

    /**
     * Clears all the instances of test-scoped singletons. After this method is
     * called, any "singleton" bound to this scope that had already been created
     * will be created again next time it gets injected.
     */
    public static void clear() {
        SINGLETON.clear();
        EAGER_SINGLETON.clear();
    }

}
