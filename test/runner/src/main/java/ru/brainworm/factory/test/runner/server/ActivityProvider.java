package ru.brainworm.factory.test.runner.server;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import ru.brainworm.factory.core.bus.client.Bus;
import ru.brainworm.factory.core.bus.client.BusContainer;
import ru.brainworm.factory.generator.activity.client.annotations.Event;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Провайдер прокси объектов для всех активитей
 */
public class ActivityProvider<T> implements Provider<T> {

    public static interface InjectorProvider {

        Injector getInjector();
    }

    public ActivityProvider( InjectorProvider injectorProvider, Class<T> rawType ) {
        this.injectorProvider = injectorProvider;
        this.rawType = rawType;
    }

    @Override
    public T get() {
        try {
            Enhancer enhancer = new Enhancer();
            enhancer.setSuperclass( rawType );
            enhancer.setCallback( new MethodInterceptor() {
                @Override
                public Object intercept( Object o, Method method, Object[] objects, MethodProxy methodProxy ) throws Throwable {
                    if ( method.getName().equals( "fireEvent" ) ) {
                        Bus bus = BusContainer.getBus();
                        bus.fireEvent( objects[ 0 ] );
                        return null;
                    } else if ( method.getName().equals( "goTo" ) ) {
                        return null;
                    } else {
                        return methodProxy.invokeSuper( o, objects );
                    }
                }
            } );

            final T object = (T) enhancer.create();
            for ( Field field : rawType.getDeclaredFields() ) {
                if ( field.getAnnotation( Inject.class ) != null ) {
                    Class fieldType = field.getType();
                    Object fieldValue = injectorProvider.getInjector().getInstance( fieldType );
                    field.setAccessible( true ); // да, мы хотим обработать поле с любым модификатором доступа
                    field.set( object, fieldValue );
                }
            }

            for ( Method method : rawType.getDeclaredMethods() ) {
                if ( method.getAnnotation( Event.class ) != null ) {

                    final Method finalMethod = method;

                    Bus bus = BusContainer.getBus();
                    bus.subscribe( method.getParameterTypes()[ 0 ], new Bus.Subscriber() {
                        @Override
                        public void update( Object event ) {
                            try {
                                finalMethod.invoke( object, event );
                            } catch (Throwable th) {
                                th.printStackTrace();
                            }
                        }
                    } );
                }
            }

            return object;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    InjectorProvider injectorProvider;

    Class<?> rawType;
}
