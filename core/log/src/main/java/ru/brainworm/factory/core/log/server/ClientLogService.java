package ru.brainworm.factory.core.log.server;

import com.google.gwt.logging.shared.RemoteLoggingService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Сервис проброса логов на сервер через slf4j логгер
 */
public class ClientLogService extends RemoteServiceServlet implements
        RemoteLoggingService {

    @Override
    public String logOnServer( LogRecord logRecord ) {
        if ( logRecord == null ) {
            return null;
        }

        Logger log = getLogger( logRecord.getLoggerName() );

        String message = buildMessage( logRecord );
        int levelValue = logRecord.getLevel().intValue();

        try {
            if ( levelValue <= TRACE_LEVEL_THRESHOLD ) {
                log.trace( message, logRecord.getThrown() );
            } else if ( levelValue <= DEBUG_LEVEL_THRESHOLD ) {
                log.debug( message, logRecord.getThrown() );
            } else if ( levelValue <= INFO_LEVEL_THRESHOLD ) {
                log.info( message, logRecord.getThrown() );
            } else if ( levelValue <= WARN_LEVEL_THRESHOLD ) {
                log.warn( message, logRecord.getThrown() );
            } else {
                log.error( message, logRecord.getThrown() );
            }
        } catch ( Exception e ) {
            log.error( "Remote logging failed", e );
            return "Remote logging failed, check stack trace for details.";
        }
        return null;
    }

    private String buildMessage( LogRecord logRecord ) {
        HttpServletRequest request = this.getThreadLocalRequest();

        StringBuilder sb = new StringBuilder();
        sb.append( "[ sid = " ).append( request.getSession().getId() )
                .append( ", ip = " ).append( request.getRemoteAddr() )
                .append( " ] : " ).append( logRecord.getMessage() );

        return sb.toString();
    }

    private Logger getLogger( String juLoggerName ) {
        Logger logger = loggerNameToLogger.get( juLoggerName );

        if ( logger == null ) {
            logger = LoggerFactory.getLogger( juLoggerName );
            loggerNameToLogger.put( juLoggerName, logger );
        }

        return logger;
    }

    private static final int TRACE_LEVEL_THRESHOLD = Level.FINEST.intValue();
    private static final int DEBUG_LEVEL_THRESHOLD = Level.FINE.intValue();
    private static final int INFO_LEVEL_THRESHOLD = Level.INFO.intValue();
    private static final int WARN_LEVEL_THRESHOLD = Level.WARNING.intValue();

    private Map<String, Logger> loggerNameToLogger = new HashMap< String, Logger >();
}