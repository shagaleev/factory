package ru.brainworm.factory.core.router.client;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.History;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Роутер из адресной строки
 */
public class Router implements ValueChangeHandler<String> {

    /**
     * Подписчик на изменения в модели данных
     */
    public static interface Subscriber {

        /**
         * Вызывается, когда изменились данные в модели
         */
        void onHistoryChanged( Map<String, String> params );
    }

    /**
     * Подписываемся на изменения в классе
     * @param key           паттерн для роутера
     * @param subscriber    получатель изменений
     */
    public void subscribe( String key, Subscriber subscriber ) {
        RegExp regExp = patterns.get( key );
        if ( regExp == null ) {
            regExp = RegExp.compile( key );
            patterns.put( key, regExp );
        }

        List<Subscriber> subscriberList = subscribers.get( regExp );
        if ( subscriberList == null ) {
            subscriberList = new ArrayList<Subscriber>();
            subscribers.put( regExp, subscriberList );
        }

        subscriberList.add( subscriber );
    }

    /**
     * Переход на указанную строчку
     * @param url
     */
    public void goTo( String url ) {
        History.newItem( url );
    }

    public Router() {
        History.addValueChangeHandler( this );

        Scheduler.get().scheduleDeferred( new Command() {
            @Override
            public void execute() {
                History.fireCurrentHistoryState();
            }
        } );

    }

    @Override
    public void onValueChange( ValueChangeEvent<String> event ) {

        String address = null;
        Map<String, String> paramMap = new HashMap<String, String>();

        String token = event.getValue();

        int questionPos = token.indexOf( "?" );
        if ( questionPos < 0 ) {
            address = token;
        }
        else {
            address = token.substring( 0, questionPos );
            paramMap = parseParamMap( token.substring( questionPos+1 ) );
        }

        for ( Map.Entry<RegExp, List<Subscriber>> entry : subscribers.entrySet() ) {
            RegExp regExp = entry.getKey();
            MatchResult result = regExp.exec( address );
            if ( result == null ) {
                continue;
            }

            final List<Subscriber> subscriberList = entry.getValue();
            final Map<String, String> params = paramMap;
            Scheduler.get().scheduleDeferred( new Command() {
                @Override
                public void execute() {
                    for ( Subscriber subscriber : subscriberList ) {
                        subscriber.onHistoryChanged( params );
                    }
                }
            } );
        }
    }

    private Map<String, String> parseParamMap( String params ) {
        Map<String, String> result = new HashMap<String, String>();

        String[] paramList = params.split( "&" );

        for (String paramValue : paramList) {
            String[] paramItems = paramValue.split( "=" );

            result.put(paramItems[0], paramItems[1]);
        }

        return result;
    }

    Map<RegExp, List< Subscriber >> subscribers = new HashMap<RegExp, List<Subscriber>>();
    Map<String, RegExp> patterns = new HashMap<String, RegExp>();
}
