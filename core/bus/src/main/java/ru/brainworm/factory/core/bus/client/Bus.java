package ru.brainworm.factory.core.bus.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Шина данных для событий
 */
public class Bus {

    /**
     * Подписчик на изменения в модели данных
     */
    public static interface Subscriber<T> {

        /**
         * Вызывается, когда изменились данные в модели
         */
        void update( T value );
    }

    /**
     * Подписываемся на изменения в классе
     * @param key
     * @param subscriber
     * @param <T>
     */
    public <T> void subscribe( Class<T> key, Subscriber<T> subscriber ) {
        List<Subscriber> subscriberList = subscribers.get( key );
        if ( subscriberList == null ) {
            subscriberList = new ArrayList<Subscriber>();
            subscribers.put( key, subscriberList );
        }

        subscriberList.add( subscriber );
    }

    /**
     * Отписаться
     * @param key
     * @param subscriber
     * @param <T>
     */
    public <T> void unsubscribe( Class<T> key, Subscriber subscriber ) {
        List<Subscriber> subscriberList = subscribers.get( key );
        if ( subscriberList == null ) {
            return;
        }

        subscriberList.remove( subscriber );
        if ( subscriberList.isEmpty() ) {
            subscribers.remove( key );
        }
    }

    /**
     * Послать событие в шину
     * @param value  событие
     * @param <T>    тип структуры данных
     */
    public <T> void fireEvent( T value ) {
        long startTime = System.currentTimeMillis();
        Class<T> key = (Class<T>) value.getClass();
        List<Subscriber> subscriberList = subscribers.get( key );
        if ( subscriberList != null ) {
            for ( Subscriber subscriber : subscriberList ) {
                subscriber.update( value );
            }
        }
        long endTime = System.currentTimeMillis();
        long handleTime = endTime-startTime;
        if ( handleTime > 50 ) {
            log.info( "Handling of " + value.toString() + " event took " + ( endTime - startTime ) + " ms" );
        }
    }

    /**
     * Очищает всех подписчиков
     */
    public void clearSubscribers() {
        subscribers.clear();
    }

    Map<Class, List< Subscriber >> subscribers = new HashMap<Class, List<Subscriber>>();

    public final static Logger log = Logger.getLogger( Bus.class.getName() );
}
