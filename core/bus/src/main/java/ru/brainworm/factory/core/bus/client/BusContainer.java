package ru.brainworm.factory.core.bus.client;

/**
 * Синглетон-контейнер для шины событий
 */
public class BusContainer {
    public static Bus getBus() {
        if ( bus == null ) {
            bus = new Bus();
        }
        return bus;
    }

    private static Bus bus;
}
