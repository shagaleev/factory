package ru.brainworm.factory.core.datetimepicker.client.view.input.single;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import com.google.inject.Inject;
import ru.brainworm.factory.core.datetimepicker.client.activity.input.single.SinglePickerActivity;
import ru.brainworm.factory.core.datetimepicker.client.util.CalendarConstant;
import ru.brainworm.factory.core.datetimepicker.client.util.DisplayStyle;
import ru.brainworm.factory.core.datetimepicker.client.widgets.pickerinput.PickerInput;
import ru.brainworm.factory.core.datetimepicker.shared.dto.PickerType;
import ru.brainworm.factory.core.datetimepicker.shared.dto.TimeEnabledMode;

import java.util.Date;


/**
 * Виджет выбора даты/времени
 */
public class SinglePicker extends Composite
        implements HasValue<Date>, HasEnabled
{

    public SinglePicker() {
        initWidget(ourUiBinder.createAndBindUi(this));

        setType( PickerType.DATETIME );
        setTimeEnabledMode( TimeEnabledMode.ALL );
    }

    @Override
    public boolean isEnabled() {
        return input.isEnabled() && button.isEnabled();
    }

    @Override
    public void setEnabled( boolean enabled ) {
        input.setEnabled( enabled );
        button.setEnabled( enabled );
    }

    public HasWidgets getContainer() {
        return root;
    }

    public UIObject getRelative() {
        return button;
    }

    public PickerType getType() {
        return type;
    }

    public void setType( PickerType type ) {
        this.type = type;
        buttonIcon.setClassName( type.getIcon() );
    }

    public void setTimeEnabledMode( TimeEnabledMode timeEnabledMode ) {
        this.timeEnabledMode = timeEnabledMode;
    }

    public TimeEnabledMode getTimeEnabledMode() {
        return timeEnabledMode;
    }


    public void setFormat( DateTimeFormat dateTimeFormat ) {
        this.dateTimeFormat = dateTimeFormat;
    }

    public void setFormatValue( String format ) {
        this.dateTimeFormat = DateTimeFormat.getFormat( format );
    }

    public DateTimeFormat getFormat() {
        if ( dateTimeFormat != null ) {
            return dateTimeFormat;
        }

        switch ( type ) {
            case TIME:
                return CalendarConstant.Formats.DEFAULT_TIME_FORMAT;
            case DATE:
                return CalendarConstant.Formats.DEFAULT_DATE_FORMAT;
            case DATETIME:
            default:
                return CalendarConstant.Formats.DEFAULT_DATETIME_FORMAT;
        }
    }

    public TimeZone getTimeZone() {
        // Current TZ
        if ( timeZone == null ) {
            return TimeZone.createTimeZone( new Date().getTimezoneOffset() );
        }
        return timeZone;
    }

    public void setTimeZone( TimeZone timeZone ) {
        this.timeZone = timeZone;
    }

    public void setEnsureDebugId( String debugId ) {
        container.ensureDebugId( debugId );
    }

    @Override
    public HandlerRegistration addValueChangeHandler( ValueChangeHandler< Date > handler ) {
        return addHandler( handler, ValueChangeEvent.getType() );
    }

    @Override
    public Date getValue() {
        try {
            return getFormat().parseStrict( input.getValue().trim() );
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void setValue( Date value ) {
        setValue( value, false );
    }

    @Override
    public void setValue( Date value, boolean fireEvents ) {
        markInputValid( value != null );
        if ( value == null ) {
            input.setText( "" );
            return;
        }

        input.setText( getFormat().format( value, getTimeZone() ) );
        if ( fireEvents ) {
            ValueChangeEvent.fire( this, value );
        }
    }

    public void markInputValid( boolean valid ) {
        clearInputMark();
        if (! mandatory
                && input.getValue() != null && input.getValue().isEmpty()) {
            return;
        }

        if ( !valid ) {
            markInput( DisplayStyle.ERROR );
        }
    }

    public void clearInputMark() {
        for ( DisplayStyle style : DisplayStyle.values() ) {
            root.removeStyleName( style.getStyleName() );
        }
    }

    public void markInput( DisplayStyle style ) {
        clearInputMark();
        if ( style == null ) {
            return;
        }
        root.addStyleName( style.getStyleName() );
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory( boolean mandatory ) {
        this.mandatory = mandatory;
    }

    public String getInputValue() {
        return input.getValue();
    }

    @Override
    protected void onAttach() {
        super.onAttach();
        activity.subscribe( this );
    }

    @Override
    protected void onDetach() {
        super.onDetach();
        activity.unsubscribe(this);
    }

    @UiHandler( "button" )
    void onShowCalendarFormBtnClicked(ClickEvent event) {
        if (activity != null) {
            activity.onCalendarButtonClicked( this );
        }
    }

    @UiHandler("input")
    void onChangeInput(ValueChangeEvent<String> event) {
        markInputValid( getValue() != null );
        fireDateChanged();
    }

    private void fireDateChanged() {
        ValueChangeEvent.fire( this, getValue() );
    }


    @UiField
    PickerInput input;
    @UiField
    Button button;
    @UiField
    Element buttonIcon;
    @UiField
    HTMLPanel container;
    @UiField
    HTMLPanel root;

    @Inject
    SinglePickerActivity activity;


    private PickerType type;
    private DateTimeFormat dateTimeFormat;
    private TimeEnabledMode timeEnabledMode;
    private boolean mandatory = true;
    private TimeZone timeZone;

    interface DatePickerUiBinder extends UiBinder<Widget, SinglePicker > {}
    private static DatePickerUiBinder ourUiBinder = GWT.create(DatePickerUiBinder.class);
}