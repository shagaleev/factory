package ru.brainworm.factory.core.datetimepicker.client.widgets.pickerinput;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;

public class PickerInput extends com.google.gwt.user.client.ui.TextBox {

    public PickerInput() {
        super();
        sinkEvents( Event.ONPASTE );
        sinkEvents( Event.ONKEYUP );
    }

    public PickerInput( Element element ) {
        super( element );
        sinkEvents( Event.ONPASTE );
        sinkEvents( Event.ONKEYUP );
    }

    @Override
    public void onBrowserEvent( Event event ) {
        super.onBrowserEvent( event );
        switch ( DOM.eventGetType( event ) ) {
            case Event.ONKEYUP:
            case Event.ONPASTE:
                Scheduler.get().scheduleDeferred( new Scheduler.ScheduledCommand() {
                    @Override
                    public void execute() {
                        ValueChangeEvent.fire( PickerInput.this, getText() );
                    }
                } );
            default:
        }
    }

}