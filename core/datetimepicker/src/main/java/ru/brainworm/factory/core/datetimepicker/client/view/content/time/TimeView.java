package ru.brainworm.factory.core.datetimepicker.client.view.content.time;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.time.AbstractTimeActivity;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.time.AbstractTimeView;

/**
 * Вью для отображения формы выбора времени
 */
public class TimeView extends Composite implements AbstractTimeView {

    public TimeView() {
        initWidget(ourUiBinder.createAndBindUi(this));
    }

    @Override
    public void setActivity(AbstractTimeActivity activity) {
        this.activity = activity;
    }

    @Override
    public void setVisibleCalendarButton( boolean b ) {
        calendarBtn.setVisible( b );
    }

    @Override
    public void setHour(String value) {
        hour.setText(value);
    }

    @Override
    public void setMinutes(String value) {
        minutes.setText(value);
    }

    @Override
    public void setSeconds(String value) {
        seconds.setText(value);
    }

    @Override
    public void enableHours( boolean value ) {
        if ( value ) {
            hourUp.removeStyleName( "disabled" );
            hour.removeStyleName( "disabled" );
            hourDown.removeStyleName( "disabled" );
        } else {
            hourUp.addStyleName( "disabled" );
            hour.addStyleName( "disabled" );
            hourDown.addStyleName( "disabled" );
        }
    }

    @Override
    public void enableMinutes( boolean value ) {
        if ( value ) {
            minUp.removeStyleName( "disabled" );
            minutes.removeStyleName( "disabled" );
            minDown.removeStyleName( "disabled" );
        } else {
            minUp.addStyleName( "disabled" );
            minutes.addStyleName( "disabled" );
            minDown.addStyleName( "disabled" );
        }
    }

    @Override
    public void enableSeconds( boolean value ) {
        if ( value ) {
            secUp.removeStyleName( "disabled" );
            seconds.removeStyleName( "disabled" );
            secDown.removeStyleName( "disabled" );
        } else {
            secUp.addStyleName( "disabled" );
            seconds.addStyleName( "disabled" );
            secDown.addStyleName( "disabled" );
        }
    }

    @UiHandler("calendarBtn")
    public void onCalendarBtnClicked(ClickEvent event) {
        if (activity != null) {
            activity.onCalendarBtnClicked( this );
        }
    }

    @UiHandler("hourUp")
    public void onHourUp(ClickEvent event) {
        if (activity != null) {
            activity.onHourUpClicked( this );
        }
    }

    @UiHandler("hourDown")
    public void onHourDown(ClickEvent event) {
        if (activity != null) {
            activity.onHourDownClicked( this );
        }
    }

    @UiHandler("minUp")
    public void onMinUp(ClickEvent event) {
        if (activity != null) {
            activity.onMinutesUpClicked( this );
        }
    }

    @UiHandler("minDown")
    public void onMinDown(ClickEvent event) {
        if (activity != null) {
            activity.onMinutesDownClicked( this );
        }
    }

    @UiHandler("secDown")
    public void onSecDown(ClickEvent event) {
        if (activity != null) {
            activity.onSecondDownClicked( this );
        }
    }

    @UiHandler("secUp")
    public void onSecUp(ClickEvent event) {
        if (activity != null) {
            activity.onSecondUpClicked( this );
        }
    }

    @UiField
    Anchor calendarBtn;
    @UiField
    Anchor hourUp;
    @UiField
    Anchor hour;
    @UiField
    Anchor hourDown;
    @UiField
    Anchor minUp;
    @UiField
    Anchor minutes;
    @UiField
    Anchor minDown;
    @UiField
    Anchor secUp;
    @UiField
    Anchor seconds;
    @UiField
    Anchor secDown;

    private AbstractTimeActivity activity;

    interface TimesViewUiBinder extends UiBinder<Widget, TimeView> {
    }

    private static TimesViewUiBinder ourUiBinder = GWT.create(TimesViewUiBinder.class);

}