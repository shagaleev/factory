package ru.brainworm.factory.core.datetimepicker.shared.dto;

/**
 * Тип дня календаря
 */
public enum CalendarType {

    DAYS("datetimepicker-days"),

    MONTH("datetimepicker-months"),

    YEARS("datetimepicker-years");

    CalendarType( String value ) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    private String value;
}
