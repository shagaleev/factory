package ru.brainworm.factory.core.datetimepicker.shared.dto;

/**
 * Тип дня календаря
 */
public enum CalendarCellType {

    CURRENT(""),

    PREV("old"),

    NEXT("new");

    CalendarCellType( String value ) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    private String value;
}
