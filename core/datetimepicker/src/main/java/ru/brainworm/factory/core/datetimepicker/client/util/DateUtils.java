package ru.brainworm.factory.core.datetimepicker.client.util;

import java.util.Date;

/**
 * Утилитарный класс для работы с датами
 */
public class DateUtils {

    /**
     * Устанавливает начало дня
     */
    public static Date setBeginOfDay(Date day) {
        day.setHours(0);
        day.setMinutes(0);
        day.setSeconds(0);
        setMilliseconds(day, 0);
        return day;
    }

    /**
     * Устанавливает конец дня
     */
    public static Date setEndOfDay(Date day) {
        day.setHours(23);
        day.setMinutes(59);
        day.setSeconds(59);
        setMilliseconds(day, 999);
        return day;
    }

    /**
     * Устанавливает время
     */
    public static Date setTime(Date day) {
        Date date = new Date();
        date.setHours(day.getHours());
        date.setMinutes(day.getMinutes());
        date.setSeconds(day.getSeconds());
        setMilliseconds( date, getMilliseconds( day ) );
        return date;
    }

    public static Date getCurrentDate() {
        Date date = new Date();
        setMilliseconds( date, 0 );
        return date;
    }

    private static void setMilliseconds(Date date, long millis) {
        date.setTime(((date.getTime() / 1000) * 1000) + millis);
    }

    private static long getMilliseconds(Date date) {
        return date.getTime() % 1000;
    }
}
