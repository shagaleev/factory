package ru.brainworm.factory.core.datetimepicker.client.activity.popup;

/**
 * Абстрактное активити для всплывающего окна (контейнер для форм календаря).
 *
 */
public interface AbstractPopupActivity {
    void onResizeWindow();
    void onScrollWindow();
}
