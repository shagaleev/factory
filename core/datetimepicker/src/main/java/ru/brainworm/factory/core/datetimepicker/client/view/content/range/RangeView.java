package ru.brainworm.factory.core.datetimepicker.client.view.content.range;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.range.AbstractRangeActivity;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.range.AbstractRangeView;
import ru.brainworm.factory.core.datetimepicker.client.lang.DatetimePickerLang;
import ru.brainworm.factory.core.datetimepicker.shared.dto.PredefinedPeriod;

import java.util.Date;

/**
 * Вью Контейнера для элементов календаря, времени, года
 */
public class RangeView
        extends Composite
        implements AbstractRangeView
{

    public RangeView() {
        setWidget(ourUiBinder.createAndBindUi(this));
    }

    @Override
    public void setActivity(AbstractRangeActivity activity) {
        this.activity = activity;
    }

    @Override
    public HasWidgets getFromContainer() {
        return fromContainer;
    }

    @Override
    public HasWidgets getToContainer() {
        return toContainer;
    }

    @Override
    public void setFromDate( Date from ) {
        if ( to == null ) {
            return;
        }
        this.from.setText( format.format( from ) );
    }

    @Override
    public void setToDate( Date to ) {
        if ( to == null ) {
            return;
        }
        this.to.setText( format.format( to ) );
    }

    @Override
    public void setUnlimitedDateTo( Date to ) {
        if ( to == null ) {
            this.to.setText( lang.dateRangePickerUnlimitedLabel() );
            return;
        }
        this.to.setText( format.format( to ) );
    }

    @Override
    public void setFormat( DateTimeFormat format ) {
        this.format = format;
    }

    @Override
    public HasVisibility visibleUnlimitedDateTo() {
        return unlimitedDateToContainer;
    }

    @Override
    public HasVisibility visiblePredefinedPeriods() {
        return predefinedPeriodContainer;
    }

    @Override
    public HasValue<Boolean> unlimitedDateTo() {
        return unlimitedDateToCheckbox;
    }

    @Override
    public void disableToPicker( Boolean value ) {
        if ( value ) {
            toContainer.addStyleName( "picker-disabled" );
        } else {
            toContainer.removeStyleName( "picker-disabled" );
        }
    }

    @UiHandler( "lastHour" )
    public void onLastHourClicked( ClickEvent event ) {
        if ( activity != null ) {
            activity.onPredefinedPeriodClicked( PredefinedPeriod.LAST_HOUR );
        }
    }

    @UiHandler( "lastDay" )
    public void onLastDayClicked( ClickEvent event ) {
        if ( activity != null ) {
            activity.onPredefinedPeriodClicked( PredefinedPeriod.LAST_DAY );
        }
    }

    @UiHandler( "lastWeek" )
    public void onLastWeekClicked( ClickEvent event ) {
        if ( activity != null ) {
            activity.onPredefinedPeriodClicked( PredefinedPeriod.LAST_WEEK );
        }
    }

    @UiHandler( "apply" )
    public void onApplyClicked( ClickEvent event ) {
        if ( activity != null ) {
            activity.onApplyClicked();
        }
    }

    @UiHandler( "cancel" )
    public void onCancelClicked( ClickEvent event ) {
        if ( activity != null ) {
            activity.onCancelClicked();
        }
    }

    @UiHandler( "unlimitedDateToCheckbox" )
    public void onUnlimitedDateToClicked( ClickEvent event ) {
        if ( activity != null ) {
            activity.onUnlimitedDateToClicked();
        }
    }

    @UiField
    HTMLPanel fromContainer;
    @UiField
    HTMLPanel toContainer;
    @UiField
    HTMLPanel unlimitedDateToContainer;
    @UiField
    CheckBox unlimitedDateToCheckbox;
    @UiField
    HTMLPanel predefinedPeriodContainer;
    @UiField
    Button lastHour;
    @UiField
    Button lastDay;
    @UiField
    Button lastWeek;
    @UiField
    Button apply;
    @UiField
    Label from;
    @UiField
    Label to;
    @UiField
    protected DatetimePickerLang lang;

    private DateTimeFormat format;
    private AbstractRangeActivity activity;

    interface RangeContainerViewUiBinder extends UiBinder<HTMLPanel, RangeView> {}
    private static RangeContainerViewUiBinder ourUiBinder = GWT.create(RangeContainerViewUiBinder.class);

}