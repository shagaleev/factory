package ru.brainworm.factory.core.datetimepicker.client.factory;

import com.google.gwt.inject.client.GinModules;
import ru.brainworm.factory.generator.injector.client.FactoryInjector;

/**
 * Factory
 */
@GinModules( DateTimePickerClientModule.class )
public interface DateTimePickerClientFactory
        extends FactoryInjector
{
}

