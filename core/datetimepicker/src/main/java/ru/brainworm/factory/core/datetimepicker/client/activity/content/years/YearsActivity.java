package ru.brainworm.factory.core.datetimepicker.client.activity.content.years;


import com.google.gwt.user.client.ui.HasWidgets;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.calendar.AbstractCalendarView;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.calendar.CalendarActivity;
import ru.brainworm.factory.core.datetimepicker.client.events.SinglePickerInternalEvents;
import ru.brainworm.factory.core.datetimepicker.client.util.CalendarConstant;
import ru.brainworm.factory.core.datetimepicker.client.util.CalendarUtils;
import ru.brainworm.factory.core.datetimepicker.shared.dto.*;
import ru.brainworm.factory.generator.activity.client.annotations.Event;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Активити выбора года
 */
public abstract class YearsActivity
        extends CalendarActivity
{
    @Event
    public void onShowYears(SinglePickerInternalEvents.ShowYears event) {
        buildCalendarView( event.parent, event.type, event.value, event.timeEnabledMode );
    }

    @Override
    public void onDateClicked( ViewModel model, String text, CalendarCellType type ) {
        model.currentDate.setYear( CalendarUtils.getYearDeNormalized( Integer.parseInt( text ) ) );
        fireEvent(new SinglePickerInternalEvents.ShowMonths( model.parent, model.type, model.currentDate, model.timeEnabledMode ));
    }

    @Override
    public void onNextBtnClicked( Date currentDate ){
        currentDate.setYear( currentDate.getYear() + gridSize );
    }

    @Override
    public void onPrevBtnClicked( Date currentDate ) {
        currentDate.setYear( currentDate.getYear() - gridSize );
    }

    @Override
    public void onSwitchBtnClicked( HasWidgets parent, PickerType type, Date currentDate, TimeEnabledMode timeEnabledMode ) {}

    @Override
    public void buildGridInView( ViewModel model ) {
        AbstractCalendarView view = model.view;
        Date currentDate = model.currentDate;

        view.resizeTable( CalendarConstant.Years.ROW, CalendarConstant.Years.COL );
        view.setType( CalendarType.YEARS );

        Date dueDate = new Date(currentDate.getTime());
        view.setDateHeader( CalendarUtils.getYearNormalized( dueDate.getYear() )
                + " - " +
                CalendarUtils.getYearNormalized( dueDate.getYear() + gridSize - 1 ) );

        int centerOfGrid = gridSize / 2;
        dueDate.setYear( dueDate.getYear() - centerOfGrid );

        for (int row = 0; row < CalendarConstant.Years.ROW; row++) {
            for (int col = 0; col < CalendarConstant.Years.COL; col++) {
                Set<String> markers = new HashSet<String>();
                CalendarCellType type = CalendarCellType.CURRENT;
                if (dueDate.getYear() == today.getYear()) {
                    markers.add(CalendarConstant.CellStyleName.TODAY);
                }
                if (dueDate.getYear() == currentDate.getYear()) {
                    markers.add(CalendarConstant.CellStyleName.ACTIVE);
                }
                view.addCell( row, col, CalendarUtils.getYearNormalized( dueDate.getYear() ), type, markers );
                dueDate.setYear(dueDate.getYear() + 1);
            }
        }
    }

    private final int gridSize = CalendarConstant.Years.ROW * CalendarConstant.Years.COL;
}
