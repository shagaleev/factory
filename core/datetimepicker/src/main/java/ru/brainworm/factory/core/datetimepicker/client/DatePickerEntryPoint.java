package ru.brainworm.factory.core.datetimepicker.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import ru.brainworm.factory.core.datetimepicker.client.factory.DateTimePickerClientFactory;

/**
 * Точка входа в модуль DateTimePicker.
 */
public class DatePickerEntryPoint implements EntryPoint {

    @Override
    public void onModuleLoad() {
        factory = GWT.create( DateTimePickerClientFactory.class );
    }

    DateTimePickerClientFactory factory;
}
