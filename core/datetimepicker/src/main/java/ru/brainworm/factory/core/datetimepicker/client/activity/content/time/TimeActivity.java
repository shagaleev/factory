package ru.brainworm.factory.core.datetimepicker.client.activity.content.time;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.inject.Inject;
import com.google.inject.Provider;
import ru.brainworm.factory.core.datetimepicker.client.events.SinglePickerInternalEvents;
import ru.brainworm.factory.core.datetimepicker.shared.dto.CalendarType;
import ru.brainworm.factory.core.datetimepicker.shared.dto.PickerType;
import ru.brainworm.factory.core.datetimepicker.shared.dto.TimeEnabledMode;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Активити для выбора времени
 */
public abstract class TimeActivity
        implements Activity,
        AbstractTimeActivity
{
    @Event
    public void onShowTimes( SinglePickerInternalEvents.ShowTimes event ) {
        AbstractTimeView view = factory.get();
        view.setActivity( this );
        view.setVisibleCalendarButton( event.type != PickerType.TIME );

        viewToEvent.put( view, event );

        view.setHour( String.valueOf( event.value.getHours() ) );
        view.setMinutes( String.valueOf( event.value.getMinutes() ) );
        view.setSeconds( String.valueOf( event.value.getSeconds() ) );

        switch ( event.timeEnabledMode ) {
            case ALL:
                enableTimeMode( view, true, true, true );
                break;
            case HOUR:
                enableTimeMode( view, true, false, false );
                break;
            case HOURMINUTE:
                enableTimeMode( view, true, true, false );
                break;
        }

        event.parent.clear();
        event.parent.add(view.asWidget());
    }

    @Event
    public void onShowMonths(SinglePickerInternalEvents.ShowMonths event) {
        parentToPrev.put( event.parent, CalendarType.MONTH );
    }

    @Event
    public void onShowYears(SinglePickerInternalEvents.ShowYears event) {
        parentToPrev.put( event.parent, CalendarType.YEARS );
    }

    @Event
    public void onShowDays(SinglePickerInternalEvents.ShowDays event) {
        parentToPrev.put( event.parent, CalendarType.DAYS );
    }

    @Override
    public void onCalendarBtnClicked( AbstractTimeView view ) {
        SinglePickerInternalEvents.ShowTimes show = viewToEvent.get( view );
        if ( show == null || show.parent == null || show.value == null ) {
            return;
        }

        CalendarType previousForm = parentToPrev.get( show.parent );
        if (previousForm == null ) {
            return;
        }

        switch (previousForm) {
            case YEARS:
                fireEvent(new SinglePickerInternalEvents.ShowYears( show.parent, show.type, show.value, show.timeEnabledMode ));
                break;
            case MONTH:
                fireEvent(new SinglePickerInternalEvents.ShowMonths( show.parent, show.type, show.value, show.timeEnabledMode ));
                break;
            case DAYS:
            default:
                fireEvent(new SinglePickerInternalEvents.ShowDays( show.parent, show.type, show.value, show.timeEnabledMode ));
        }
        fireEvent( new SinglePickerInternalEvents.DateSelected( show.parent, show.value ) );
    }

    @Override
    public void onHourUpClicked( AbstractTimeView view ) {
        SinglePickerInternalEvents.ShowTimes show = viewToEvent.get( view );
        if ( show == null || show.parent == null || show.value == null ) {
            return;
        }

        show.value.setHours( show.value.getHours() + 1 );
        refresh( view, show.value );

        fireChangeEvent( show.parent, show.value );
    }

    @Override
    public void onHourDownClicked( AbstractTimeView view ) {
        SinglePickerInternalEvents.ShowTimes show = viewToEvent.get( view );
        if ( show == null || show.parent == null || show.value == null ) {
            return;
        }

        show.value.setHours( show.value.getHours() - 1 );
        refresh( view, show.value );

        fireChangeEvent( show.parent, show.value );
    }

    @Override
    public void onMinutesUpClicked( AbstractTimeView view ) {
        SinglePickerInternalEvents.ShowTimes show = viewToEvent.get( view );
        if ( show == null || show.parent == null || show.value == null ) {
            return;
        }

        if ( show.timeEnabledMode == TimeEnabledMode.HOUR ) {
            return;
        }

        show.value.setMinutes( show.value.getMinutes() + 1 );
        refresh( view, show.value );

        fireChangeEvent( show.parent, show.value );
    }

    @Override
    public void onMinutesDownClicked( AbstractTimeView view ) {
        SinglePickerInternalEvents.ShowTimes show = viewToEvent.get( view );
        if ( show == null || show.parent == null || show.value == null ) {
            return;
        }

        if ( show.timeEnabledMode == TimeEnabledMode.HOUR ) {
            return;
        }

        show.value.setMinutes( show.value.getMinutes() - 1 );
        refresh( view, show.value );

        fireChangeEvent( show.parent, show.value );
    }

    @Override
    public void onSecondUpClicked( AbstractTimeView view ) {
        SinglePickerInternalEvents.ShowTimes show = viewToEvent.get( view );
        if ( show == null || show.parent == null || show.value == null ) {
            return;
        }

        if ( show.timeEnabledMode == TimeEnabledMode.HOUR || show.timeEnabledMode == TimeEnabledMode.HOURMINUTE ) {
            return;
        }

        show.value.setSeconds( show.value.getSeconds() + 1 );
        refresh( view, show.value );

        fireChangeEvent( show.parent, show.value );
    }

    @Override
    public void onSecondDownClicked( AbstractTimeView view ) {
        SinglePickerInternalEvents.ShowTimes show = viewToEvent.get( view );
        if ( show == null || show.parent == null || show.value == null ) {
            return;
        }

        if ( show.timeEnabledMode == TimeEnabledMode.HOUR || show.timeEnabledMode == TimeEnabledMode.HOURMINUTE ) {
            return;
        }

        show.value.setSeconds( show.value.getSeconds() - 1 );
        refresh( view, show.value );

        fireChangeEvent( show.parent, show.value );
    }

    private void enableTimeMode( AbstractTimeView view, boolean hours, boolean minutes, boolean seconds ) {
        view.enableHours( hours );
        view.enableMinutes( minutes );
        view.enableSeconds( seconds );
    }

    private void refresh( AbstractTimeView view, Date value ) {
        view.setHour( Integer.toString( value.getHours() ) );
        view.setMinutes( Integer.toString( value.getMinutes() ) );
        view.setSeconds( Integer.toString( value.getSeconds() ) );
    }

    private void fireChangeEvent( HasWidgets parent, Date value ) {
        fireEvent( new SinglePickerInternalEvents.DateSelected( parent, value ) );
    }

    @Inject
    Provider<AbstractTimeView> factory;

    private Map<AbstractTimeView, SinglePickerInternalEvents.ShowTimes > viewToEvent = new HashMap<AbstractTimeView, SinglePickerInternalEvents.ShowTimes>();
    private Map<HasWidgets, CalendarType > parentToPrev = new HashMap<HasWidgets, CalendarType>();
}
