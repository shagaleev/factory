package ru.brainworm.factory.core.datetimepicker.client.view.popup;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import ru.brainworm.factory.core.datetimepicker.client.activity.popup.AbstractPopupActivity;
import ru.brainworm.factory.core.datetimepicker.client.activity.popup.AbstractPopupView;

/**
 * Вью Контейнера для элементов календаря, времени, года
 */
public class PopupView extends PopupPanel implements AbstractPopupView {

    public PopupView() {
        setWidget(ourUiBinder.createAndBindUi(this));
        setAutoHideEnabled(true);
    }

    @Override
    public void setActivity(AbstractPopupActivity activity) {
        this.activity = activity;
    }

    @Override
    public HasWidgets getContainer() {
        return container;
    }

    @Override
    public void showNear(UIObject relativeTo) {
        updatePosition(relativeTo);
    }

    @Override
    protected void onAttach(){
        super.onAttach();
        registerHandlers();
    }

    @Override
    protected void onDetach() {
        super.onDetach();

        windowResizeHandlerRegistration.removeHandler();
        windowScrollHandlerRegistration.removeHandler();
        keyHandlerRegistration.removeHandler();
    }

    private void registerHandlers() {
        keyHandlerRegistration = RootPanel.get().addDomHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent keyUpEvent) {
                if (KeyCodes.KEY_ESCAPE == keyUpEvent.getNativeKeyCode()) {
                    hide();
                }
            }
        }, KeyUpEvent.getType());

        windowResizeHandlerRegistration = Window.addResizeHandler( new ResizeHandler() {
            @Override
            public void onResize(ResizeEvent event ) {
                if ( activity != null && isAttached()) {
                    activity.onResizeWindow();
                }
            }
        } );

        windowScrollHandlerRegistration = Window.addWindowScrollHandler(new Window.ScrollHandler() {
            @Override
            public void onWindowScroll(Window.ScrollEvent event) {
                if ( activity != null && isAttached()) {
                    activity.onScrollWindow();
                }
            }
        });
    }

    private void updatePosition(UIObject relative) {
        showRelativeTo( relative );
        int left = (relative.getAbsoluteLeft() + relative.getOffsetWidth() / 2) - this.getElement().getAbsoluteLeft();
        arrow.getStyle().setLeft(left, Style.Unit.PX);

        if (this.getElement().getAbsoluteTop() < relative.getAbsoluteTop()) {
            arrow.addClassName("rotate");
        } else {
            arrow.removeClassName("rotate");
        }
    }

    @UiField
    HTMLPanel container;

    @UiField
    DivElement arrow;

    private HandlerRegistration windowResizeHandlerRegistration;
    private HandlerRegistration windowScrollHandlerRegistration;
    private HandlerRegistration keyHandlerRegistration;
    private AbstractPopupActivity activity;

    interface PopupContainerViewUiBinder extends UiBinder<HTMLPanel, PopupView > {}
    private static PopupContainerViewUiBinder ourUiBinder = GWT.create(PopupContainerViewUiBinder.class);

}