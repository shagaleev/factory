package ru.brainworm.factory.core.datetimepicker.client.activity.content.range;

import ru.brainworm.factory.core.datetimepicker.shared.dto.PredefinedPeriod;

/**
 * Абстрактное активити для всплывающего окна (контейнер для форм календаря).
 *
 */
public interface AbstractRangeActivity {

    void onApplyClicked();

    void onCancelClicked();

    void onUnlimitedDateToClicked();

    void onPredefinedPeriodClicked( PredefinedPeriod lastPeriod );
}
