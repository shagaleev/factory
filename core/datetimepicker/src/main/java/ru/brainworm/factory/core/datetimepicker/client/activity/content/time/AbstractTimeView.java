package ru.brainworm.factory.core.datetimepicker.client.activity.content.time;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * Абстрактный вью выбора времени
 */
public interface AbstractTimeView extends IsWidget {

    /**
     * Установить значение часа во вью
     *
     * @param hourText значение часа во вью
     */
    void setHour(String hourText);

    /**
     * Установить значение минут во вью
     *
     * @param minutesText значение минут во вью
     */
    void setMinutes(String minutesText);

    /**
     * Установить активити для вью
     * @param activity активити для вью
     */
    void setActivity(AbstractTimeActivity activity);

    /**
     * Устанавливает видимость кнопки перехода к календарю
     */
    void setVisibleCalendarButton( boolean b );

    /**
     * Устанавливает секунды
     */
    void setSeconds( String value );

    /**
     * Устанавливает активность часов
     * @param value
     */
    void enableHours( boolean value );

    /**
     * Устанавливает активность минут
     * @param value
     */
    void enableMinutes( boolean value );

    /**
     * Устанавливает активность секунд
     * @param value
     */
    void enableSeconds( boolean value );
}
