package ru.brainworm.factory.core.datetimepicker.client.activity.input.single;

import com.google.gwt.user.client.ui.HasWidgets;
import ru.brainworm.factory.core.datetimepicker.client.events.*;
import ru.brainworm.factory.core.datetimepicker.client.util.DateUtils;
import ru.brainworm.factory.core.datetimepicker.client.view.input.single.SinglePicker;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;

import java.util.*;

/**
 * Активити виджета выбора даты/времени
 */
public abstract class SinglePickerActivity
        implements Activity
{
    @Event
    public void onSetDateTime( SinglePickerInternalEvents.SetDateTime event ) {
        SinglePicker view = parentToView.get( event.parent );
        if ( view == null ) {
            return;
        }

        view.setValue( event.value, true );
    }

    public void onCalendarButtonClicked( SinglePicker view ) {
        Date date = view.getValue();
        if ( date == null )  {
            date = DateUtils.getCurrentDate();
        }
        fireEvent( new SinglePickerInternalEvents.ShowPopup( view.getContainer(), view.getRelative(), view.getType(),
                date, view.getTimeEnabledMode() ) );
    }

    public void subscribe( SinglePicker widget ) {
        parentToView.put( widget.getContainer(), widget );
    }

    public void unsubscribe( SinglePicker widget ) {
        parentToView.remove( widget.getContainer() );
    }

    private Map<HasWidgets, SinglePicker > parentToView = new HashMap<HasWidgets, SinglePicker >();
}
