package ru.brainworm.factory.core.datetimepicker.shared.dto;

import com.google.gwt.user.client.ui.HasWidgets;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.calendar.AbstractCalendarView;

import java.util.Date;

/**
 * Модель для календаря
 */
public class ViewModel {

    public ViewModel( AbstractCalendarView view, HasWidgets parent, Date currentDate, DateInterval interval,
                      PickerType type, TimeEnabledMode timeEnabledMode ) {
        this.view = view;
        this.parent = parent;
        this.currentDate = currentDate;
        this.interval = interval;
        this.type = type;
        this.timeEnabledMode = timeEnabledMode;
    }

    /**
     * Представление
     */
    public AbstractCalendarView view;

    /**
     * Родительский контейнер
     */
    public HasWidgets parent;

    /**
     * Выбранная дата
     */
    public Date currentDate;

    /**
     * Интервал ( для разметки выбранного интервала в range-picker )
     */
    public DateInterval interval;

    /**
     * Тип пикера
     */
    public PickerType type;

    /**
     * Режим активности времени
     */
    public TimeEnabledMode timeEnabledMode;
}
