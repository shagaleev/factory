package ru.brainworm.factory.core.datetimepicker.client.activity.content.calendar;

import ru.brainworm.factory.core.datetimepicker.shared.dto.CalendarCellType;

/**
 * Абстрактное активити для всплывающего окна (контейнер для форм календаря).
 *
 */
public interface AbstractCalendarActivity {
    /**
     * Обработчик клика по кнопке выбора предыдущего месяца.
     */
    void onNextBtnClicked( AbstractCalendarView currentView );

    /**
     * Обработчик клика по кнопке (месяц-год) вызова формы выбора месяца.
     */
    void onSwitchBtnClicked( AbstractCalendarView currentView );

    /**
     * Обработчик клика по кнопке выбора следующего месяца.
     */
    void onPrevBtnClicked( AbstractCalendarView currentView );

    /**
     * Обработчик клика по кнопке вызова формы выбора времени.
     */
    void onTimesBtnClicked( AbstractCalendarView currentView );

    /**
     * Выбрана дата
     */
    void onDateClicked(AbstractCalendarView view, String text, CalendarCellType type);
}
