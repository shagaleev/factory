package ru.brainworm.factory.core.datetimepicker.client.activity.content.range;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.*;

import java.util.Date;

/**
 * Абстрактное представление для диапазона дат
 */
public interface AbstractRangeView extends IsWidget {

    void setActivity(AbstractRangeActivity activity);

    /**
     * Возвращает контейнер с
     */
    HasWidgets getFromContainer();

    /**
     * Возвращает контейнер по
     */
    HasWidgets getToContainer();

    /**
     * Устанавливает значение даты "С"
     */
    void setFromDate( Date from );

    /**
     * Устанавливает значение даты "По"
     */
    void setToDate( Date from );

    /**
     * Устанавливает значение даты "По" если выставлен флаг "по неопределенное время"
     */
    void setUnlimitedDateTo( Date date );

    /**
     * Установить формат даты-времени
     */
    void setFormat( DateTimeFormat format );

    /**
     * Установить видимость флага "по неопределенное время"
     */
    HasVisibility visibleUnlimitedDateTo();

    /**
     * Установить видимость предустановленных периодов
     */
    HasVisibility visiblePredefinedPeriods();

    /**
     * Значение флага "по неопределенное время"
     */
    HasValue<Boolean> unlimitedDateTo();

    /**
     * Дизактивация пикера "До"
     */
    void disableToPicker( Boolean value );
}
