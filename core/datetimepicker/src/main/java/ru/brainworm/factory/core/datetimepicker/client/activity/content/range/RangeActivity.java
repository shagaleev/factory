package ru.brainworm.factory.core.datetimepicker.client.activity.content.range;

import com.google.inject.Inject;
import ru.brainworm.factory.core.datetimepicker.client.events.RangePickerInternalEvents;
import ru.brainworm.factory.core.datetimepicker.client.events.SinglePickerInternalEvents;
import ru.brainworm.factory.core.datetimepicker.client.util.CalendarUtils;
import ru.brainworm.factory.core.datetimepicker.client.util.DateUtils;
import ru.brainworm.factory.core.datetimepicker.shared.dto.DateInterval;
import ru.brainworm.factory.core.datetimepicker.shared.dto.PickerType;
import ru.brainworm.factory.core.datetimepicker.shared.dto.PredefinedPeriod;
import ru.brainworm.factory.core.datetimepicker.shared.dto.TimeEnabledMode;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;
import ru.brainworm.factory.generator.injector.client.PostConstruct;

import java.util.Date;

/**
 * Активити для всплывающего окна DateTimePicker.
 */
public abstract class RangeActivity
        implements Activity,
        AbstractRangeActivity {

    @PostConstruct
    void onInit() {
        view.setActivity( this );
    }

    @Event
    public void onShowPopupContainer( RangePickerInternalEvents.ShowRange event ) {

        pickerType = event.type;
        timeEnabledMode = event.timeEnabledMode;

        if ( event.interval == null ) {
            interval = new DateInterval();
        } else {
            interval = event.interval;
        }

        if ( event.format != null ) {
            view.setFormat( event.format );
        }

        showFromToPickers();

        view.visiblePredefinedPeriods().setVisible( event.hasPredefinedPeriods );
        view.visibleUnlimitedDateTo().setVisible( event.hasUnlimitedDateTo );
        isUnlimitedDateToVisible = event.hasUnlimitedDateTo;

        view.setFromDate( interval.from );

        view.unlimitedDateTo().setValue( event.hasUnlimitedDateTo && interval.to == null );
        view.disableToPicker( view.unlimitedDateTo().getValue() );
        if ( event.hasUnlimitedDateTo ) {
            view.setUnlimitedDateTo( interval.to );
            if ( interval.to == null ) {
                savedDateTo = new Date();
            }
        } else {
            view.setToDate( interval.to );
        }

        event.parent.clear();
        event.parent.add( view.asWidget() );
    }

    @Event
    public void onDateChanged( SinglePickerInternalEvents.DateSelected event ) {

        if ( event.parent == view.getFromContainer() ) {
            interval.from = getDate( event.value );
            view.setFromDate( interval.from );
        } else if ( event.parent == view.getToContainer() ) {
            interval.to = getDate( event.value );
            if ( isUnlimitedDateToVisible ) {
                view.setUnlimitedDateTo( interval.to );
            } else {
                view.setToDate( interval.to );
            }
        }

        fireEvent( new SinglePickerInternalEvents.MarkRange( view.getFromContainer(), interval ) );
        fireEvent( new SinglePickerInternalEvents.MarkRange( view.getToContainer(), interval ) );
    }

    @Override
    public void onApplyClicked() {
        fireEvent( new RangePickerInternalEvents.RangeSelected( interval ) );
    }

    @Override
    public void onCancelClicked() {
        fireEvent( new RangePickerInternalEvents.Canceled() );
    }

    @Override
    public void onUnlimitedDateToClicked() {
        Boolean isUnlimitedDateTo = view.unlimitedDateTo().getValue();
        view.disableToPicker( isUnlimitedDateTo );

        if ( isUnlimitedDateTo ) {
            savedDateTo = interval.to;
            interval.to = null;
            view.setUnlimitedDateTo( interval.to );
        } else {
            interval.to = savedDateTo;
            view.setToDate( interval.to );
        }

        fireEvent( new SinglePickerInternalEvents.MarkRange( view.getToContainer(), interval ) );
    }

    @Override
    public void onPredefinedPeriodClicked( PredefinedPeriod lastPeriod ) {
        if ( view.unlimitedDateTo().getValue() ) {
            view.unlimitedDateTo().setValue( false );
            view.disableToPicker( false );
        }

        interval.from = new Date();
        interval.to = new Date();

        switch ( lastPeriod ) {
            case LAST_HOUR:
                CalendarUtils.addHoursToDate( interval.from, -1 );
                break;
            case LAST_DAY:
                CalendarUtils.addDaysToDate( interval.from, -1 );
                break;
            case LAST_WEEK:
                CalendarUtils.addDaysToDate( interval.from, -7 );
                break;
        }

        view.setFromDate( interval.from );
        view.setToDate( interval.to );

        showFromToPickers();
    }

    private void showFromToPickers() {
        if ( pickerType == PickerType.TIME ) {
            fireEvent( new SinglePickerInternalEvents.ShowTimes( view.getFromContainer(), pickerType, interval.from, timeEnabledMode ) );
            fireEvent( new SinglePickerInternalEvents.ShowTimes( view.getToContainer(), pickerType, interval.to, timeEnabledMode ) );
        } else {
            fireEvent( new SinglePickerInternalEvents.ShowDays( view.getFromContainer(), pickerType, interval.from, timeEnabledMode ) );
            fireEvent( new SinglePickerInternalEvents.ShowDays( view.getToContainer(), pickerType, interval.to, timeEnabledMode ) );
        }

        fireEvent( new SinglePickerInternalEvents.MarkRange( view.getFromContainer(), interval ) );
        fireEvent( new SinglePickerInternalEvents.MarkRange( view.getToContainer(), interval ) );
    }

    private Date getDate( Date date ) {
        if ( pickerType != PickerType.TIME ) {
            return date;
        }
        return DateUtils.setTime( date );
    }

    @Inject
    public AbstractRangeView view;

    private Date savedDateTo;
    private DateInterval interval;
    private PickerType pickerType;
    private TimeEnabledMode timeEnabledMode;
    private boolean isUnlimitedDateToVisible;
}
