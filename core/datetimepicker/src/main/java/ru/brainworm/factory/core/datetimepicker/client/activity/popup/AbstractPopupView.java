package ru.brainworm.factory.core.datetimepicker.client.activity.popup;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.UIObject;

/**
 * Абстрактый вью для PopupContainer (контейнер для форм календаря).
 */
public interface AbstractPopupView extends IsWidget {
    /**
     * Установить активити для всплывающего контейнера
     */
    void setActivity(AbstractPopupActivity activity);

    /**
     * Вернуть контейнер для размещения форм.
     */
    HasWidgets getContainer();

    void showRelativeTo(UIObject button);

    void hide();

    void showNear(UIObject button);
}
