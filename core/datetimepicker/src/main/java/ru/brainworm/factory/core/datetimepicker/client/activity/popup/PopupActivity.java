package ru.brainworm.factory.core.datetimepicker.client.activity.popup;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.UIObject;
import com.google.inject.Inject;
import ru.brainworm.factory.core.datetimepicker.client.events.RangePickerInternalEvents;
import ru.brainworm.factory.core.datetimepicker.client.events.SinglePickerInternalEvents;
import ru.brainworm.factory.core.datetimepicker.shared.dto.PickerType;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;
import ru.brainworm.factory.generator.injector.client.PostConstruct;

import java.util.Date;

/**
 * Активити для всплывающего окна DateTimePicker.
 */

public abstract class PopupActivity
        implements Activity,
        AbstractPopupActivity {

    @PostConstruct
    void onInit() {
        popupView.setActivity( this );
    }

    @Event
    public void onDateSelected(SinglePickerInternalEvents.DateSelected event) {
        currentDate = event.value;
        fireEvent( new SinglePickerInternalEvents.SetDateTime( parent, currentDate ) );
    }

    @Event
    public void onDateRangeSelected(RangePickerInternalEvents.RangeSelected event) {
        fireEvent( new RangePickerInternalEvents.SetDateTime( parent, event.interval ) );
        popupView.hide();
    }

    @Event
    public void onDateRangeCanceled(RangePickerInternalEvents.Canceled event) {
        popupView.hide();
    }

    @Event
    public void onShowPopup( SinglePickerInternalEvents.ShowPopup event ) {
        relative = event.button;
        this.parent = event.parent;

        if ( event.type == PickerType.TIME ) {
            fireEvent( new SinglePickerInternalEvents.ShowTimes( popupView.getContainer(), event.type, event.value, event.timeEnabledMode ) );
        } else {
            fireEvent( new SinglePickerInternalEvents.ShowDays( popupView.getContainer(), event.type, event.value, event.timeEnabledMode ) );
        }

        popupView.showNear( relative );
    }

    @Event
    public void onShowRangePopupContainer(RangePickerInternalEvents.ShowPopup event) {
        relative = event.button;
        this.parent = event.inputParent;

        fireEvent( new RangePickerInternalEvents.ShowRange( popupView.getContainer(), event.interval, event.type,
                event.format, event.timeEnabledMode, event.hasUnlimitedDateTo, event.hasPredefinedPeriods ) );

        popupView.showNear( relative );
    }

    @Override
    public void onResizeWindow() {
        popupView.showNear( relative );
    }

    @Override
    public void onScrollWindow() {
        popupView.showNear( relative );
    }

    @Inject
    public AbstractPopupView popupView;

    private Date currentDate;
    private HasWidgets parent;
    private UIObject relative;
}
