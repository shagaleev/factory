package ru.brainworm.factory.core.datetimepicker.client.util;

import com.google.gwt.i18n.client.DateTimeFormat;

/**
 * Класс констант для виджета времени
 */
public interface CalendarConstant {

    interface Days {
        int ROWCOL = 7;
    }

    interface Month {
        int ROW = 4;
        int COL = 3;
    }

    interface Years {
        int ROW = 4;
        int COL = 4;
    }

    interface Icons {
        String DATE = "fa fa-calendar";
        String TIME = "fa fa-clock-o";
        String DATETIME = "fa fa-calendar";
    }
    interface CellStyleName {
        String ACTIVE = "active";
        String MARKED = "marked";
        String NEXT = "new";
        String TODAY = "today";
        String PREV = "old";
    }
   interface Formats {
        DateTimeFormat DEFAULT_DATETIME_FORMAT = DateTimeFormat.getFormat("dd.MM.yyyy HH:mm:ss");
        DateTimeFormat DEFAULT_TIME_FORMAT = DateTimeFormat.getFormat("HH:mm:ss");
        DateTimeFormat DEFAULT_DATE_FORMAT = DateTimeFormat.getFormat("dd.MM.yyyy");
    }
}
