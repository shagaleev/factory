package ru.brainworm.factory.core.datetimepicker.client.view.content.calendar;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.calendar.AbstractCalendarActivity;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.calendar.AbstractCalendarView;
import ru.brainworm.factory.core.datetimepicker.shared.dto.CalendarCellType;
import ru.brainworm.factory.core.datetimepicker.shared.dto.CalendarType;

import java.util.Set;

/**
 * Вью Контейнера для элементов календаря, времени, года
 */
public class CalendarView
        extends Composite
        implements AbstractCalendarView
{
    public CalendarView() {
        initWidget( ourUiBinder.createAndBindUi( this ) );
    }

    @Override
    public void setActivity(AbstractCalendarActivity activity) {
        this.activity = activity;
    }

    @Override
    public void addCell(final int row, int col, final String text, final CalendarCellType type, Set<String> markers ) {
        Label lbl = new Label(text);
        String style = type.getValue();

        for(String marker : markers){
            style += " " + marker;
        }

        lbl.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                activity.onDateClicked( view, text, type );
            }
        });
        table.getCellFormatter().setStyleName(row, col, style);
        table.setWidget(row, col, lbl);
    }

    @Override
    public void addHeaderCell(int col, String text) {
        Label lbl = new Label(text);

        table.getCellFormatter().setStyleName(0, col, "dow");
        table.setWidget(0, col, lbl);
    }

    @Override
    public void setDateHeader(String value) {
        this.value.setText( value == null ? "" : value );
    }

    @Override
    public void resizeTable( int row, int col ) {
        table.resize( row, col );
    }

    @Override
    public void setType( CalendarType type ) {
        root.addStyleName( type.getValue() );
    }

    @Override
    public void setVisibleTimesButton( boolean b ) {
        timeContainer.setVisible( b );
    }

    @UiHandler("nextBtn")
    void onNextButtonClicked(ClickEvent event) {
        event.preventDefault();
        if (activity != null) {
            activity.onNextBtnClicked( this );
        }
    }

    @UiHandler("timesButton")
    void onTimesBtnClicked(ClickEvent event) {
        event.preventDefault();
        if (activity != null) {
            activity.onTimesBtnClicked(this);
        }
    }

    @UiHandler("prevBtn")
    void onPreviousButtonClicked(ClickEvent event) {
        event.preventDefault();
        if (activity != null) {
            activity.onPrevBtnClicked( this );
        }
    }

    @UiHandler("value")
    void onSwitchButtonClicked(ClickEvent event) {
        event.preventDefault();
        if (activity != null) {
            activity.onSwitchBtnClicked( this );
        }
    }

    @UiField
    Grid table;
    @UiField
    Anchor value;
    @UiField
    HTMLPanel root;
    @UiField
    HTMLPanel timeContainer;

    AbstractCalendarView view = this;
    private AbstractCalendarActivity activity;

    interface PopupContainerViewUiBinder extends UiBinder<HTMLPanel, CalendarView> {}
    private static PopupContainerViewUiBinder ourUiBinder = GWT.create(PopupContainerViewUiBinder.class);
}