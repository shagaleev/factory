package ru.brainworm.factory.core.datetimepicker.client.util;

import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.datepicker.client.CalendarUtil;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Утилиты для работы с календарём
 */
public class CalendarUtils extends CalendarUtil {
    /**
     * Приводит значение года к привыному начиная с 1900 года.
     *
     * @param year значение года из Date.getYear();
     * @return приведенное значение года (1900 + 114 = 2014 год)
     */
    public static String getYearNormalized(int year) {
        return Integer.toString(START_YEAR + year);
    }

    /**
     * Получить название дня недели по его номеру в неделе
     *
     * @param dayIndex номер дня в неделе
     * @return название дня недели
     */
    public static String getDayOfWeak(int dayIndex) {
        String[] weekdays = LocaleInfo.getCurrentLocale().getDateTimeFormatInfo().weekdaysFull();
        return weekdays[dayIndex];
    }

    /**
     * Получить сокращенное название дня недели по его номеру в неделе
     *
     * @param dayIndex  номер дня в неделе
     * @param numOfChar количество первых букв названия дня недели которое будет возвращено
     *                  если передан 0 или меньше то вывести дефолтное название
     * @return название дня недели
     */
    public static String getDayOfWeakShort(int dayIndex, int numOfChar) {
        String[] weekdays = LocaleInfo.getCurrentLocale().getDateTimeFormatInfo().weekdaysShortStandalone();

        return (numOfChar < 1) ? weekdays[dayIndex] : weekdays[dayIndex].substring(0, numOfChar);
    }

    /**
     * Название месяца по номеру.
     *
     * @param month int номер месяца в году начиная с 0.
     * @return название месяца.
     */
    public static String getMonthName(int month) {
        String[] months = LocaleInfo.getCurrentLocale().getDateTimeFormatInfo().monthsFullStandalone();
        return months[month];
    }

    /**
     * Название месяца по номеру.
     *
     * @param month int номер месяца в году начиная с 0.
     * @return название месяца.
     */
    public static String getMonthNameShort(int month) {
        String[] months = LocaleInfo.getCurrentLocale().getDateTimeFormatInfo().monthsShortStandalone();
        return months[month];
    }

    /**
     * Номер месяца по названию
     */
    public static int getMonthFromNameShort(String monthName) {
        List< String > months = Arrays.asList( LocaleInfo.getCurrentLocale().getDateTimeFormatInfo().monthsShortStandalone() );
        return months.indexOf( monthName );
    }

    /**
     * Преобразует четырехзначное представление года к представлению в классе Date.
     *
     * @param year четырехзначное представление года (2014)
     * @return значение года для Date.setYear(114); (2014 - 1900 = 114)
     */
    public static int getYearDeNormalized(int year) {
        return year - START_YEAR;
    }

    /**
     * Установить месяц с сохранением числа месяца текущим,
     * не превышая количество дней в месяце.
     *      17 марта -> установить февраля = 17 февраля
     *      31 октября -> установить ноябрь = 30 ноября;
     *
     * @param date дата для которой изменяется месяц
     * @param month устанавливаемый месяц в дате
     * @return  дата с установленным месяцем
     */
    public static Date setMonthPreserveDate( Date date, int month ) {
        int nextMonth = CalendarUtils.getMonthNormalized( month );
        int dayInNextMonth = getDaysInMonth( nextMonth, date.getYear() );
        if ( dayInNextMonth < date.getDate() ) {
            date.setDate( dayInNextMonth );
        }
        date.setMonth( nextMonth );

        return date;
    }

    /**
     * Добавляет к дате количество месяцев
     * Инкрементируя года при необходимости
     * @param date
     * @param addedMonths
     */
    public static void addMonthsToDate( Date date, int addedMonths ) {
        int resultMonth = date.getMonth() + addedMonths;
        int deltaYear = 0;
        if ( resultMonth > 11 ) {
            deltaYear = resultMonth / 12;
        } else if ( resultMonth < 0 ) {
            deltaYear = ( resultMonth + 1 ) / 12 - 1;
        }
        date.setYear( date.getYear() + deltaYear );
        CalendarUtils.setMonthPreserveDate( date, resultMonth );
    }

    public static int getNextMonth( Date date ) {
        return (date.getMonth() + 1) % 12;
    }

    public static int getPrevMonth( Date date ) {
        int prevMonth = date.getMonth() - 1;
        return prevMonth < 0 ? 11 : prevMonth;
    }

    /**
     * Вернуть номер месяца отбросив годы
     */
    public static int getMonthNormalized( int month ) {
        if (month >= 12) {
            return month % 12;
        } else if (month < 0) {
            return 11;
        }

        return month;
    }

    /**
     * Является ли указанный день месяца последним днем месяца.
     *
     * @param date  проверяемый день месяца
     * @param month месяц для которого проверяется день
     * @param year  год влияет на количество дней в феврале
     * @return  true если указанный день является последним днем месяца.
     */
    public static boolean isLastDayOfMonth(int date, int month, int year){
        int daysInMonth = getDaysInMonth(  month, year );
        return daysInMonth==date;
    }

    /**
     * Число дней в месяце
     *
     * @param year год влияет на количество дней в феврале
     */
    public static int getDaysInMonth( int month, int year ) {
        switch (month) {
            case 1:
                if ( isLeapYear( year ) )
                    return 29; // leap year
                else
                    return 28;
            case 3:
            case 5:
            case 8:
            case 10:
                return 30;
            default:
                return 31;
        }
    }

    /**
     * Является ли год високосным
     */
    public static boolean isLeapYear(int year){
        return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0);
    }

    public static void addHoursToDate( Date date, int hours ) {
        date.setTime( date.getTime() + 3600 * 1000 * hours );
    }

    /**
     * Исчесление начинается с этого года
     */
    private static final int START_YEAR = 1900;
}
