package ru.brainworm.factory.core.datetimepicker.client.util;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import ru.brainworm.factory.core.datetimepicker.shared.dto.DateInterval;

import java.util.Date;

/**
 *
 */
public class DateIntervalFormatter {

    public static String toString( DateInterval interval, DateTimeFormat format ) {
        return toString( interval, format, TimeZone.createTimeZone( new Date().getTimezoneOffset() ) );
    }

    public static String toString( DateInterval interval, DateTimeFormat format, TimeZone timeZone ) {
        if ( interval == null || interval.isEmpty() ) {
            return "";
        }

        StringBuilder sb = new StringBuilder();

        if ( interval.from != null ) {
            sb.append( format.format( interval.from, timeZone ) ).append( " - " );
        }

        if ( interval.to != null ) {
            sb.append( format.format( interval.to, timeZone ) );
        }

        return sb.toString();
    }

    public static DateInterval parseString( String value, DateTimeFormat format ) {
        DateInterval interval = new DateInterval( null, null );
        if ( value.isEmpty() ) {
            return interval;
        }

        String pattern = "(.*)\\s*[-—‐−‒⁃–—―]\\s*(.*)";
        RegExp compile = RegExp.compile( pattern );
        MatchResult m = compile.exec( value );
        if (m != null) {
            if ( value.contains( m.getGroup( 0 ) )) {
                try {
                    interval.from = format.parseStrict( m.getGroup( 1 ).trim() );
                    if ( ! is4SignedYear( interval.from ) ) {
                        interval.from = null;
                    }
                } catch (Exception e) {
                    interval.from = null;
                }

                try {
                    interval.to = format.parseStrict( m.getGroup( 2 ).trim() );
                    if ( ! is4SignedYear( interval.to ) ) {
                        interval.to = null;
                    }
                } catch (Exception e) {
                    interval.to = null;
                }
            }
        }

        return interval;
    }

    private static boolean is4SignedYear( Date date ) {
        int year = 1900 + date.getYear() ;
        if ( year < 1000 || year > 9999 ) {
            return false;
        }

        return true;
    }
}
