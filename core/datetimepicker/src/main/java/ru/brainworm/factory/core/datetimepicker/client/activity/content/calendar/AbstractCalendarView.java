package ru.brainworm.factory.core.datetimepicker.client.activity.content.calendar;

import com.google.gwt.user.client.ui.IsWidget;
import ru.brainworm.factory.core.datetimepicker.shared.dto.CalendarCellType;
import ru.brainworm.factory.core.datetimepicker.shared.dto.CalendarType;

import java.util.Set;

/**
 * Абстрактный вид формы выбора даты
 */
public interface AbstractCalendarView extends IsWidget {

    void setActivity(AbstractCalendarActivity activity);

    void addCell( int row, int col, String text, CalendarCellType type, Set<String> markers );

    void addHeaderCell(int col, String text);

    void setDateHeader(String s);

    void resizeTable( int row, int col );

    void setType( CalendarType type );

    void setVisibleTimesButton( boolean b );
}
