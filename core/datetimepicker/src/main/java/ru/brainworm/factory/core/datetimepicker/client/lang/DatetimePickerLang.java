package ru.brainworm.factory.core.datetimepicker.client.lang;

import com.google.gwt.i18n.client.Messages;

/**
 * Локализация модуля
 */
public interface DatetimePickerLang extends Messages {

    String dateRangePickerFrom();

    String dateRangePickerTo();

    String dateRangePickerApply();

    String dateRangePickerUnlimitedLabel();

    String dateRangePickerUnlimitedCheckbox();

    String lastHourButton();

    String lastDayButton();

    String lastWeekButton();

    String dateRangePickerCancel();

    String dateRangePickerRangeNotDefined();
}
