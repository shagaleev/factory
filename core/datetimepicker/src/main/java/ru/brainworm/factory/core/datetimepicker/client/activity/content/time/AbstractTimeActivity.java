package ru.brainworm.factory.core.datetimepicker.client.activity.content.time;

import ru.brainworm.factory.core.datetimepicker.client.view.content.time.TimeView;

/**
 * Абстрактный активити для выбора времени
 */
public interface AbstractTimeActivity {
    /**
     * При клике на кнопку календаря
     * производится замена формы выбора времени на форму календаря.
     */
    void onCalendarBtnClicked( AbstractTimeView view );

    /**
     * Увеличить значение часа.
     */
    void onHourUpClicked( AbstractTimeView view );

    /**
     * Уменьшить значение часа.
     */
    void onHourDownClicked( AbstractTimeView view );

    /**
     * Увеличить значение минут.
     */
    void onMinutesUpClicked( AbstractTimeView view );

    /**
     * Уменьшить значение минут.
     */
    void onMinutesDownClicked( AbstractTimeView view );

    /**
     *  Уменьшить значение секунд
     */
    void onSecondDownClicked( AbstractTimeView timeView );

    /**
     *  Увеличить значение секунд
     */
    void onSecondUpClicked( AbstractTimeView timeView );
}
