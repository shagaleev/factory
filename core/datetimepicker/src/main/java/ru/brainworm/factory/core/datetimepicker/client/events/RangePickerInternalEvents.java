package ru.brainworm.factory.core.datetimepicker.client.events;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.UIObject;
import ru.brainworm.factory.core.datetimepicker.shared.dto.DateInterval;
import ru.brainworm.factory.core.datetimepicker.shared.dto.PickerType;
import ru.brainworm.factory.core.datetimepicker.shared.dto.TimeEnabledMode;

/**
 * Набор событий виджета выбора диапазона даты/времени
 */
public class RangePickerInternalEvents {

    /**
     * Событие показать форму контейнера в котором будут размещены
     * элементы календаря
     */
    public static class ShowPopup {
        public ShowPopup( DateInterval interval, UIObject button, HasWidgets inputParent,
                          PickerType type, DateTimeFormat format, TimeEnabledMode timeEnabledMode,
                          boolean hasUnlimitedDateTo, boolean hasPredefinedPeriods ) {
            this.interval = interval;
            this.button = button;
            this.inputParent = inputParent;
            this.type = type;
            this.format = format;
            this.timeEnabledMode = timeEnabledMode;
            this.hasUnlimitedDateTo = hasUnlimitedDateTo;
            this.hasPredefinedPeriods = hasPredefinedPeriods;
        }

        public DateInterval interval;
        public UIObject button;
        public HasWidgets inputParent;
        public PickerType type;
        public DateTimeFormat format;
        public TimeEnabledMode timeEnabledMode;
        public boolean hasUnlimitedDateTo;
        public boolean hasPredefinedPeriods;
    }

    public static class SetDateTime {
        public SetDateTime(HasWidgets parent, DateInterval interval) {
            this.parent = parent;
            this.interval = interval;
        }

        public HasWidgets parent;
        public DateInterval interval;
    }

    public static class ShowRange {
        public ShowRange( HasWidgets parent, DateInterval interval, PickerType type,
                          DateTimeFormat format, TimeEnabledMode timeEnabledMode,
                          boolean hasUnlimitedDateTo, boolean hasPredefinedPeriods ) {
            this.parent = parent;
            this.interval = interval;
            this.type = type;
            this.format = format;
            this.timeEnabledMode = timeEnabledMode;
            this.hasUnlimitedDateTo = hasUnlimitedDateTo;
            this.hasPredefinedPeriods = hasPredefinedPeriods;
        }

        public HasWidgets parent;
        public DateInterval interval;
        public PickerType type;
        public DateTimeFormat format;
        public TimeEnabledMode timeEnabledMode;
        public boolean hasUnlimitedDateTo;
        public boolean hasPredefinedPeriods;
    }

    /**
     * Событие при выборе  года, месяца, дня месяца и времени в dateTimePicker
     */
    public static class RangeSelected {
        public RangeSelected( DateInterval interval ) {
            this.interval = interval;
        }

        public DateInterval interval;
    }

    public static class Canceled {}
}