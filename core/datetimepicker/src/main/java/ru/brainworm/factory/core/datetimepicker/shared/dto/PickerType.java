package ru.brainworm.factory.core.datetimepicker.shared.dto;

import ru.brainworm.factory.core.datetimepicker.client.util.CalendarConstant;

/**
 * Тип виджета выбора даты-времени
 */
public enum PickerType {
    /**
     * Дата
     */
    DATE( CalendarConstant.Icons.DATE ),
    /**
     * Время
     */
    TIME( CalendarConstant.Icons.TIME ),
    /**
     * Дата и время
     */
    DATETIME( CalendarConstant.Icons.DATETIME );

    PickerType( String icon ) {
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    private String icon;
}
