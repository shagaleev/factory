package ru.brainworm.factory.core.datetimepicker.client.activity.content.calendar;


import com.google.gwt.user.client.ui.HasWidgets;
import com.google.inject.Inject;
import com.google.inject.Provider;
import ru.brainworm.factory.core.datetimepicker.client.events.SinglePickerInternalEvents;
import ru.brainworm.factory.core.datetimepicker.client.util.DateUtils;
import ru.brainworm.factory.core.datetimepicker.shared.dto.CalendarCellType;
import ru.brainworm.factory.core.datetimepicker.shared.dto.PickerType;
import ru.brainworm.factory.core.datetimepicker.shared.dto.TimeEnabledMode;
import ru.brainworm.factory.core.datetimepicker.shared.dto.ViewModel;
import ru.brainworm.factory.generator.activity.client.activity.Activity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Форма отображения дней месяца
 */
public abstract class CalendarActivity
        implements Activity,
        AbstractCalendarActivity {

    @Override
    public void onDateClicked(AbstractCalendarView currentView, String text, CalendarCellType type) {
        ViewModel model = viewToModel.get( currentView );
        if ( model == null ) {
            return;
        }

        onDateClicked( model, text, type );
        refresh( model );
    }


    @Override
    public void onNextBtnClicked(AbstractCalendarView currentView) {
        ViewModel model = viewToModel.get( currentView );
        if ( model == null ) {
            return;
        }

        onNextBtnClicked( model.currentDate );
        refresh( model );
    }


    @Override
    public void onSwitchBtnClicked(AbstractCalendarView currentView) {
        ViewModel model = viewToModel.get( currentView );
        if ( model == null ) {
            return;
        }

        onSwitchBtnClicked(  model.parent, model.type, model.currentDate, model.timeEnabledMode );
    }

    @Override
    public void onPrevBtnClicked(AbstractCalendarView currentView) {
        ViewModel model = viewToModel.get( currentView );
        if ( model == null ) {
            return;
        }

        onPrevBtnClicked( model.currentDate );
        refresh( model );
    }

    @Override
    public void onTimesBtnClicked(AbstractCalendarView currentView) {
        ViewModel model = viewToModel.get( currentView );
        if ( model == null ) {
            return;
        }

        fireEvent(new SinglePickerInternalEvents.ShowTimes( model.parent, model.type, model.currentDate, model.timeEnabledMode ));
    }

    public void refresh( ViewModel model ) {
        fireEvent(new SinglePickerInternalEvents.DateSelected( model.parent, model.currentDate ));
        buildGridInView( model );
    }

    public void buildCalendarView( HasWidgets parent, PickerType type, Date date, TimeEnabledMode timeEnabledMode ) {
        AbstractCalendarView view = factory.get();
        view.setActivity(this);
        view.setVisibleTimesButton( type != PickerType.DATE );

        Date currentDate = date == null ? DateUtils.getCurrentDate() : date;
        ViewModel model = new ViewModel( view, parent, currentDate, null, type, timeEnabledMode );

        viewToModel.put( view, model );
        parentToModel.put( parent, model );

        buildGridInView( model );

        parent.clear();
        parent.add( view.asWidget() );
    }

    public ViewModel getModelFromParent( HasWidgets parent ) {
        return parentToModel.get( parent );
    }

    public abstract void buildGridInView( ViewModel model );

    public abstract void onDateClicked( ViewModel model, String text, CalendarCellType type );

    public abstract void onNextBtnClicked( Date currentDate );

    public abstract void onPrevBtnClicked( Date currentDate );

    public abstract void onSwitchBtnClicked( HasWidgets parent, PickerType type, Date currentDate, TimeEnabledMode timeEnabledMode );

    public Date today = new Date();

    private Map<AbstractCalendarView, ViewModel> viewToModel = new HashMap<AbstractCalendarView, ViewModel>();
    private Map<HasWidgets, ViewModel> parentToModel = new HashMap<HasWidgets, ViewModel>();

    @Inject
    private Provider<AbstractCalendarView> factory;
}
