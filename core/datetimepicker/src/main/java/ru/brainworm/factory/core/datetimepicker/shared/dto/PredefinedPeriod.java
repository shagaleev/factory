package ru.brainworm.factory.core.datetimepicker.shared.dto;

/**
 * Типы предустановленных фильтров
 */
public enum PredefinedPeriod {
    LAST_HOUR,
    LAST_DAY,
    LAST_WEEK
}
