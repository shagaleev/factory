package ru.brainworm.factory.core.datetimepicker.client.util;

/**
 * Стиль отображения текстового сообщения в datepicker
 */
public enum DisplayStyle {
    ERROR("has-error"),
    WARNING("has-warning"),
    SUCCESS("has-success");

    DisplayStyle( String styleName ) {
        this.styleName = styleName;
    }

    public String getStyleName() {
        return styleName;
    }

    private String styleName;
}
