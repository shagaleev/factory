package ru.brainworm.factory.core.datetimepicker.client.factory;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.calendar.AbstractCalendarView;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.days.DaysActivity;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.months.MonthsActivity;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.range.AbstractRangeView;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.range.RangeActivity;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.time.AbstractTimeView;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.time.TimeActivity;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.years.YearsActivity;
import ru.brainworm.factory.core.datetimepicker.client.activity.input.range.RangePickerActivity;
import ru.brainworm.factory.core.datetimepicker.client.activity.input.single.SinglePickerActivity;
import ru.brainworm.factory.core.datetimepicker.client.activity.popup.AbstractPopupView;
import ru.brainworm.factory.core.datetimepicker.client.activity.popup.PopupActivity;
import ru.brainworm.factory.core.datetimepicker.client.view.content.calendar.CalendarView;
import ru.brainworm.factory.core.datetimepicker.client.view.content.range.RangeView;
import ru.brainworm.factory.core.datetimepicker.client.view.content.time.TimeView;
import ru.brainworm.factory.core.datetimepicker.client.view.popup.PopupView;

/**
 * Описание классов фабрики
 */
public class DateTimePickerClientModule extends AbstractGinModule {

    @Override
    protected void configure() {
        bind(SinglePickerActivity.class).asEagerSingleton();
        bind(RangePickerActivity.class).asEagerSingleton();

        bind(AbstractPopupView.class).to(PopupView.class).in(Singleton.class);
        bind(PopupActivity.class).asEagerSingleton();

        bind(AbstractCalendarView.class).to(CalendarView.class);
        bind(YearsActivity.class).asEagerSingleton();
        bind(MonthsActivity.class).asEagerSingleton();
        bind(DaysActivity.class).asEagerSingleton();

        bind(AbstractTimeView.class).to(TimeView.class);
        bind(TimeActivity.class).asEagerSingleton();

        bind(RangeActivity.class).asEagerSingleton();
        bind(AbstractRangeView.class).to(RangeView.class).in(Singleton.class);
    }
}

