package ru.brainworm.factory.core.datetimepicker.client.activity.input.range;

import com.google.gwt.user.client.ui.HasWidgets;
import ru.brainworm.factory.core.datetimepicker.client.events.*;
import ru.brainworm.factory.core.datetimepicker.client.util.DateUtils;
import ru.brainworm.factory.core.datetimepicker.client.view.input.range.RangePicker;
import ru.brainworm.factory.core.datetimepicker.shared.dto.DateInterval;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Активити виджета выбора даты/времени
 */
public abstract class RangePickerActivity
        implements Activity
{
    @Event
    public void onSetDateTime( RangePickerInternalEvents.SetDateTime event ) {
        RangePicker view = parentToView.get( event.parent );
        if ( view == null ) {
            return;
        }
        view.setValue( event.interval, true );
        if ( view.getHasUnlimitedDateTo() ) {
            view.markInputValid( event.interval.isFromValid() );
        } else {
            view.markInputValid( event.interval.isValid() );
        }
    }

    public void onCalendarButtonClicked( RangePicker view ) {
        DateInterval interval = view.getValue();
        if ( interval.from == null ) {
            interval.from = DateUtils.setBeginOfDay( new Date() );
        }
        if ( interval.to == null && !view.getHasUnlimitedDateTo() ) {
            interval.to = DateUtils.setEndOfDay( new Date() );
        }

        fireEvent( new RangePickerInternalEvents.ShowPopup( interval, view.getRelative(), view.getContainer(),
                view.getType(), view.getFormat(), view.getTimeEnabledMode(), view.getHasUnlimitedDateTo(), view.getHasPredefinedPeriods() ) );
    }

    public void subscribe( RangePicker widget ) {
        parentToView.put( widget.getContainer(), widget );
    }

    public void unsubscribe( RangePicker widget ) {
        parentToView.remove( widget.getContainer() );
    }

    private Map<HasWidgets, RangePicker > parentToView = new HashMap<HasWidgets, RangePicker>();
}
