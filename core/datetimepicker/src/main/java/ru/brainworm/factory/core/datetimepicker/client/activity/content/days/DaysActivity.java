package ru.brainworm.factory.core.datetimepicker.client.activity.content.days;


import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.calendar.AbstractCalendarView;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.calendar.CalendarActivity;
import ru.brainworm.factory.core.datetimepicker.client.events.SinglePickerInternalEvents;
import ru.brainworm.factory.core.datetimepicker.client.util.CalendarConstant;
import ru.brainworm.factory.core.datetimepicker.client.util.CalendarUtils;
import ru.brainworm.factory.core.datetimepicker.shared.dto.*;
import ru.brainworm.factory.generator.activity.client.annotations.Event;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Форма отображения дней месяца
 */
public abstract class DaysActivity
        extends CalendarActivity {

    @Event
    public void onShowDays( SinglePickerInternalEvents.ShowDays event ) {
        buildCalendarView( event.parent, event.type, event.value, event.timeEnabledMode );
    }

    @Event
    public void onMarkRange( SinglePickerInternalEvents.MarkRange event ) {
        ViewModel model = getModelFromParent( event.parent );
        if ( model == null ) {
            return;
        }

        model.interval = event.interval;
        buildGridInView( model );
    }

    @Override
    public void onDateClicked( ViewModel model, String text, CalendarCellType type ) {
        int currentDay = Integer.parseInt( text );
        if ( type == CalendarCellType.NEXT ) {
            CalendarUtils.addMonthsToDate( model.currentDate, 1 );
        } else if ( type == CalendarCellType.PREV ) {
            CalendarUtils.addMonthsToDate( model.currentDate, - 1 );
        }

        model.currentDate.setDate( currentDay );
    }

    @Override
    public void onNextBtnClicked( Date currentDate ) {
        CalendarUtils.addMonthsToDate( currentDate, 1 );
    }

    @Override
    public void onPrevBtnClicked( Date currentDate ) {
        CalendarUtils.addMonthsToDate( currentDate, - 1 );
    }

    @Override
    public void onSwitchBtnClicked( HasWidgets parent, PickerType type, Date currentDate, TimeEnabledMode timeEnabledMode ) {
        fireEvent( new SinglePickerInternalEvents.ShowMonths( parent, type, currentDate, timeEnabledMode ) );
    }

    @Override
    public void buildGridInView( ViewModel model ) {
        AbstractCalendarView view = model.view;
        Date currentDate = model.currentDate;

        view.resizeTable( CalendarConstant.Days.ROWCOL, CalendarConstant.Days.ROWCOL );
        view.setType( CalendarType.DAYS );

        Date dueDate = new Date(currentDate.getTime());
        CalendarUtil.setToFirstDayOfMonth(dueDate);
        view.setDateHeader( CalendarUtils.getMonthName( dueDate.getMonth() )
                + " "
                + CalendarUtils.getYearNormalized( dueDate.getYear() ) );

        int startingDayOfWeek = CalendarUtil.getStartingDayOfWeek();

        if (dueDate.getDay() >= startingDayOfWeek) {
            dueDate.setDate(dueDate.getDate() - dueDate.getDay() + startingDayOfWeek);
        } else {
            dueDate.setDate(dueDate.getDate() - dueDate.getDay() - (7 - startingDayOfWeek));
        }

        for (int row = 1; row < CalendarConstant.Days.ROWCOL; row++) {
            for (int col = 0; col < CalendarConstant.Days.ROWCOL; col++) {
                if (row == 1) {
                    view.addHeaderCell( col, CalendarUtils.getDayOfWeakShort( dueDate.getDay(), 2 ) );
                }

                Set<String> markers = new HashSet<String>();
                CalendarCellType type = CalendarCellType.CURRENT;
                if ( dueDate.getMonth() == CalendarUtils.getPrevMonth(currentDate) ) {
                    type = CalendarCellType.PREV;
                    markers.add(CalendarConstant.CellStyleName.PREV);
                } else if ( dueDate.getMonth() == CalendarUtils.getNextMonth(currentDate) ) {
                    type = CalendarCellType.NEXT;
                    markers.add(CalendarConstant.CellStyleName.NEXT);
                }

                if(isMarked(model, dueDate)){
                    markers.add(CalendarConstant.CellStyleName.MARKED);
                }
                if (CalendarUtil.isSameDate(dueDate, currentDate)) {
                    markers.add(CalendarConstant.CellStyleName.ACTIVE);
                }
                if(CalendarUtil.isSameDate(dueDate, today)) {
                    markers.add(CalendarConstant.CellStyleName.TODAY);
                }
                view.addCell( row, col, Integer.toString( dueDate.getDate() ), type, markers);
                dueDate.setDate(dueDate.getDate() + 1);
            }
        }
    }

    private boolean isMarked(ViewModel model, Date dueDate) {
        return model.interval != null && model.interval.contains(dueDate);
    }
}
