package ru.brainworm.factory.core.datetimepicker.client.view.input.range;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import com.google.inject.Inject;
import ru.brainworm.factory.core.datetimepicker.client.activity.input.range.RangePickerActivity;
import ru.brainworm.factory.core.datetimepicker.client.lang.DatetimePickerLang;
import ru.brainworm.factory.core.datetimepicker.client.util.CalendarConstant;
import ru.brainworm.factory.core.datetimepicker.client.util.DateIntervalFormatter;
import ru.brainworm.factory.core.datetimepicker.client.util.DisplayStyle;
import ru.brainworm.factory.core.datetimepicker.client.widgets.pickerinput.PickerInput;
import ru.brainworm.factory.core.datetimepicker.shared.dto.DateInterval;
import ru.brainworm.factory.core.datetimepicker.shared.dto.PickerType;
import ru.brainworm.factory.core.datetimepicker.shared.dto.TimeEnabledMode;

import java.util.Date;

/**
 * Виджет выбора даты/времени
 */
public class RangePicker
        extends Composite
        implements HasValue<DateInterval>, HasEnabled
{
    @Inject
    public void onInit( DatetimePickerLang lang ) {
        this.lang = lang;

        initWidget(ourUiBinder.createAndBindUi(this));
        setPlaceholder( input, lang.dateRangePickerRangeNotDefined() );

        setType( PickerType.DATETIME );
        setTimeEnabledMode( TimeEnabledMode.ALL );
        setHasUnlimitedDateTo( false );
        setHasPredefinedPeriods( false );
    }

    @Override
    public boolean isEnabled() {
        return input.isEnabled() && button.isEnabled();
    }

    @Override
    public void setEnabled( boolean enabled ) {
        input.setEnabled( enabled );
        button.setEnabled( enabled );
    }

    public void setPlaceholder( String value ) {
        setPlaceholder( input, value );
    }

    public PickerType getType() {
        return type;
    }

    public void setType( PickerType type ) {
        this.type = type;
        buttonIcon.setClassName( type.getIcon() );
    }

    /**
     * Установить режим редактирования секунд, минут и часов
     * @param timeEnabledMode
     */
    public void setTimeEnabledMode( TimeEnabledMode timeEnabledMode ) {
        this.timeEnabledMode = timeEnabledMode;
    }

    public TimeEnabledMode getTimeEnabledMode() {
        return timeEnabledMode;
    }

    public void setHasUnlimitedDateTo( boolean hasUnlimitedDateTo ) {
        this.hasUnlimitedDateTo = hasUnlimitedDateTo;
    }

    public boolean getHasUnlimitedDateTo() {
        return hasUnlimitedDateTo;
    }

    public void setHasPredefinedPeriods( boolean hasPredefinedPeriods ) {
        this.hasPredefinedPeriods = hasPredefinedPeriods;
    }

    public boolean getHasPredefinedPeriods() {
        return hasPredefinedPeriods;
    }

    public UIObject getRelative() {
        return button;
    }

    public HasWidgets getContainer() {
        return root;
    }

    public void setFormat( DateTimeFormat dateTimeFormat ) {
        this.dateTimeFormat = dateTimeFormat;
    }

    public void setFormatValue( String format ) {
        this.dateTimeFormat = DateTimeFormat.getFormat( format );
    }

    public DateTimeFormat getFormat() {
        if ( dateTimeFormat != null ) {
            return dateTimeFormat;
        }

        switch ( type ) {
            case TIME:
                return CalendarConstant.Formats.DEFAULT_TIME_FORMAT;
            case DATE:
                return CalendarConstant.Formats.DEFAULT_DATE_FORMAT;
            case DATETIME:
            default:
                return CalendarConstant.Formats.DEFAULT_DATETIME_FORMAT;
        }
    }

    public TimeZone getTimeZone() {
        if ( timeZone != null ) {
            return timeZone;
        }
        // Current TZ
        return TimeZone.createTimeZone( new Date().getTimezoneOffset() );
    }

    public void setTimeZone( TimeZone timeZone ) {
        this.timeZone = timeZone;
    }

    @Override
    public DateInterval getValue() {
       return DateIntervalFormatter.parseString( input.getText(), getFormat() );
    }

    @Override
    public void setValue( DateInterval interval ) {
        setValue( interval, false );
    }

    @Override
    public void setValue( DateInterval value, boolean fireEvents ) {
        if ( value == null || value.isEmpty() ) {
            input.setText( "" );
            return;
        }
        input.setText( DateIntervalFormatter.toString( value, getFormat(), getTimeZone() ) );
        markInputValid( value.isValid() );
        if ( getHasUnlimitedDateTo() ) {
            markInputValid( value.isFromValid() );
        } else {
            markInputValid( value.isValid() );
        }

        if ( fireEvents ) {
            ValueChangeEvent.fire( this, value );
        }
    }

    @Override
    public HandlerRegistration addValueChangeHandler( ValueChangeHandler< DateInterval > handler ) {
        return addHandler( handler, ValueChangeEvent.getType() );
    }

    public void markInput( DisplayStyle style, String message ) {
        clearInputMark();
        this.message.setVisible( message != null );
        this.message.setText( message == null ? "" : message );
        if ( style == null ) {
            return;
        }
        root.addStyleName( style.getStyleName() );
    }

    public void clearInputMark() {
        for ( DisplayStyle style : DisplayStyle.values() ) {
            root.removeStyleName( style.getStyleName() );
        }
        message.setVisible( false );
        message.setText( "" );
    }

    public void markInputValid( boolean valid ) {
        clearInputMark();
        if (! mandatory
                && input.getValue() != null && input.getValue().isEmpty()) {
            return;
        }

        if ( !valid ) {
            markInput( DisplayStyle.ERROR, null );
        }
    }

    public void setEnsureDebugId( String debugId ) {
        input.ensureDebugId( debugId );
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory( boolean mandatory ) {
        this.mandatory = mandatory;
    }

    public String getInputValue() {
        return input.getValue();
    }

    @Override
    protected void onAttach() {
        super.onAttach();
        activity.subscribe( this );
    }

    @Override
    protected void onDetach() {
        super.onDetach();
        activity.unsubscribe( this );
    }

    @UiHandler( "button" )
    void onShowCalendarButtonClicked(ClickEvent event) {
         activity.onCalendarButtonClicked( this );
    }

    @UiHandler( "input" )
    void onChangeInput( ValueChangeEvent<String> event ) {
        markInputValid( getValue().isValid() );
        fireDateChanged();
    }

    private void fireDateChanged() {
        ValueChangeEvent.fire( this, getValue() );
    }

    private void setPlaceholder( TextBox input, String value ) {
        input.getElement().setAttribute( "placeholder", value );
    }

    @UiField
    PickerInput input;
    @UiField
    Button button;
    @UiField
    DivElement container;
    @UiField
    Element buttonIcon;
    @UiField
    HTMLPanel root;
    @UiField
    Label message;

    @Inject
    RangePickerActivity activity;

    DatetimePickerLang lang;

    private PickerType type;
    private DateTimeFormat dateTimeFormat;
    private TimeEnabledMode timeEnabledMode;
    private boolean hasUnlimitedDateTo;
    private boolean hasPredefinedPeriods;
    private TimeZone timeZone;
    private boolean mandatory = true;

    interface DatePickerUiBinder extends UiBinder<Widget, RangePicker > {}
    private static DatePickerUiBinder ourUiBinder = GWT.create(DatePickerUiBinder.class);
}