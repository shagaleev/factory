package ru.brainworm.factory.core.datetimepicker.shared.dto;

/**
 * Тип отображения элементов виджета даты-времени
 */
public enum TimeEnabledMode {
    ALL,
    HOUR,
    HOURMINUTE
}
