package ru.brainworm.factory.core.datetimepicker.client.activity.content.months;


import com.google.gwt.user.client.ui.HasWidgets;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.calendar.AbstractCalendarView;
import ru.brainworm.factory.core.datetimepicker.client.activity.content.calendar.CalendarActivity;
import ru.brainworm.factory.core.datetimepicker.client.events.SinglePickerInternalEvents;
import ru.brainworm.factory.core.datetimepicker.client.util.CalendarConstant;
import ru.brainworm.factory.core.datetimepicker.client.util.CalendarUtils;
import ru.brainworm.factory.core.datetimepicker.shared.dto.*;
import ru.brainworm.factory.generator.activity.client.annotations.Event;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Активити выбора года
 */
public abstract class MonthsActivity
        extends CalendarActivity {

    @Event
    public void onShowMonths(SinglePickerInternalEvents.ShowMonths event) {
        buildCalendarView( event.parent, event.type, event.value, event.timeEnabledMode );
    }

    @Override
    public void onDateClicked( ViewModel model, String text, CalendarCellType type ) {
        CalendarUtils.setMonthPreserveDate( model.currentDate, CalendarUtils.getMonthFromNameShort( text ) );
        fireEvent( new SinglePickerInternalEvents.ShowDays( model.parent, model.type, model.currentDate, model.timeEnabledMode ) );
    }

    @Override
    public void onNextBtnClicked( Date currentDate ) {
        currentDate.setYear( currentDate.getYear() + 1 );
    }

    @Override
    public void onPrevBtnClicked( Date currentDate ) {
        currentDate.setYear( currentDate.getYear() - 1 );
    }

    @Override
    public void onSwitchBtnClicked( HasWidgets parent, PickerType type, Date currentDate, TimeEnabledMode timeEnabledMode ){
        fireEvent( new SinglePickerInternalEvents.ShowYears( parent, type, currentDate, timeEnabledMode ) );
    }

    @Override
    public void buildGridInView( ViewModel model ) {
        AbstractCalendarView view = model.view;
        Date currentDate = model.currentDate;

        view.resizeTable( CalendarConstant.Month.ROW, CalendarConstant.Month.COL );
        view.setType( CalendarType.MONTH );

        Date dueDate = new Date(currentDate.getTime());
        dueDate.setDate( 1 );
        view.setDateHeader( CalendarUtils.getYearNormalized( dueDate.getYear() ) );

        dueDate.setMonth( 0 );
        for (int row = 0; row < CalendarConstant.Month.ROW; row++) {
            for (int col = 0; col < CalendarConstant.Month.COL; col++) {
                Set<String> markers = new HashSet<String>();
                CalendarCellType type = CalendarCellType.CURRENT;
                if (dueDate.getYear() == today.getYear()
                        && dueDate.getMonth() == today.getMonth()) {
                   markers.add(CalendarConstant.CellStyleName.TODAY);
                }
                if (dueDate.getYear() == currentDate.getYear()
                        && dueDate.getMonth() == currentDate.getMonth()) {
                    markers.add(CalendarConstant.CellStyleName.ACTIVE);
                }
                view.addCell( row, col, CalendarUtils.getMonthNameShort( dueDate.getMonth() ), type, markers);
                dueDate.setMonth( dueDate.getMonth() + 1 );
            }
        }
    }
}
