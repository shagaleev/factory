package ru.brainworm.factory.core.datetimepicker.shared.dto;


import java.io.Serializable;
import java.util.Date;

/**
 * Интервал дат
 */
public class DateInterval implements Serializable {

    public DateInterval() {
        from = setBeginOfDay( new Date() );
        to = setEndOfDay( new Date() );
    }

    public DateInterval( Date from, Date to ) {
        this.from = from;
        this.to = to;
    }

    /**
     * Дата с
     */
    public Date from;

    /**
     * Дата по
     */
    public Date to;

    public boolean contains( Date currentDate ) {
        if ( from != null && to != null && currentDate != null ) {
            return ( currentDate.equals( from ) || currentDate.after( from )  )
                    && ( currentDate.equals( to ) || currentDate.before( to ) );
        }

        return false;
    }

    public boolean isValid() {
        return from != null
                && to != null
                && ( from.getTime() < to.getTime()
                || from.getTime() == to.getTime() );
    }

    public boolean isFromValid() {
        if ( from == null ) {
            return false;
        }

        if ( to == null ) {
            return true;
        } else {
            return ( from.getTime() < to.getTime()
                    || from.getTime() == to.getTime() );
        }
    }

    public boolean isEmpty() {
        return from == null
                && to == null;
    }

    private Date setBeginOfDay(Date day) {
        day.setHours(0);
        day.setMinutes(0);
        day.setSeconds(0);
        return day;
    }

    private Date setEndOfDay(Date day) {
        day.setHours(23);
        day.setMinutes(59);
        day.setSeconds(59);
        return day;
    }

    @Override
    public String toString() {
        return "DateInterval{" +
                "from=" + from +
                ", to=" + to +
                '}';
    }
}
