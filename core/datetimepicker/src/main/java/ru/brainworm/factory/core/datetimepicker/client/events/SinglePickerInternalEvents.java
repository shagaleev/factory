package ru.brainworm.factory.core.datetimepicker.client.events;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.UIObject;
import ru.brainworm.factory.core.datetimepicker.shared.dto.DateInterval;
import ru.brainworm.factory.core.datetimepicker.shared.dto.PickerType;
import ru.brainworm.factory.core.datetimepicker.shared.dto.TimeEnabledMode;

import java.util.Date;

/**
 * Набор внутренних событий виджета выбора даты/времени
 */
public class SinglePickerInternalEvents {

    /**
     * Событие показать форму контейнера в котором будут размещены
     * элементы календаря
     */
    public static class ShowPopup {
        public ShowPopup( HasWidgets parent, UIObject button, PickerType type, Date value, TimeEnabledMode timeEnabledMode ) {
            this.value = value;
            this.button = button;
            this.parent = parent;
            this.type = type;
            this.timeEnabledMode = timeEnabledMode;
        }

        public Date value;
        public UIObject button;
        public HasWidgets parent;
        public PickerType type;
        public TimeEnabledMode timeEnabledMode;
    }

    /**
     * Событие показать форму для выбора времени через контейнер
     */
    public static class ShowTimes {
        public ShowTimes( HasWidgets parent, PickerType type, Date value, TimeEnabledMode timeEnabledMode ) {
            this.parent = parent;
            this.value = value;
            this.type = type;
            this.timeEnabledMode = timeEnabledMode;
        }

        public HasWidgets parent;
        public Date value;
        public PickerType type;
        public TimeEnabledMode timeEnabledMode;
    }

    /**
     * Событие показать форму для выбора года
     */
    public static class ShowYears {
        public ShowYears( HasWidgets parent, PickerType type, Date value, TimeEnabledMode timeEnabledMode ) {
            this.parent = parent;
            this.value = value;
            this.type = type;
            this.timeEnabledMode = timeEnabledMode;
        }

        public HasWidgets parent;
        public Date value;
        public PickerType type;
        public TimeEnabledMode timeEnabledMode;
    }

    /**
     * Событие показать форму для выбора месяца
     */
    public static class ShowMonths {
        public ShowMonths( HasWidgets parent, PickerType type, Date value, TimeEnabledMode timeEnabledMode ) {
            this.parent = parent;
            this.value = value;
            this.type = type;
            this.timeEnabledMode = timeEnabledMode;
        }

        public HasWidgets parent;
        public Date value;
        public PickerType type;
        public TimeEnabledMode timeEnabledMode;
    }


    /**
     * Событие показать форму календаря для выбора дня месяца
     */
    public static class ShowDays {
        public ShowDays( HasWidgets parent, PickerType type, Date value, TimeEnabledMode timeEnabledMode ) {
            this.parent = parent;
            this.value = value;
            this.type = type;
            this.timeEnabledMode = timeEnabledMode;
        }

        public HasWidgets parent;
        public Date value;
        public PickerType type;
        public TimeEnabledMode timeEnabledMode;
    }


    /**
     * Событие при выборе  года, месяца, дня месяца и времени в dateTimePicker
     */
    public static class DateSelected {
        public DateSelected(HasWidgets parent, Date value) {
            this.parent = parent;
            this.value = value;
        }

        public HasWidgets parent;
        public Date value;
    }

    /**
     * When date and times has been chosen by user
     * set it into input field of picker.
     */
    public static class SetDateTime {
        public SetDateTime(HasWidgets parent, Date value) {
            this.parent = parent;
            this.value = value;
        }

        public HasWidgets parent;
        public Date value;

    }

    /**
     * Пометить диапазон дат
     */
    public static class MarkRange {
        public MarkRange( HasWidgets parent, DateInterval interval ) {
            this.parent = parent;
            this.interval = interval;
        }

        public HasWidgets parent;
        public DateInterval interval;
    }

}