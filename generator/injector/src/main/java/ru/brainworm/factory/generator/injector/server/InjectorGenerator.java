package ru.brainworm.factory.generator.injector.server;

import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.inject.rebind.GinjectorGenerator;
import com.google.gwt.user.rebind.ClassSourceFileComposerFactory;
import com.google.gwt.user.rebind.SourceWriter;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Класс, который будет генерировать активити с таблицами
 */
public class InjectorGenerator extends Generator {
    @Override
    public String generate( TreeLogger logger, GeneratorContext context, String typeName ) throws UnableToCompleteException {

        // default Ginjector implementation
        GinjectorGenerator generator = new GinjectorGenerator();

        // find all activity generators
        Map< String, Class<? extends Generator>> generators = new HashMap<String, Class<? extends Generator>>();
//        JClassType[] allGeneratedActivities = context.getTypeOracle().findType( Activity.class.getCanonicalName() ).getSubtypes();
//        for ( JClassType activityToGenerate : allGeneratedActivities ) {
//            generators.put( activityToGenerate.getQualifiedSourceName(), ActivityGenerator.class );
//        }
//
//        JClassType[] allGeneratedPlaces = context.getTypeOracle().findType( Place.class.getCanonicalName() ).getSubtypes();
//        for ( JClassType placeToGenerate : allGeneratedPlaces ) {
//            generators.put( placeToGenerate.getQualifiedSourceName(), PlaceGenerator.class );
//        }

        // wrapper for replacing all private fields with public fields
        GeneratorContextWrapper gcw = new GeneratorContextWrapper( context, generators );

        // generate all-public ginjector
        String ginjectorTypeName = generator.generate( logger, gcw, typeName );

        try {
            // custom Factory Injector implementation
            JClassType typeInfo = context.getTypeOracle().getType( typeName );

            String packageName = typeInfo.getPackage().getName();
            String simpleName = typeInfo.getSimpleSourceName() + "_FactoryInjector";

            ClassSourceFileComposerFactory composer = new ClassSourceFileComposerFactory( packageName, simpleName );
            composer.addImport( typeInfo.getQualifiedSourceName() );
            composer.setSuperclass( ginjectorTypeName );
            PrintWriter printWriter = context.tryCreate( logger, packageName, simpleName );
            if ( printWriter != null ) {
                SourceWriter sw = composer.createSourceWriter( context, printWriter );

                // static instance member
                sw.println( "public static "+ginjectorTypeName+" instance;" );

                // filling instance member in constructor
                sw.println( "public "+typeInfo.getName()+"_FactoryInjector() {" );
                sw.println( "  this.instance = this;" );
                sw.println( "}" );

                sw.commit( logger );
            }
            return packageName+"."+simpleName;
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }

        return null;
    }
}
