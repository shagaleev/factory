package ru.brainworm.factory.generator.injector.client;

import com.google.gwt.inject.client.Ginjector;

/**
 * Маркерный интерфейс
 */
public interface FactoryInjector extends Ginjector {
}
