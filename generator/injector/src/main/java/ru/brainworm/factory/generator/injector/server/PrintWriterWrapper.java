package ru.brainworm.factory.generator.injector.server;

import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.core.ext.typeinfo.JMethod;
import ru.brainworm.factory.generator.injector.client.PostConstruct;

import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 */
public class PrintWriterWrapper extends PrintWriter {

    static final private Logger LOG = Logger.getLogger(PrintWriterWrapper.class.getName());

    private final PrintWriter pw;
    private final TreeLogger treeLogger;
    private final GeneratorContext generatorContext;
    private final HashMap<String, String> generatedClasses = new HashMap<String, String>();
    private Map< String, Class< ? extends Generator> > generators = new HashMap<String, Class<? extends Generator>>();

    static {
        LOG.setLevel(Level.WARNING);
    }

    public PrintWriterWrapper(
            PrintWriter writer, TreeLogger treeLogger, GeneratorContext generatorContext,
            Map< String, Class<? extends Generator>> generators

    ) {
        super(writer);
        this.treeLogger = treeLogger;
        this.generatorContext = generatorContext;
        this.generators = generators;
        pw = writer;
    }

    private class Scope {
        String clazz;
        public Scope(String clazz) {
            this.clazz = clazz;
        }
    }

    private class BindingScope extends Scope {
        public BindingScope(String clazz) {
            super(clazz);
        }
    }

    private class AssistedInjectScope extends Scope {
        public AssistedInjectScope(String clazz) {
            super(clazz);
        }
    }

    private class SingletonScope extends Scope {
        public SingletonScope(String clazz) {
            super(clazz);
        }
    }

    private Stack<Scope> scopeQueue = new Stack<Scope>();

    public PrintWriter getOriginal() {
        return pw;
    }

    private void writePostConstruct( String clazz ) {
        int pos = clazz.indexOf( '<' );
        if ( pos > 0 ) {
            clazz = clazz.substring( 0, pos );
        }
        JClassType clazzType = generatorContext.getTypeOracle().findType(clazz);
        if(clazzType != null) {
            Map<String, JMethod> methods = new HashMap<String, JMethod>();

            for(JMethod method : getAllMethods(clazzType)) {
                if(method.isAnnotationPresent( PostConstruct.class ) ) {
                    methods.put(method.getName(), method);
                }
            }
            for(JMethod method : methods.values()) {
                super.write("// performing PostConstruct\n\t\t");
                super.write("result." + method.getName() + "();\n\t\t");
            }
        }
    }

    @Override
    public void write(String s) {
        super.write( processString( s ) );
    }



    @Override
    public void write( String s, int off, int len ) {
        String processed = processString( s.substring( off, off+len ) );
        super.write( processed, 0, processed.length() );
    }

    private String processString( String s ) {
        if(s.contains("private ") && s.contains(" singleton_Key")) {
            int startInd = s.indexOf("private ") + 8;
            int endInd = s.indexOf(" singleton_Key");
            String clazz = s.substring(startInd, endInd);
            scopeQueue.push(new SingletonScope(clazz));
        } else if(s.contains(" assistedInject_") && s.contains("public ")) {
            int startInd = s.indexOf("public ") + 7;
            int endInd = s.indexOf(" assistedInject_");
            String clazz = s.substring(startInd, endInd);
            scopeQueue.push(new AssistedInjectScope(clazz));
        } else if(s.contains("Binding for ")) {
            int startInd = s.indexOf("Binding for ") + 12;
            int endInd = s.indexOf(" declared at");
            String clazz = s.substring(startInd, endInd);
            scopeQueue.push(new BindingScope(clazz));
        } else if(s.contains("return result;") && !scopeQueue.empty()) {
            Scope scope = scopeQueue.pop();
            if(scope instanceof BindingScope) {
                super.write( "// done binding " + scope.clazz + "\n\t\t" );
                writePostConstruct( scope.clazz );
            } else if(scope instanceof AssistedInjectScope) {
                super.write( "// done with assisted inject " + scope.clazz + "\n\t\t" );
                writePostConstruct( scope.clazz );
            }
        } else if(s.contains("singleton_Key") && s.contains(" = result")) {
            Scope scope  = scopeQueue.pop();
            writePostConstruct( scope.clazz );
        } else if(scopeQueue.size() > 0 && scopeQueue.peek() instanceof AssistedInjectScope &&
                s.contains(" result =")) {
            scopeQueue.peek().clazz = s.substring(0, s.indexOf(" result ="));
        } else if(scopeQueue.size() > 0 && scopeQueue.peek() instanceof BindingScope &&
                s.contains("ImplementedBy")) {
            scopeQueue.pop();
        }

        if ( generators != null ) {
            if(s.contains("GWT.create(") || s.contains("return new")) {
                boolean isNew = s.contains("return new");
                String className;
                int startInd;
                int endInd;
                if(!isNew) {
                    startInd = s.indexOf("GWT.create(") + 11;
                    endInd = s.indexOf(".class);");
                } else {
                    startInd = s.indexOf("return new") + 11;
                    endInd = s.indexOf("(");
                }
                className = s.substring(startInd, endInd);
                for ( String generatedClass : generatedClasses.values() ) {
                    if ( className.equals( generatedClass ) ) {
                        return s;
                    }
                }
                JClassType clazz = generatorContext.getTypeOracle().findType(className);
                try {
                    for( Map.Entry<String, Class<? extends Generator>> generator : generators.entrySet() ) {
                        JClassType extClazz = generatorContext.getTypeOracle().findType( generator.getKey() );

                        if( clazz.isAssignableTo( extClazz ) ) {
                            String newClass = runExtension( clazz, generator.getValue() );
                            assert newClass != null;
                            generatedClasses.put(className, newClass);
                            return s.replaceAll( className, newClass );
                        }
                    }
                } catch(Exception e) {
                    LOG.log(Level.SEVERE, "Caught exception running extension", e);
                }

            }
        }
       return s.replace( "private ", "public " );
    }

    private String runExtension(JClassType clazz, Class<? extends Generator> generator) throws Exception {
        Generator gen = generator.newInstance();
        return gen.generate( treeLogger, generatorContext, clazz.getQualifiedSourceName() );
    }

    public static Set<JMethod> getAllMethods(JClassType classType) {
        Set<JMethod> methods = new HashSet<JMethod>();
        for(JClassType c : classType.getFlattenedSupertypeHierarchy()) {
            for(JMethod method : c.getMethods()) {
                methods.add(method);
            }
        }
        return methods;
    }
}