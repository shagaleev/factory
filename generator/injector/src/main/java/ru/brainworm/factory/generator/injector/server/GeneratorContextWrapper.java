package ru.brainworm.factory.generator.injector.server;

import com.google.gwt.core.ext.*;
import com.google.gwt.core.ext.linker.Artifact;
import com.google.gwt.core.ext.linker.GeneratedResource;
import com.google.gwt.core.ext.typeinfo.TypeOracle;
import com.google.gwt.dev.resource.ResourceOracle;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: shagaleev
 * Date: 5/20/14
 * Time: 2:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class GeneratorContextWrapper implements GeneratorContext {

    private final GeneratorContext generatorContext;
    private final Map< String, Class<? extends Generator>> generators;

    static final private Logger LOG = Logger.getLogger(GeneratorContextWrapper.class.getName());

    static {
        LOG.setLevel( Level.WARNING);
    }

    public GeneratorContextWrapper(
            GeneratorContext generatorContext, Map< String, Class<? extends Generator>> generators
    ) {
        this.generatorContext = generatorContext;
        this.generators = generators;
    }

    @Override
    public boolean checkRebindRuleAvailable(String s) {
        return generatorContext.checkRebindRuleAvailable(s);
    }

    @Override
    public void commit(TreeLogger treeLogger, PrintWriter writer) {
        if(writer instanceof PrintWriterWrapper) {
            generatorContext.commit(treeLogger,
                    ((PrintWriterWrapper)writer).getOriginal());
        } else {
            generatorContext.commit(treeLogger, writer);
        }
    }

    @Override
    public void commitArtifact(TreeLogger treeLogger, Artifact<?> artifact) throws UnableToCompleteException {
        generatorContext.commitArtifact(treeLogger, artifact);
    }

    @Override
    public GeneratedResource commitResource(TreeLogger treeLogger, OutputStream outputStream) throws UnableToCompleteException {
        return generatorContext.commitResource(treeLogger, outputStream);
    }

    @Override
    public PropertyOracle getPropertyOracle() {
        return generatorContext.getPropertyOracle();
    }

    @Override
    public ResourceOracle getResourcesOracle() {
        return generatorContext.getResourcesOracle();
    }

    @Override
    public TypeOracle getTypeOracle() {
        return generatorContext.getTypeOracle();
    }

    @Override
    public PrintWriter tryCreate(TreeLogger treeLogger, String s, String s1) {
        PrintWriter pw = generatorContext.tryCreate(treeLogger, s, s1);
        if(pw == null) {
            return null;
        }
        return new PrintWriterWrapper( pw, treeLogger, generatorContext, generators );
    }

    @Override
    public OutputStream tryCreateResource(TreeLogger treeLogger, String s) throws UnableToCompleteException {
        return generatorContext.tryCreateResource(treeLogger, s);
    }

    @Override
    public CachedGeneratorResult getCachedGeneratorResult() {
        return generatorContext.getCachedGeneratorResult();
    }

    @Override
    public boolean isGeneratorResultCachingEnabled() {
        return generatorContext.isGeneratorResultCachingEnabled();
    }

    @Override
    public boolean isProdMode() {
        return generatorContext.isProdMode();
    }

    @Override
    public boolean tryReuseTypeFromCache( String s ) {
        return generatorContext.tryReuseTypeFromCache( s );
    }
}