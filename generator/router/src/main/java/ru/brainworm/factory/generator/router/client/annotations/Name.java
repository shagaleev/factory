package ru.brainworm.factory.generator.router.client.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Аннотация для параметра метода
 */
@Target( {ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD} )
public @interface Name {

    /**
     * название переменной для сериализации
     */
    public String value();
}
