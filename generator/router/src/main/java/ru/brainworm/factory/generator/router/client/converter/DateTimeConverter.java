package ru.brainworm.factory.generator.router.client.converter;

import com.google.gwt.i18n.client.DateTimeFormat;

import java.util.Date;

/**
 * Конвертер для датовремени
 */
public class DateTimeConverter implements AbstractParamTypeConverter<Date>{
    @Override
    public String marshall( Date value ) {
        return value == null ? null : format.format( value );
    }

    @Override
    public Date unmarshall( String value ) {
        return value == null ? null : format.parse( value );
    }

    DateTimeFormat format = DateTimeFormat.getFormat( "dd-MM-yyyy'T'HH:mm:ss" );
}
