package ru.brainworm.factory.generator.router.server;

import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.*;
import com.google.gwt.user.rebind.ClassSourceFileComposerFactory;
import com.google.gwt.user.rebind.SourceWriter;
import ru.brainworm.factory.core.router.client.Router;
import ru.brainworm.factory.generator.router.client.annotations.Converter;
import ru.brainworm.factory.generator.router.client.annotations.Name;
import ru.brainworm.factory.generator.router.client.converter.DateTimeConverter;
import ru.brainworm.factory.generator.router.client.params.Params;

import java.io.PrintWriter;

/**
 * Класс, который будет парсить параметры
 */
public class ParamsGenerator extends Generator {
    @Override
    public String generate( TreeLogger logger, GeneratorContext context, String typeName ) throws UnableToCompleteException {
        try {
            this.log = logger;
            this.context = context;
            this.typeName = typeName;

            typeInfo = context.getTypeOracle().getType( typeName );
            markerTypeInfo = context.getTypeOracle().findType( Params.class.getName() );
            if ( !markerTypeInfo.isAssignableFrom( typeInfo ) ) {
                logger.log( TreeLogger.Type.ERROR, "Class " + typeName + " is not assignable from " + Router.class.getName() );
                return null;
            }

            packageName = typeInfo.getPackage().getName();
            String sourceClassName = typeName.substring( packageName.length()+1 ).replace( ".", "__" );
            simpleName = sourceClassName + "_Impl";

            ClassSourceFileComposerFactory composer = new ClassSourceFileComposerFactory( packageName, simpleName );
            composer.addImport( typeInfo.getQualifiedSourceName() );
            composer.addImplementedInterface( typeName );

            PrintWriter printWriter = context.tryCreate( logger, packageName, simpleName );
            if ( printWriter != null ) {
                SourceWriter sw = composer.createSourceWriter( context, printWriter );

                generateConstructor( sw );
                generateMembers( sw );
                generateGetters( sw );

                sw.commit( logger );
            }

            return packageName + "." + simpleName;
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
        return null;
    }

    private void generateMembers( SourceWriter sw ) {
        // print members
        sw.println( "private java.util.Map<String, String> params;" );
        sw.println( "" );
    }

    private void generateConstructor( SourceWriter sw ) {
        sw.println( "  public " + simpleName + "( java.util.Map<String, String> params ) {" );
        sw.println( "    this.params = params;" );
        sw.println( "  }" );
    }

    private void generateGetters( SourceWriter sw ) {
        for ( JMethod methodToGenerate : typeInfo.getInheritableMethods() ) {
            generateGetter( sw, methodToGenerate );
        }
    }

    private void generateGetter( SourceWriter sw, JMethod method ) {
        sw.println( "  public " + method.getReturnType().getQualifiedSourceName() + " " + method.getName() + "() {" );

        String parameterName = resolveParamName( method );

        sw.println( "    if ( !params.containsKey( \"" + parameterName + "\" ) ) {" );
        sw.println( "      return null;" );
        sw.println( "    }" );

        String parameterTypeName = method.getReturnType().getQualifiedSourceName();

        String conveterClassName = detectConverterClass( method );
        if ( conveterClassName != null ) {
            sw.println( "    return new " + conveterClassName + "().unmarshall( params.get( \""+parameterName+"\" ) ); ");
        }
        else if ( parameterTypeName.equals( "java.lang.Integer" ) ) {
            sw.println( "    return Integer.parseInt( params.get( \"" + parameterName + "\" ) );" );
        } else if ( parameterTypeName.equals("java.lang.Long")) {
            sw.println( "    return Long.parseLong( params.get( \"" + parameterName + "\" ) );" );
        } else if (parameterTypeName.equals("java.lang.String")) {
            sw.println( "    return params.get( \"" + parameterName + "\" );" );
        } else {
            log.log( TreeLogger.Type.ERROR, "Unknown parameter type: " + parameterTypeName );
        }

        sw.println( "  }" );
        sw.println( "" );
    }

    private String resolveParamName( JMethod method ) {
        Name nameAnnotation = method.getAnnotation( Name.class );
        if ( nameAnnotation != null ) {
            return nameAnnotation.value();
        }

        String methodName = method.getName();
        return methodName.toLowerCase().substring( 3, 4 ) + methodName.substring( 4 );
    }

    private String detectConverterClass( JMethod method ) {
        Converter converterAnnotation = method.getAnnotation( Converter.class );
        if ( converterAnnotation != null ) {
            return converterAnnotation.value().getCanonicalName();
        }

        JType returnType = method.getReturnType();
        if ( returnType.getQualifiedSourceName().equals( "java.util.Date" ) ) {
            return DateTimeConverter.class.getCanonicalName();
        }

        JEnumType mayBeEnum = returnType.isEnum();
        if ( mayBeEnum != null ) {
            return generateEnumConverter( mayBeEnum );
        }

        return null;
    }

    private String generateEnumConverter( JEnumType enumType ) {
        String generatedTypeName = EnumConverterGenerator.getQualifiedSourceNameForGeneratedClass( enumType );
        JClassType type = context.getTypeOracle().findType( generatedTypeName );
        if ( type == null ) {
            try {
                return EnumConverterGenerator.class.newInstance().generate( log, context, enumType.getQualifiedSourceName() );
            }
            catch ( Exception e ) {
                log.log( TreeLogger.Type.ERROR, e.toString() );
            }
        }
        return type.getQualifiedSourceName();
    }

    TreeLogger log;
    GeneratorContext context;
    String typeName;

    JClassType typeInfo;
    JClassType markerTypeInfo;

    String packageName;
    String simpleName;
}
