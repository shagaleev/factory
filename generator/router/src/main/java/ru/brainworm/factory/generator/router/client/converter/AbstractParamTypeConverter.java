package ru.brainworm.factory.generator.router.client.converter;

/**
 * Абстрактный конвертер для типов
 */
public interface AbstractParamTypeConverter<T> {
    /**
     * Сериализовать тип в строку
     * @param value    исходный объект
     * @return сериализованная строка
     */
    String marshall( T value );

    /**
     * Десериализовать
     * @param value    сериализованная строка
     * @return десериализованный объект
     */
    T unmarshall( String value );
}
