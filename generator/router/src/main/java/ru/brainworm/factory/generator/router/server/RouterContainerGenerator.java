package ru.brainworm.factory.generator.router.server;

import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.user.rebind.ClassSourceFileComposerFactory;
import com.google.gwt.user.rebind.SourceWriter;
import ru.brainworm.factory.core.router.client.Router;

import java.io.PrintWriter;

/**
 * Класс, который будет контейнером для роутера
 */
public class RouterContainerGenerator extends Generator {
    @Override
    public String generate( TreeLogger logger, GeneratorContext context, String typeName ) throws UnableToCompleteException {
        try {
            this.log = logger;
            this.context = context;
            this.typeName = typeName;

            typeInfo = context.getTypeOracle().getType( typeName );
            markerTypeInfo = context.getTypeOracle().findType( Router.class.getName() );
            if ( !markerTypeInfo.isAssignableFrom( typeInfo ) ) {
                logger.log( TreeLogger.Type.ERROR, "Class " + typeName + " is not assignable from " + Router.class.getName() );
                return null;
            }

            packageName = typeInfo.getPackage().getName();
            simpleName = typeInfo.getSimpleSourceName() + "_Container";

            ClassSourceFileComposerFactory composer = new ClassSourceFileComposerFactory( packageName, simpleName );
            composer.addImport( typeInfo.getQualifiedSourceName() );

            PrintWriter printWriter = context.tryCreate( logger, packageName, simpleName );
            if ( printWriter != null ) {
                SourceWriter sw = composer.createSourceWriter( context, printWriter );

                generateMembers( sw );
                generateGetter( sw );

                sw.commit( logger );
            }

            return typeName + "_Container";
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
        return null;
    }

    private void generateMembers( SourceWriter sw ) {
        // print members
        sw.println( "private static ru.brainworm.factory.core.router.client.Router router;" );
        sw.println( "" );
    }

    private void generateGetter( SourceWriter sw ) {
        sw.println( "  public static ru.brainworm.factory.core.router.client.Router getRouter() {" );
        sw.println( "    if ( router == null ) {" );
        sw.println( "      router = new ru.brainworm.factory.core.router.client.Router();" );
        sw.println( "    }" );
        sw.println( "    return router;" );
        sw.println( "  }" );
        sw.println( "" );
    }

    TreeLogger log;
    GeneratorContext context;
    String typeName;

    JClassType typeInfo;
    JClassType markerTypeInfo;

    String packageName;
    String simpleName;
}
