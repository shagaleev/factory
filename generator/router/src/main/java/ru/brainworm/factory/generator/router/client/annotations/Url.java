package ru.brainworm.factory.generator.router.client.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Аннотация для класса — представляющего собой место
 */
@Target( ElementType.TYPE )
public @interface Url {

    /**
     * Строка для перехода
     */
    public String value();
}
