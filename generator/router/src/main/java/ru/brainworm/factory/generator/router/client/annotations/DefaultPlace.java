package ru.brainworm.factory.generator.router.client.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Аннотация для поля — менеджера истории
 */
@Target( ElementType.FIELD )
public @interface DefaultPlace {
    /**
     * Интерфейс класса place
     */
    public Class<?> value();
}
