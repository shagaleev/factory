package ru.brainworm.factory.generator.router.client.place;

/**
 * Интерфейс который надо инжектить для того чтобы иметь возможность уйти назад
 */
public abstract class PlaceHistory {
    /**
     * Метод возвращающий нас назад по истории
     */
    public abstract void back();

    /**
     * Количество переходов
     */
    public static int historySize = 0;
}
