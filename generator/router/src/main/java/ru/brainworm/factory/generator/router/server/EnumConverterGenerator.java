package ru.brainworm.factory.generator.router.server;

import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.*;
import com.google.gwt.user.rebind.ClassSourceFileComposerFactory;
import com.google.gwt.user.rebind.SourceWriter;
import ru.brainworm.factory.generator.router.client.annotations.Converter;
import ru.brainworm.factory.generator.router.client.annotations.Name;
import ru.brainworm.factory.generator.router.client.annotations.Url;
import ru.brainworm.factory.generator.router.client.place.Place;

import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Класс, который будет генерировать конвертер для Enum-ов
 */
public class EnumConverterGenerator extends Generator {

    public static String getQualifiedSourceNameForGeneratedClass( JClassType typeInfo ) {
        String packageName = typeInfo.getPackage().getName();
        String sourceClassName = typeInfo.getQualifiedSourceName().substring( packageName.length()+1 ).replace( ".", "__" );
        String simpleName = sourceClassName + "_PlaceConverter";
        return packageName + "." + simpleName;
    }

    @Override
    public String generate( TreeLogger logger, GeneratorContext context, String typeName ) throws UnableToCompleteException {
        try {
            this.log = logger;
            this.context = context;
            this.typeName = typeName;

            typeInfo = context.getTypeOracle().getType( typeName );
            JEnumType enumType = typeInfo.isEnum();
            if ( enumType == null ) {
                logger.log( TreeLogger.Type.ERROR, "Class " + typeName + " is not enum" );
                return null;
            }

            packageName = typeInfo.getPackage().getName();
            String sourceClassName = typeName.substring( packageName.length()+1 ).replace( ".", "__" );
            simpleName = sourceClassName + "_PlaceConverter";

            ClassSourceFileComposerFactory composer = new ClassSourceFileComposerFactory( packageName, simpleName );
            composer.addImport( typeInfo.getQualifiedSourceName() );
            composer.addImport( "com.google.inject.Inject" );
            composer.addImplementedInterface( "ru.brainworm.factory.generator.router.client.converter.AbstractParamTypeConverter<"+typeInfo.getQualifiedSourceName()+">" );

            PrintWriter printWriter = context.tryCreate( logger, packageName, simpleName );
            if ( printWriter != null ) {
                SourceWriter sw = composer.createSourceWriter( context, printWriter );

                generateMarshall( sw );
                generateUnmarshall( sw );

                sw.commit( logger );
            }

            return packageName + "." + simpleName;
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
        return null;
    }

    private void generateMarshall( SourceWriter sw ) {
        sw.println( "  public java.lang.String marshall( " + typeInfo.getQualifiedSourceName() + " value ) {");
        sw.println( "    return value == null ? null : value.name().toLowerCase();" );
        sw.println( "  }" );
        sw.println();
    }

    private void generateUnmarshall( SourceWriter sw ) {
        sw.println( "  public "+typeInfo.getQualifiedSourceName()+" unmarshall( java.lang.String value ) {");
        sw.println( "    return value == null ? null : "+typeInfo.getQualifiedSourceName()+".valueOf( value.toUpperCase() );" );
        sw.println( "  }" );
        sw.println();
    }

    TreeLogger log;
    GeneratorContext context;
    String typeName;

    JClassType typeInfo;

    String packageName;
    String simpleName;
}
