package ru.brainworm.factory.generator.router.client.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Аннотация для метода — обработчика события изменения адресной строчки
 */
@Target( ElementType.METHOD )
public @interface Route {

    /**
     * Интерфейс класса place
     */
    public Class<?> value();
}
