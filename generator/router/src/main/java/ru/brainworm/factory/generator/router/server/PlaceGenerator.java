package ru.brainworm.factory.generator.router.server;

import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.*;
import com.google.gwt.user.rebind.ClassSourceFileComposerFactory;
import com.google.gwt.user.rebind.SourceWriter;
import ru.brainworm.factory.generator.router.client.annotations.Converter;
import ru.brainworm.factory.generator.router.client.annotations.Name;
import ru.brainworm.factory.generator.router.client.annotations.Url;
import ru.brainworm.factory.generator.router.client.converter.DateTimeConverter;
import ru.brainworm.factory.generator.router.client.place.Place;

import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Класс, который будет генерировать активити с таблицами
 */
public class PlaceGenerator extends Generator {

    public static String getQualifiedSourceNameForGeneratedClass( JClassType typeInfo ) {
        String packageName = typeInfo.getPackage().getName();
        String sourceClassName = typeInfo.getQualifiedSourceName().substring( packageName.length()+1 ).replace( ".", "__" );
        String simpleName = sourceClassName + "_Impl";
        return packageName + "." + simpleName;
    }

    @Override
    public String generate( TreeLogger logger, GeneratorContext context, String typeName ) throws UnableToCompleteException {
        try {
            this.log = logger;
            this.context = context;
            this.typeName = typeName;

            typeInfo = context.getTypeOracle().getType( typeName );
            markerTypeInfo = context.getTypeOracle().findType( Place.class.getName() );
            if ( !markerTypeInfo.isAssignableFrom( typeInfo ) ) {
                logger.log( TreeLogger.Type.ERROR, "Class " + typeName + " is not assignable from " + Place.class.getName() );
                return null;
            }

            packageName = typeInfo.getPackage().getName();
            String sourceClassName = typeName.substring( packageName.length()+1 ).replace( ".", "__" );
            simpleName = sourceClassName + "_Impl";

            ClassSourceFileComposerFactory composer = new ClassSourceFileComposerFactory( packageName, simpleName );
            composer.addImport( typeInfo.getQualifiedSourceName() );
            composer.addImport( "com.google.inject.Inject" );
            composer.addImplementedInterface( typeInfo.getName() );

            parse();

            PrintWriter printWriter = context.tryCreate( logger, packageName, simpleName );
            if ( printWriter != null ) {
                SourceWriter sw = composer.createSourceWriter( context, printWriter );

                generateConstrustor( sw );
                generateMethods( sw );

                sw.commit( logger );
            }

            return packageName + "." + simpleName;
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
        return null;
    }

    private void generateConstrustor( SourceWriter sw ) {
        sw.println( "public "+simpleName+"() {}" );
    }

    private void generateMethods( SourceWriter sw ) {
        for ( JMethod method : typeInfo.getInheritableMethods() ) {
            JType methodReturnType = method.getReturnType();

            if ( methodReturnType.getQualifiedSourceName().equals( "void" ) ) {
                generateGoTo( sw, method );
            }
            else if ( methodReturnType.getQualifiedSourceName().equals( "java.lang.String" ) ) {
                generateMakeUrl( sw, method );
            }
        }
    }

    private void parse() {
        goToMethods.clear();

        urlAnnotation = typeInfo.getAnnotation( Url.class );
        if ( urlAnnotation == null ) {
            log.log( TreeLogger.Type.ERROR, "Place class without @Url annotation" );
        }

        for ( JMethod method : typeInfo.getInheritableMethods() ) {
            if ( !method.getReturnType().getQualifiedSourceName().equals( "java.lang.String" ) ) {
                continue;
            }

            goToMethods.add( method );
        }
    }

    private void generateGoTo( SourceWriter sw, JMethod method ) {
        generateRouterContainer();
        generateBody( sw, method, "void", "ru.brainworm.factory.core.router.client.Router_Container.getRouter().goTo( sb.toString() );" );
    }

    private void generateMakeUrl( SourceWriter sw, JMethod method ) {
        generateBody( sw, method, "java.lang.String", "return \"#\" + sb.toString();" );
    }

    private void generateBody( SourceWriter sw, JMethod method, String returnType, String returnExpression ) {
        sw.println( "  public "+returnType+" "+method.getName()+"( " );
        boolean needComma = false;
        for ( JParameter parameter : method.getParameters() ) {
            sw.println( "    " + (needComma ? ", " : "" ) + parameter.getType().getQualifiedSourceName() + " " + parameter.getName() );
            needComma = true;
        }
        sw.println( " ) {" );

        sw.println( "    ru.brainworm.factory.generator.router.client.place.PlaceHistory.historySize++;" );
        sw.println( "    StringBuilder sb = new StringBuilder( \"" + urlAnnotation.value() + "\" );" );
        sw.println( "    Integer paramCounter = 0;" );
        for ( JParameter parameter : method.getParameters() ) {
            Name nameAnnotation = parameter.getAnnotation( Name.class );
            String name = parameter.getName();
            if ( nameAnnotation != null ) {
                name = nameAnnotation.value();
            }

            sw.println( "    if ( " + parameter.getName() + " != null ) { " );
            sw.println( "      if ( paramCounter.equals( 0 ) ) {" );
            sw.println( "         sb.append( \"?\" );" );
            sw.println( "      }" );
            sw.println( "      else {" );
            sw.println( "         sb.append( \"&\" );" );
            sw.println( "      }" );
            sw.println( "      ++paramCounter;" );
            sw.println( "      sb.append( \"" + name + "\" );" );
            sw.println( "      sb.append( \"=\" );" );
            sw.print(   "      sb.append( " );

            String converterClassName = detectConverterClass( parameter );
            if ( converterClassName != null ) {
                sw.print( " new " + converterClassName+ "().marshall( " + parameter.getName() + " )" );
            }
            else {
                sw.print( parameter.getName() );
            }

            sw.print( " );" );
            sw.println( "    }" );
        }

        sw.println( "    "+returnExpression );
        sw.println( "  }" );
        sw.println( "" );
    }

    private String detectConverterClass( JParameter parameter ) {
        Converter converterAnnotation = parameter.getAnnotation( Converter.class );
        if ( converterAnnotation != null ) {
            return converterAnnotation.value().getCanonicalName();
        }

        JType paramType = parameter.getType();
        if ( paramType.getQualifiedSourceName().equals( "java.util.Date" ) ) {
            return DateTimeConverter.class.getCanonicalName();
        }

        JEnumType mayBeEnum = paramType.isEnum();
        if ( mayBeEnum != null ) {
            return generateEnumConverter( mayBeEnum );
        }

        return null;
    }

    private String generateEnumConverter( JEnumType enumType ) {
        String generatedTypeName = EnumConverterGenerator.getQualifiedSourceNameForGeneratedClass( enumType );
        JClassType type = context.getTypeOracle().findType( generatedTypeName );
        if ( type == null ) {
            try {
                return EnumConverterGenerator.class.newInstance().generate( log, context, enumType.getQualifiedSourceName() );
            }
            catch ( Exception e ) {
                log.log( TreeLogger.Type.ERROR, e.toString() );
            }
        }
        return type.getQualifiedSourceName();
    }

    private void generateRouterContainer() {
        JClassType routerContainterType = context.getTypeOracle().findType( "ru.brainworm.factory.core.router.client.Router_Container" );
        if ( routerContainterType == null ) {
            try {
                RouterContainerGenerator.class.newInstance().generate( log, context, "ru.brainworm.factory.core.router.client.Router" );
            }
            catch ( Exception e ) {
                log.log( TreeLogger.Type.ERROR, e.toString() );
            }
        }
    }

    TreeLogger log;
    GeneratorContext context;
    String typeName;

    JClassType typeInfo;
    JClassType markerTypeInfo;

    String packageName;
    String simpleName;

    Url urlAnnotation;

    ArrayList<JMethod> goToMethods = new ArrayList<JMethod>();
}
