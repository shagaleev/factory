package ru.brainworm.factory.generator.common.client.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Аннотация для события инициализации
 */
@Target( ElementType.TYPE )
public @interface InitEvent {
    Class<?> type();
    String parentName();
}
