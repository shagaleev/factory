package ru.brainworm.factory.generator.common.client.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Аннотация для события изменения
 */
@Target( ElementType.TYPE )
public @interface ChangeEvent {
    Class<?> type();
    String name();
    String container() default "";
}
