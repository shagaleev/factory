package ru.brainworm.factory.generator.common.client.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Аннотация для события показа
 */
@Target( ElementType.TYPE )
public @interface ShowEvent {
    Class<?> type();
    String name() default "";
}
