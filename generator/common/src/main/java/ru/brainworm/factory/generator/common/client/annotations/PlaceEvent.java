package ru.brainworm.factory.generator.common.client.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Аннотация для события поместить в адресную строку
 */
@Target( ElementType.TYPE )
public @interface PlaceEvent {
    Class<?> type();
    String parentName();
}
