package ru.brainworm.factory.generator.common.client.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Аннотация для метода, который надо вызвать после того как событие "Show" выполнилось
 */
@Target( ElementType.METHOD )
public @interface ShowEventHandler {
}
