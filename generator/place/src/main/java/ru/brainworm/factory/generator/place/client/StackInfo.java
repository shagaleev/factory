package ru.brainworm.factory.generator.place.client;

/**
 * Информация о положении place-а внутри стека
 */
public class StackInfo {

    /**
     * Порядковый номер внутри place-а (с 0)
     */
    public int index;

    /**
     * Общее количество элементов в стеке включая данный
     */
    public int stackSize;

    /**
     * Порядковый номер среди place-ов данного типа
     */
    public int typeIndex;

    /**
     * Общее число элементов данного типа включая этот
     */
    public int typeSize;

    /**
     * Текущий URL
     */
    public String currentUrl;
}
