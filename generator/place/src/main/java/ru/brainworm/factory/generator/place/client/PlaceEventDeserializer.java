package ru.brainworm.factory.generator.place.client;

/**
 * Created by shagaleev on 10/21/14.
 */
public interface PlaceEventDeserializer<T extends PlaceEvent > {
    public T deserialize( String segment );
}
