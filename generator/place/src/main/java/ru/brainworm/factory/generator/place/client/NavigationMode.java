package ru.brainworm.factory.generator.place.client;

/**
 * Тип навигации для работы с адресной строкой
 */
public enum NavigationMode {
    /**
     * Добавление historyItem
     */
    ADD,

    /**
     * Замена historyToken
     */
    REPLACE
}
