package ru.brainworm.factory.generator.place.client;

/**
 * Created by shagaleev on 10/21/14.
 */
public interface PlaceEventSerializer<T extends PlaceEvent > {
    public String serialize( T event );
}
