package ru.brainworm.factory.generator.place.client;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Аннотация для поля — не требующего сериализации
 */
@Target( ElementType.FIELD )
public @interface Omit {

    /**
     * Область применения поля, не требующего сериализации
     */
    public ScopeType scope() default ScopeType.FULL;
}
