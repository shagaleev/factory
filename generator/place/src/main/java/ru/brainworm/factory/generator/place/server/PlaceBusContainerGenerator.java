package ru.brainworm.factory.generator.place.server;

import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.user.rebind.ClassSourceFileComposerFactory;
import com.google.gwt.user.rebind.SourceWriter;
import ru.brainworm.factory.generator.place.client.PlaceBus;

import java.io.PrintWriter;

/**
 * Класс, который будет контейнером для роутера
 */
public class PlaceBusContainerGenerator extends Generator {
    @Override
    public String generate( TreeLogger logger, GeneratorContext context, String typeName ) throws UnableToCompleteException {
        try {
            this.log = logger;
            this.context = context;
            this.typeName = typeName;

            typeInfo = context.getTypeOracle().getType( typeName );
            markerTypeInfo = context.getTypeOracle().findType( PlaceBus.class.getName() );

            packageName = typeInfo.getPackage().getName();
            simpleName = typeInfo.getSimpleSourceName() + "_Container";

            ClassSourceFileComposerFactory composer = new ClassSourceFileComposerFactory( packageName, simpleName );
            composer.addImport( typeInfo.getQualifiedSourceName() );


            PrintWriter printWriter = context.tryCreate( logger, packageName, simpleName );
            if ( printWriter != null ) {
                SourceWriter sw = composer.createSourceWriter( context, printWriter );

                generateMembers( sw );
                generateGetter( sw );

                sw.commit( logger );
            }

            return typeName + "_Container";
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
        return null;
    }

    private void generateMembers( SourceWriter sw ) {
        // print members
        sw.println( "private static ru.brainworm.factory.generator.place.client.PlaceBus bus;" );
        sw.println( "" );
    }

    private void generateGetter( SourceWriter sw ) throws Exception {
        sw.println( "  public static ru.brainworm.factory.generator.place.client.PlaceBus getBus() {" );
        sw.println( "    if ( bus == null ) {" );
        sw.println( "      java.util.Map< java.lang.Class, ru.brainworm.factory.generator.place.client.PlaceEventSerializer > serializerMap = new java.util.HashMap< java.lang.Class, ru.brainworm.factory.generator.place.client.PlaceEventSerializer >();" );
        sw.println( "      java.util.List< ru.brainworm.factory.generator.place.client.PlaceEventDeserializer > deserializerList = new java.util.ArrayList< ru.brainworm.factory.generator.place.client.PlaceEventDeserializer >();" );
        sw.println( "      java.util.Map< java.lang.Class, ru.brainworm.factory.generator.place.client.PlaceEventCopier > copierMap = new java.util.HashMap< java.lang.Class, ru.brainworm.factory.generator.place.client.PlaceEventCopier >();" );

        JClassType[] placeEvents = context.getTypeOracle().findType( "ru.brainworm.factory.generator.place.client.PlaceEvent" ).getSubtypes();
        for ( JClassType event : placeEvents ) {
            String serializerClassName = getSerializerClassName( event );
            sw.println( "      serializerMap.put( "+event.getQualifiedSourceName()+".class, new "+serializerClassName+"() );" );
            sw.println( "      deserializerList.add( new "+serializerClassName+"() );" );
            sw.println( "      copierMap.put( "+event.getQualifiedSourceName()+".class, new "+serializerClassName+"() );" );
        }

        sw.println( "      bus = new ru.brainworm.factory.generator.place.client.PlaceBus( serializerMap, deserializerList, copierMap );" );
        sw.println( "    }" );
        sw.println( "    return bus;" );
        sw.println( "  }" );
        sw.println( "" );
    }

    private String getSerializerClassName( JClassType event ) throws Exception {
        String serializerClassName = PlaceEventGenerator.getQualifiedSourceNameForGeneratedClass( event );

        JClassType generatedClassType = context.getTypeOracle().findType( serializerClassName );
        if ( generatedClassType == null ) {
            try {
                PlaceEventGenerator.class.newInstance().generate( log, context, event.getQualifiedSourceName() );
            }
            catch ( Exception e ) {
                log.log( TreeLogger.Type.ERROR, e.toString() );
                throw e;
            }
        }
        return serializerClassName;
    }

    TreeLogger log;
    GeneratorContext context;
    String typeName;

    JClassType typeInfo;
    JClassType markerTypeInfo;

    String packageName;
    String simpleName;
}
