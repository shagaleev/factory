package ru.brainworm.factory.generator.place.server;

import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.core.ext.typeinfo.JEnumType;
import com.google.gwt.core.ext.typeinfo.JField;
import com.google.gwt.core.ext.typeinfo.JType;
import com.google.gwt.user.rebind.ClassSourceFileComposerFactory;
import com.google.gwt.user.rebind.SourceWriter;
import ru.brainworm.factory.generator.place.client.Omit;
import ru.brainworm.factory.generator.place.client.PlaceEvent;
import ru.brainworm.factory.generator.place.client.ScopeType;
import ru.brainworm.factory.generator.router.client.annotations.Converter;
import ru.brainworm.factory.generator.router.client.annotations.Name;
import ru.brainworm.factory.generator.router.client.annotations.Url;
import ru.brainworm.factory.generator.router.client.converter.DateTimeConverter;
import ru.brainworm.factory.generator.router.server.EnumConverterGenerator;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Класс, который будет генерировать сериализаторы для place-ов
 */
public class PlaceEventGenerator extends Generator {

    public static String getQualifiedSourceNameForGeneratedClass( JClassType typeInfo ) {
        String packageName = typeInfo.getPackage().getName();
        String sourceClassName = typeInfo.getQualifiedSourceName().substring( packageName.length()+1 ).replace( ".", "__" );
        String simpleName = sourceClassName + "_Impl";
        return packageName + "." + simpleName;
    }

    @Override
    public String generate( TreeLogger logger, GeneratorContext context, String typeName ) throws UnableToCompleteException {
        try {
            this.log = logger;
            this.context = context;
            this.typeName = typeName;

            typeInfo = context.getTypeOracle().getType( typeName );
            markerTypeInfo = context.getTypeOracle().findType( PlaceEvent.class.getName() );
            if ( !markerTypeInfo.isAssignableFrom( typeInfo ) ) {
                logger.log( TreeLogger.Type.ERROR, "Class " + typeName + " is not assignable from " + PlaceEvent.class.getName() );
                return null;
            }

            packageName = typeInfo.getPackage().getName();
            String sourceClassName = typeName.substring( packageName.length()+1 ).replace( ".", "__" );
            simpleName = sourceClassName + "_Impl";

            ClassSourceFileComposerFactory composer = new ClassSourceFileComposerFactory( packageName, simpleName );
            composer.addImport( typeInfo.getQualifiedSourceName() );
            composer.addImport( "com.google.inject.Inject" );
            composer.addImplementedInterface( "ru.brainworm.factory.generator.place.client.PlaceEventSerializer<"+typeInfo.getName()+">" );
            composer.addImplementedInterface( "ru.brainworm.factory.generator.place.client.PlaceEventDeserializer<"+typeInfo.getName()+">" );
            composer.addImplementedInterface( "ru.brainworm.factory.generator.place.client.PlaceEventCopier<"+typeInfo.getName()+">" );

            parse();

            PrintWriter printWriter = context.tryCreate( logger, packageName, simpleName );
            if ( printWriter != null ) {
                SourceWriter sw = composer.createSourceWriter( context, printWriter );

                generateConstrustor( sw );
                generateSerialize( sw );
                generateDeserialize( sw );
                generateCopy( sw );
                generateFields( sw );

                sw.commit( logger );
            }

            return packageName + "." + simpleName;
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
        return null;
    }

    private void generateConstrustor( SourceWriter sw ) {
        sw.println( "public "+simpleName+"() {}" );
    }

    private void parse() {
        urlAnnotation = typeInfo.getAnnotation( Url.class );
    }

    private void generateSerialize( SourceWriter sw ) {
        sw.println( "public String serialize( " + typeName + " event ) {" );
        sw.println( "  java.lang.StringBuilder sb = new java.lang.StringBuilder(); ");

        String prefix = typeInfo.getName();
        if ( urlAnnotation != null ) {
            prefix = urlAnnotation.value();
        }

        sw.println( "  sb.append( \"" + prefix + "\" );" );
        sw.println( "  Integer counter = 0;" );

        for ( JField field : getFlattenedFields( typeInfo ) ) {

            String name = field.getName();
            Name nameAnnotation = field.getAnnotation( Name.class );
            if ( nameAnnotation != null ) {
                name = nameAnnotation.value();
            }

            Omit omitAnnotation = field.getAnnotation( Omit.class );
            if ( omitAnnotation != null ) {
                continue;
            }

            sw.println( "  if ( counter > 0 ) sb.append( \";\" ); " );
            sw.println( "  if ( event." + field.getName() + " != null ) {"  );
            sw.println( "    if ( counter == 0 ) sb.append( \":\" );" );
            sw.println( "    counter++;" );
            sw.println( "    sb.append( \"" + name + "\" );" );
            sw.println( "    sb.append( \"=\" );" );
            sw.print(   "    sb.append( " );

            String converterClassName = detectConverterClass( field );
            if ( converterClassName != null ) {
                sw.print( " new " + converterClassName+ "().marshall( event." + field.getName() + " )" );
            }
            else {
                sw.print( " event." + field.getName() );
            }

            sw.println( " );" );

            sw.println( "  }" );
        }
        sw.println( "  return sb.toString();" );
        sw.println( "}" );
        sw.println();
    }

    private void generateDeserialize( SourceWriter sw ) {

        String prefix = typeInfo.getName();
        if ( urlAnnotation != null ) {
            prefix = urlAnnotation.value();
        }

        sw.println( "public " + typeInfo.getQualifiedSourceName() + " deserialize( String token ) {" );
        sw.println( "  int prefixIndex = token.indexOf( \":\" );" );
        sw.println( "  String prefix = prefixIndex == -1 ? token : token.substring( 0, prefixIndex );" );
        sw.println( "  if ( !\""+prefix+"\".equals( prefix ) ) return null;" );
        sw.println( "  "+typeInfo.getQualifiedSourceName()+" result = new "+typeInfo.getQualifiedSourceName()+"();" );
        sw.println( "  log.info( \"token=\"+token );" );
        sw.println( "  if ( prefixIndex+1 >= token.length()-1 ) return result;" );
        sw.println( "  String paramString = token.substring( prefixIndex+1, token.length() );" );
        sw.println( "  if ( paramString.isEmpty() ) return result;" );
        sw.println( "  String[] params = paramString.split(\";\");" );
        sw.println( "  for ( String param: params ) {" );
        sw.println( "    String[] paramParts = param.split(\"=\");" );

        for ( JField field : getFlattenedFields( typeInfo ) ) {
            if ( field.getAnnotation( Omit.class ) != null ) {
                continue;
            }

            String name = field.getName();
            Name nameAnnotation = field.getAnnotation( Name.class );
            if ( nameAnnotation != null ) {
                name = nameAnnotation.value();
            }

            sw.println( "    if ( \""+name+"\".equals( paramParts[0] ) ) {" );
            sw.print(   "      result." + field.getName() + " = " );

            String parameterTypeName = field.getType().getQualifiedSourceName();

            String conveterClassName = detectConverterClass( field );
            if ( conveterClassName != null ) {
                sw.println( " new " + conveterClassName + "().unmarshall( paramParts[1] );" );
            } else if ( parameterTypeName.equals( "java.lang.Boolean" ) ) {
                sw.println( " Boolean.valueOf( paramParts[ 1 ] );" );
            } else if ( parameterTypeName.equals( "java.lang.Integer" ) ) {
                sw.println( " Integer.parseInt( paramParts[ 1 ] );" );
            } else if ( parameterTypeName.equals("java.lang.Long") ) {
                sw.println( " Long.parseLong( paramParts[ 1 ] );" );
            } else if ( parameterTypeName.equals("java.math.BigInteger") ) {
                sw.println( " new java.math.BigInteger( paramParts[ 1 ] );" );
            } else if (parameterTypeName.equals("java.lang.String") ) {
                sw.println( " paramParts[ 1 ];" );
            } else {
                log.log( TreeLogger.Type.ERROR, "Unknown parameter type: " + parameterTypeName );
            }

            sw.println( "    }" );
        }
        sw.println( "  }" );
        sw.println( "  return result;" );
        sw.println( "}" );
    }

    private void generateCopy( SourceWriter sw ) {
        sw.println( "public void copy( "+typeInfo.getQualifiedSourceName() + " from, " + typeInfo.getQualifiedSourceName()+" to ) {" );
        for ( JField field : getFlattenedFields( typeInfo ) ) {
            Omit omit = field.getAnnotation( Omit.class );
            if ( omit != null && omit.scope() == ScopeType.FULL ) {
                continue;
            }

            sw.println( "  to."+field.getName()+" = from."+field.getName()+";" );
        }
        sw.println( "}" );
    }

    private String detectConverterClass( JField parameter ) {
        Converter converterAnnotation = parameter.getAnnotation( Converter.class );
        if ( converterAnnotation != null ) {
            return converterAnnotation.value().getCanonicalName();
        }

        JType paramType = parameter.getType();
        if ( paramType.getQualifiedSourceName().equals( "java.util.Date" ) ) {
            return DateTimeConverter.class.getCanonicalName();
        }

        JEnumType mayBeEnum = paramType.isEnum();
        if ( mayBeEnum != null ) {
            return generateEnumConverter( mayBeEnum );
        }

        return null;
    }

    private Set<JField> getFlattenedFields( JClassType typeInfo ) {
        Set<JField> fieldsToScan = new HashSet< JField >();
        for ( JClassType superType : typeInfo.getFlattenedSupertypeHierarchy() ) {
            if ( Object.class.getCanonicalName().equals( superType.getQualifiedSourceName() ) ) {
                continue;
            }

            fieldsToScan.addAll( Arrays.asList( superType.getFields() ) );
        }

        return fieldsToScan;
    }

    private String generateEnumConverter( JEnumType enumType ) {
        String generatedTypeName = EnumConverterGenerator.getQualifiedSourceNameForGeneratedClass( enumType );
        JClassType type = context.getTypeOracle().findType( generatedTypeName );
        if ( type == null ) {
            try {
                return EnumConverterGenerator.class.newInstance().generate( log, context, enumType.getQualifiedSourceName() );
            }
            catch ( Exception e ) {
                log.log( TreeLogger.Type.ERROR, e.toString() );
            }
        }
        return type.getQualifiedSourceName();
    }

    private void generateFields( SourceWriter sw ) throws Exception {
        sw.println( "private final java.util.logging.Logger log = java.util.logging.Logger.getLogger( \"" + typeInfo.getQualifiedSourceName() + "\" );\n" );
    }

    TreeLogger log;
    GeneratorContext context;
    String typeName;

    JClassType typeInfo;
    JClassType markerTypeInfo;

    String packageName;
    String simpleName;

    Url urlAnnotation;
}
