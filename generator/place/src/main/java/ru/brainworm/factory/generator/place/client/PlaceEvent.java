package ru.brainworm.factory.generator.place.client;

/**
 * Маркерный интерфейс для событий с place-ами
 */
public interface PlaceEvent {

    /**
     * Возвращает код операции
     */
    public Long getOperationId();

    /**
     * Устанавливает код операции
     */
    public void setOperation( Long opId );

    /**
     * Возвращает operationId для дочерней карточки
     */
    public Long getChildOperationId();

    /**
     * Устанавливает operationId для дочерней карточки
     */
    public void setChildOperationId( Long childOpId );

    /**
     * Возвращает информацию о стеке
     */
    public StackInfo getStackInfo();

    /**
     * Задает информацию о стеке
     */
    public void setStackInfo( StackInfo info );

    /**
     * Задает режим навигации
     */
    public void setNavigationMode( NavigationMode mode );

    /**
     * Возвращает режим навигации
     */
    NavigationMode getNavigationMode();
}
