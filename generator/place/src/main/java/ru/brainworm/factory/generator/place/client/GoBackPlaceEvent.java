package ru.brainworm.factory.generator.place.client;

/**
 * Событие позволяющее отмотать стек "назад"
 */
public class GoBackPlaceEvent extends DefaultPlaceEvent {
    public GoBackPlaceEvent() {
    }

    public GoBackPlaceEvent( Long operationId, PlaceEvent ifEmptyEvent ) {
        this.opId = operationId;
        this.ifEmptyEvent = ifEmptyEvent;
    }

    @Omit
    PlaceEvent ifEmptyEvent;
}
