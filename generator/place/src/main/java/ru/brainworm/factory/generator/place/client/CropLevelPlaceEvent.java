package ru.brainworm.factory.generator.place.client;

/**
 * Событие позволяющее отмотать стек "назад"
 */
public class CropLevelPlaceEvent extends DefaultPlaceEvent {
    public CropLevelPlaceEvent() {
    }

    public CropLevelPlaceEvent( Integer level ) {
        this.level = level;
    }

    @Omit
    Integer level;
}
