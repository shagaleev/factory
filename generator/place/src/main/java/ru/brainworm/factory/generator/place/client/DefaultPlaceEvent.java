package ru.brainworm.factory.generator.place.client;

/**
 * Дефолтное событие
 */
public class DefaultPlaceEvent implements PlaceEvent {

    public DefaultPlaceEvent() {
    }

    @Override
    public Long getOperationId() {
        return opId;
    }

    @Override
    public void setOperation( Long opId ) {
        this.opId = opId;
    }

    @Override
    public Long getChildOperationId() {
        return childOpId;
    }

    @Override
    public void setChildOperationId( Long childOpId ) {
        this.childOpId = childOpId;
    }

    @Override
    public StackInfo getStackInfo() {
        return stackInfo;
    }

    @Override
    public void setStackInfo( StackInfo info ) {
        stackInfo = info;
    }

    @Override
    public void setNavigationMode( NavigationMode mode ) {
        navigationMode = mode;
    }

    @Override
    public NavigationMode getNavigationMode() {
        return navigationMode;
    }

    @Omit
    public StackInfo stackInfo;
    @Omit
    public Long opId;
    @Omit
    public Long childOpId;
    @Omit
    public NavigationMode navigationMode = NavigationMode.ADD;
}
