package ru.brainworm.factory.generator.place.client;

/**
 * Created by shagaleev on 10/21/14.
 */
public interface PlaceEventCopier<T extends PlaceEvent > {
    public void copy( T from, T to );
}
