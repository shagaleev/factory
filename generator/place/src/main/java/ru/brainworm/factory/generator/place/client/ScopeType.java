package ru.brainworm.factory.generator.place.client;


/**
 * Область применения поля, не требующего сериализации
 */
public enum ScopeType {
    /**
     * не сериализовывать и не копировать
     */
    FULL,

    /**
     * не сериализовывать
     */
    SERIALIZATION
}
