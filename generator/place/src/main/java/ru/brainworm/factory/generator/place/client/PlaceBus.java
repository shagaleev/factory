package ru.brainworm.factory.generator.place.client;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import ru.brainworm.factory.core.bus.client.Bus;

import java.lang.Class;
import java.lang.Integer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by shagaleev on 10/21/14.
 */
public class PlaceBus extends Bus {

    public PlaceBus(
            Map<Class, PlaceEventSerializer> eventSerializerMap,
            List< PlaceEventDeserializer > deserializers,
            Map<Class, PlaceEventCopier > eventCopierMap
    ) {
        this.eventSerializerMap = eventSerializerMap;
        this.eventDeserializerList = deserializers;
        this.eventCopierMap = eventCopierMap;

        History.addValueChangeHandler( new ValueChangeHandler<String>() {
            @Override
            public void onValueChange( ValueChangeEvent<String> event ) {
                onRouteChanged( event.getValue() );
            }
        } );
    }

    public <T> void fireEvent( String source, T value ) {
        log.info( source + " fires " + value.toString() );
        if ( !( value instanceof PlaceEvent ) ) {
            super.fireEvent( value );
            return;
        }

        PlaceEvent placeEvent = (PlaceEvent)value;
        if ( value instanceof GoBackPlaceEvent ) {
            rollbackStack( source, (GoBackPlaceEvent)value );
            return;
        }
        else if ( value instanceof CropLevelPlaceEvent ) {
            cropStack( source, (CropLevelPlaceEvent)value );
            return;
        }

        if (placeEvent.getOperationId() == null ) {
            stackEvents.clear();
        }

        Integer stackSize = stackEvents.size();
        if ( stackSize > 0 ) {
            PlaceEvent lastEvent = ( stackEvents.get( stackSize - 1 ) );
            if ( lastEvent.getOperationId() != null && lastEvent.getOperationId().equals( placeEvent.getOperationId() ) ) {
                stackEvents.remove( lastEvent );
            }
        }

        stackEvents.add( placeEvent );

        String token = buildTokenFromStackEvents();

        if ( placeEvent.getNavigationMode() == NavigationMode.REPLACE ) {
            History.replaceItem( token );
            return;
        }

        if ( History.getToken().equals( token ) ) {
            History.fireCurrentHistoryState();
        } else {
            History.newItem(token);
        }
    }

    public PlaceEvent getLastPlaceEvent() {
        if ( stackEvents.isEmpty() ) {
            return new DefaultPlaceEvent();
        }

        return stackEvents.get( stackEvents.size()-1 );
    }

    private void rollbackStack( String source, GoBackPlaceEvent goBackEvent ) {
        Long operationId = goBackEvent.getOperationId();
        if ( operationId == null ) {
            if ( goBackEvent.ifEmptyEvent != null ) {
                fireEvent( source, goBackEvent.ifEmptyEvent );
            }
            return;
        }

        String[] pathSegments = History.getToken().split( "/" );
        StringBuilder newPath = new StringBuilder();
        String separator = "";
        int index=0;
        List<PlaceEvent> newStackEvents = new ArrayList<PlaceEvent>();

        for ( PlaceEvent event : stackEvents ) {
            if ( operationId.equals( event.getOperationId() ) ) {
                stackEvents = newStackEvents;
                History.newItem( newPath.toString() );
                return;
            }

            newPath.append( separator ).append( pathSegments[index] );
            newStackEvents.add( event );

            if ( separator.isEmpty() ) {
                separator = "/";
            }
            index++;
        }

        log.info( "Unknown operation id in GoBackEvent" );
        if ( goBackEvent.ifEmptyEvent != null ) {
            fireEvent( source, goBackEvent.ifEmptyEvent );
        }
    }

    private String buildTokenFromStackEvents() {
        String separator = "";
        StringBuilder newPath = new StringBuilder();

        for ( PlaceEvent event : stackEvents ) {
            PlaceEventSerializer serializer = eventSerializerMap.get( event.getClass() );
            newPath.append( separator ).append( serializer.serialize( event ) );

            if ( separator.isEmpty() ) {
                separator = "/";
            }
        }

        return newPath.toString();
    }

    private void cropStack( String source, CropLevelPlaceEvent event ) {
        Integer level = event.level;
        if ( level < 0 ) {
            return;
        }

        String route = History.getToken();
        String[] segments = route.split( "/" );
        if ( level > segments.length ) {
            level = segments.length;
        }

        StringBuilder newRoute = new StringBuilder();
        String separator = "";
        for ( int i = 0; i < level; i++ ) {
            newRoute.append( separator ).append( segments[i] );
            if ( separator.isEmpty() ) {
                separator = "/";
            }
        }
        History.newItem( newRoute.toString() );
    }

    private void onRouteChanged( String newRoute ) {
        String[] pathSegments = newRoute.split( "/" );
        while( stackEvents.size() > pathSegments.length ) {
            stackEvents.remove( stackEvents.size()-1 );
        }

        int index = 0;
        Map<Class<? extends PlaceEvent>, Integer> typeToIndex = new HashMap<Class<? extends PlaceEvent>, Integer>();

        // Цикл со счетчиком вместо foreach, т.к. нужен индекс участка пути:
        // сравнение по ссылке строк в GWT работает некорректно
        for ( int pathIndex = 0; pathIndex < pathSegments.length; ++pathIndex ) {
            PlaceEvent event = getPlaceEventFromStackOrCreateNew( index, pathSegments[pathIndex] );
            if ( event == null ) {
                super.fireEvent( new UnknownPlaceEvent() );
                return;
            }
            Integer typeIndex = typeToIndex.get( event.getClass() );
            if ( typeIndex == null ) {
                typeIndex = 0;
            }
            typeToIndex.put( event.getClass(), typeIndex + 1 );

            if ( nextOperationId != null ) {
                event.setOperation( nextOperationId );
                nextOperationId = null;
            }

            StringBuilder urlBuilder = new StringBuilder();

            String delimiter = "";
            for (int i = 0; i <= pathIndex; ++i) {
                urlBuilder.append( delimiter ).append( pathSegments[i] );
                delimiter = "/";
            }

            StackInfo si = new StackInfo();
            si.index = index;
            si.stackSize = pathSegments.length;
            si.typeIndex = typeIndex;
            si.typeSize = 0;
            si.currentUrl = urlBuilder.toString();

            event.setStackInfo( si );
            event.setChildOperationId( null );
            super.fireEvent( event );

            nextOperationId = event.getChildOperationId();

            index++;
        }
    }

    private PlaceEvent getPlaceEventFromStackOrCreateNew( int index, String segment ) {
        PlaceEvent deserializedEvent = deserializeSegment( segment );
        if ( deserializedEvent == null ) {
            return null;
        }

        if ( index >= stackEvents.size() ) {
            stackEvents.add( deserializedEvent );
            return deserializedEvent;
        }

        PlaceEvent stackEvent = stackEvents.get( index );
        if ( stackEvent.getClass().equals( deserializedEvent.getClass() ) ) {
            copyDeserializedEventParametersToStackInfoOne( deserializedEvent, stackEvent );
            return stackEvent;
        }

        while ( stackEvents.size() > index ) {
            stackEvents.remove( index );
        }
        stackEvents.add( deserializedEvent );
        return deserializedEvent;
    }

    private PlaceEvent deserializeSegment( String segment ) {
        for ( PlaceEventDeserializer deserializer : eventDeserializerList ) {
            PlaceEvent event = deserializer.deserialize( segment );
            if ( event != null ) {
                return event;
            }
        }
        return null;
    }

    private void copyDeserializedEventParametersToStackInfoOne( PlaceEvent fromEvent, PlaceEvent toEvent ) {
        PlaceEventCopier copier = eventCopierMap.get( fromEvent.getClass() );
        if ( copier == null ) {
            return;
        }

        copier.copy( fromEvent, toEvent );
    }

    Map< Class, PlaceEventSerializer > eventSerializerMap;
    List< PlaceEventDeserializer > eventDeserializerList;
    Map< Class, PlaceEventCopier > eventCopierMap;

    List<PlaceEvent> stackEvents = new ArrayList<PlaceEvent>();

    Long nextOperationId = null;

    Logger log = Logger.getLogger( PlaceBus.class.getName() );
}
