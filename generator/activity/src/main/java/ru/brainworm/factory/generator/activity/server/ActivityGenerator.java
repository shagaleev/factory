package ru.brainworm.factory.generator.activity.server;

import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.core.ext.typeinfo.JField;
import com.google.gwt.core.ext.typeinfo.JMethod;
import com.google.gwt.core.ext.typeinfo.JParameter;
import com.google.gwt.user.rebind.ClassSourceFileComposerFactory;
import com.google.gwt.user.rebind.SourceWriter;
import com.google.inject.Inject;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;
import ru.brainworm.factory.generator.activity.client.annotations.IfTrue;
import ru.brainworm.factory.generator.editor.client.annotations.DataReader;
import ru.brainworm.factory.generator.editor.client.annotations.DataWriter;
import ru.brainworm.factory.generator.editor.server.AsyncWriterData;
import ru.brainworm.factory.generator.editor.server.DataReaderGenerator;
import ru.brainworm.factory.generator.editor.server.DataWriterGenerator;
import ru.brainworm.factory.generator.place.server.PlaceBusContainerGenerator;
import ru.brainworm.factory.generator.router.client.annotations.DefaultPlace;
import ru.brainworm.factory.generator.router.client.annotations.Route;
import ru.brainworm.factory.generator.router.client.annotations.Url;
import ru.brainworm.factory.generator.router.server.ParamsGenerator;
import ru.brainworm.factory.generator.router.server.PlaceGenerator;
import ru.brainworm.factory.generator.router.server.RouterContainerGenerator;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Класс, который будет генерировать активити с таблицами
 */
public class ActivityGenerator extends Generator {
    @Override
    public String generate( TreeLogger logger, GeneratorContext context, String typeName ) throws UnableToCompleteException {
        try {
            this.log = logger;
            this.context = context;
            this.typeName = typeName;

            typeInfo = context.getTypeOracle().getType( typeName );
            markerTypeInfo = context.getTypeOracle().findType( Activity.class.getName() );
            if ( !markerTypeInfo.isAssignableFrom( typeInfo ) ) {
                logger.log( TreeLogger.Type.ERROR, "Class " + typeName + " is not assignable from " + Activity.class.getName() );
                return null;
            }

            packageName = typeInfo.getPackage().getName();
            simpleName = typeInfo.getSimpleSourceName() + "_Impl";

            ClassSourceFileComposerFactory composer = new ClassSourceFileComposerFactory( packageName, simpleName );
            composer.addImport( typeInfo.getQualifiedSourceName() );
            composer.addImport( "com.google.inject.Inject" );
            composer.setSuperclass( typeInfo.getName() );

            parse( typeInfo );

            PrintWriter printWriter = context.tryCreate( logger, packageName, simpleName );
            if ( printWriter != null ) {
                SourceWriter sw = composer.createSourceWriter( context, printWriter );

                generateFireEvent( sw );
                generateGetLastPlaceEvent( sw );
                generateReaders( sw );
                generateWriters( sw );
                generateMembers( sw );
                generateConstructor( sw );
                generateFields( sw );

                sw.commit( logger );
            }

            return typeName + "_Impl";
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
        return null;
    }

    private void parse( JClassType typeInfo ) {
        eventHandlers.clear();
        routeHandlers.clear();
        placeHistory.clear();
        dataReaders.clear();
        dataWriters.clear();
        for ( JMethod method : typeInfo.getInheritableMethods() ) {
            Event annotation = method.getAnnotation( Event.class );
            if ( annotation != null ) {
                eventHandlers.add( method );
            }

            Route route = method.getAnnotation( Route.class );
            if ( route != null ) {
                routeHandlers.add( method );
            }

            DataReader dataReader = method.getAnnotation( DataReader.class );
            if ( dataReader != null ) {
                dataReaders.add( method );
            }

            DataWriter dataWriter = method.getAnnotation( DataWriter.class );
            if ( dataWriter != null ) {
                dataWriters.add( method );
            }
        }

        for ( JField field: typeInfo.getFields() ) {
            DefaultPlace defaultPlace = field.getAnnotation( DefaultPlace.class );
            if ( defaultPlace != null ) {
                placeHistory.add( field );
                continue;
            }

            Inject injectAnnotation = field.getAnnotation( Inject.class );
            if ( injectAnnotation == null ) {
                continue;
            }

            JClassType fieldType = field.getType().isClassOrInterface();
            Url urlAnnotation = fieldType.getAnnotation( Url.class );
            if ( urlAnnotation != null ) {
                urlFields.add( field );
            }

        }
    }

    private void generateMembers( SourceWriter sw ) throws Exception {
        Set<String> generatedNames = new HashSet< String >();

        for ( AsyncWriterData data : writerGenerator.writerDatas ) {
            String varName = "map_"+data.changedEventType.getCanonicalName().replace( ".", "_" );
            if ( generatedNames.contains( varName ) ) {
                continue;
            }
            generatedNames.add( varName );

            sw.print( "java.util.Map< " + data.viewMethod.getReturnType().getQualifiedSourceName() + ", "+data.dtoFieldType.getQualifiedSourceName() + ">" );
            sw.print( " "+varName );
            sw.println( " = new java.util.HashMap<" + data.viewMethod.getReturnType().getQualifiedSourceName() + ", " + data.dtoFieldType.getQualifiedSourceName() + ">();" );
        }
    }

    private void generateConstructor( SourceWriter sw ) throws Exception {
        sw.println( "  public "+simpleName+"() {" );

        for ( JMethod handler : eventHandlers ) {
            if ( handler.getParameters().length != 1 ) {
                return;
            }

            IfTrue eventCondition = handler.getAnnotation( IfTrue.class );
            JClassType handleEvent = handler.getParameters()[ 0 ].getType().isClassOrInterface();
            if ( handleEvent == null ) {
                log.log( TreeLogger.Type.WARN, "handle event not is class or interface");
                continue;
            }

            JClassType[] eventSubtypes = handleEvent.getSubtypes();
            JClassType[] handleEventTypes = eventSubtypes.length != 0 ? eventSubtypes : new JClassType[]{ handleEvent };

            for ( JClassType type : handleEventTypes ) {

                sw.println( "    ru.brainworm.factory.generator.place.client.PlaceBus_Container.getBus().subscribe( " + type.getQualifiedSourceName() + ".class, new ru.brainworm.factory.core.bus.client.Bus.Subscriber< " + type.getQualifiedSourceName() + ">() {" );
                sw.println( "      public void update( " + type.getQualifiedSourceName() + " event ) {" );

                if ( eventCondition != null ) {
                    String cond = eventCondition.value().trim().replace( "${event}", handler.getParameters()[ 0 ].getName() );
                    sw.println( "         if ( !(" + cond + ") ) {" );
                    sw.println( "            return;" );
                    sw.println( "         }" );
                }

                sw.println( "        log.info( \"" + typeName + "#" + handler.getName() + "(\"+ event.toString() + \")\");" );
                sw.println( "        try {" );
                sw.println( "          " + handler.getName() +"( event );" );
                sw.println( "        } catch ( Throwable th ) {" );
                sw.println( "           log.warning( \"Exception: \"+ th.getMessage() );"  );
                sw.println( "        }" );
                sw.println( "      }" );
                sw.println( "    } );" );
            }
        }

        for ( JMethod handler : routeHandlers ) {
            Route annotation = handler.getAnnotation( Route.class );

            JClassType routeClass = context.getTypeOracle().findType( annotation.value().getCanonicalName() );
            Url urlAnnotation = routeClass.getAnnotation( Url.class );
            if ( urlAnnotation == null ) {
                log.log( TreeLogger.Type.ERROR, "No Url annotation for " + annotation.value().getCanonicalName() + " defined" );
            }

            JClassType routerContainerTypeName = context.getTypeOracle().findType( "ru.brainworm.factory.core.router.client.Router_Container" );
            if ( routerContainerTypeName == null ) {
                try {
                    RouterContainerGenerator.class.newInstance().generate( log, context, "ru.brainworm.factory.core.router.client.Router" );
                }
                catch ( Exception e ) {
                    log.log( TreeLogger.Type.ERROR, e.toString() );
                    throw e;
                }
            }

            sw.println( "    ru.brainworm.factory.core.router.client.Router_Container.getRouter().subscribe( \""+urlAnnotation.value()+"\", new ru.brainworm.factory.core.router.client.Router.Subscriber() {" );
            sw.println( "      public void onHistoryChanged( java.util.Map<String,String> params ) {" );
            sw.println( "        " + handler.getName()+"( " );

            boolean needSeparator = false;
            JParameter[] handlerParameters = handler.getParameters();
            for ( JParameter handlerParameter : handlerParameters ) {
                String parameterTypeName = handlerParameter.getType().getQualifiedSourceName();
                JClassType parameterType = context.getTypeOracle().findType( parameterTypeName );

                String parameterTypePackageName = parameterType.getPackage().getName();
                String parameterTypeImplName = parameterTypePackageName + "." + parameterTypeName.substring( parameterTypePackageName.length()+1 ).replace( ".", "__" ) + "_Impl";

                JClassType parameterImplType = context.getTypeOracle().findType( parameterTypeImplName );
                if ( parameterImplType == null ) {
                    try {
                        ParamsGenerator.class.newInstance().generate( log, context, parameterTypeName );
                    }
                    catch ( Exception e ) {
                        log.log( TreeLogger.Type.ERROR, e.toString() );
                        throw e;
                    }
                }

                if ( needSeparator ) {
                    sw.print( "," );
                }
                sw.println( "        new " + parameterTypeImplName + "( params ) " );
                needSeparator = true;
            }

            sw.println( "        );" );
            sw.println( "      }" );
            sw.println( "    } );" );
        }

        for ( JField field : placeHistory ) {
            sw.println( "    "+field.getName()+" = new "+field.getType().getQualifiedSourceName()+"() {" );
            sw.println( "      public void back() {" );
            sw.println( "        if ( historySize > 0 ) {" );
            sw.println( "          com.google.gwt.user.client.History.back();" );
            sw.println( "          return;" );
            sw.println( "        }" );

            DefaultPlace annotation = field.getAnnotation( DefaultPlace.class );
            JClassType placeClass = context.getTypeOracle().findType( annotation.value().getCanonicalName() );
            String generatedClassName = PlaceGenerator.getQualifiedSourceNameForGeneratedClass( placeClass );
            JClassType generatedClassType = context.getTypeOracle().findType( generatedClassName );
            if ( generatedClassType == null ) {
                try {
                    PlaceGenerator.class.newInstance().generate( log, context, annotation.value().getCanonicalName() );
                }
                catch ( Exception e ) {
                    log.log( TreeLogger.Type.ERROR, e.toString() );
                    throw e;
                }
            }

            sw.println( "        new "+generatedClassName + "().go();" );
            sw.println( "      }" );
            sw.println( "    };" );
        }

        for ( AsyncWriterData data : writerGenerator.writerDatas ) {
            sw.println( "    ru.brainworm.factory.generator.place.client.PlaceBus_Container.getBus().subscribe( " + data.changedEventType.getCanonicalName() + ".class, new ru.brainworm.factory.core.bus.client.Bus.Subscriber< " + data.changedEventType.getCanonicalName() + ">() {" );
            sw.println( "      public void update( " + data.changedEventType.getCanonicalName() + " event ) {" );
            sw.println( "        log.info( \"" + typeName + "#generatedOnChange(\"+ event.toString() + \")\");" );
            sw.println( "        try {" );
            sw.println( "          if ( !map_"+data.changedEventType.getCanonicalName().replace( ".", "_" )+ ".containsKey( event.parent ) ) { return; }" );
            sw.println( "          map_"+data.changedEventType.getCanonicalName().replace( ".", "_" )+".put( event.parent, event.value );" );
            sw.println( "        } catch ( Throwable th ) {" );
            sw.println( "           log.warning( \"Exception: \"+ th.getMessage() );"  );
            sw.println( "        }" );
            sw.println( "      }" );
            sw.println( "    } );" );
        }

        sw.println( "  }" );
        sw.println( "" );
    }

    private void generateFireEvent( SourceWriter sw ) throws Exception {
        sw.println( "  public <T> void fireEvent( T event ) {" );

        JClassType placeBusType = context.getTypeOracle().findType( "ru.brainworm.factory.generator.place.client.PlaceBus_Container" );
        if ( placeBusType == null ) {
            try {
                PlaceBusContainerGenerator.class.newInstance().generate( log, context, "ru.brainworm.factory.generator.place.client.PlaceBus" );
            }
            catch ( Exception e ) {
                log.log( TreeLogger.Type.ERROR, e.toString() );
                throw e;
            }
        }
        sw.println( "    ru.brainworm.factory.generator.place.client.PlaceBus_Container.getBus().fireEvent( \""+typeInfo.getName()+"\", event );" );
        sw.println( "  }" );
        sw.println( "" );
    }

    private void generateGetLastPlaceEvent( SourceWriter sw ) throws Exception {
        sw.println( "  public ru.brainworm.factory.generator.place.client.PlaceEvent getLastPlaceEvent() {" );
        sw.println( "    return ru.brainworm.factory.generator.place.client.PlaceBus_Container.getBus().getLastPlaceEvent();" );
        sw.println( "  }" );
        sw.println( "" );
    }

    private void generateReaders( SourceWriter sw ) throws Exception {
        for ( JMethod dataReader : dataReaders ) {
            JParameter[] parameters = dataReader.getParameters();
            if ( parameters.length != 2 ) {
                error( "Method " + typeName + "." + dataReader.getName() + " must have exactly 2 parameters" );
            }

            JParameter dtoParameter = parameters[ 0 ];
            JClassType dtoType = dtoParameter.getType().isClassOrInterface();
            if ( dtoType == null ) {
                error( "Parameter \"" + dtoParameter.getName() + "\" of method " + typeName+"."+dataReader.getName()+" must be class or interface" );
            }

            JParameter viewParameter = parameters[ 1 ];
            JClassType viewType = viewParameter.getType().isClassOrInterface();
            if ( viewType == null ) {
                error( "Parameter \"" + viewParameter.getName() + "\" of method " + typeName+"."+dataReader.getName()+" must be class or interface" );
            }

            String accessor = "protected";
            if ( dataReader.isPublic() ) {
                accessor = "public";
            }
            else if ( dataReader.isPrivate() ) {
                accessor = "private";
            }
            sw.print( "  "+accessor+" void " + dataReader.getName() + "( " );
            sw.print( dtoType.getQualifiedSourceName() + " " + dtoParameter.getName() + ", " );
            sw.println( viewType.getQualifiedSourceName() + " " + viewParameter.getName() + " ) {" );

            readerGenerator.setDto( dtoParameter, dtoType );
            readerGenerator.setView( viewParameter, viewType );
            readerGenerator.generate( sw );

            sw.println( "}" );
        }
    }

    private void generateWriters( SourceWriter sw ) throws Exception {
        for ( JMethod dataWriter : dataWriters ) {
            JParameter[] parameters = dataWriter.getParameters();
            if ( parameters.length != 2 ) {
                error( "Method " + typeName + "." + dataWriter.getName() + " must have exactly 2 parameters" );
            }

            JParameter viewParameter = parameters[ 0 ];
            JClassType viewType = viewParameter.getType().isClassOrInterface();
            if ( viewType == null ) {
                error( "Parameter \"" + viewParameter.getName() + "\" of method " + typeName+"."+dataWriter.getName()+" must be class or interface" );
            }

            JParameter dtoParameter = parameters[ 1 ];
            JClassType dtoType = dtoParameter.getType().isClassOrInterface();
            if ( dtoType == null ) {
                error( "Parameter \"" + dtoParameter.getName() + "\" of method " + typeName+"."+dataWriter.getName()+" must be class or interface" );
            }

            String accessor = "protected";
            if ( dataWriter.isPublic() ) {
                accessor = "public";
            }
            else if ( dataWriter.isPrivate() ) {
                accessor = "private";
            }
            sw.print( "  "+accessor+" void " + dataWriter.getName() + "( " );
            sw.print( viewType.getQualifiedSourceName() + " " + viewParameter.getName() + ", " );
            sw.println( dtoType.getQualifiedSourceName() + " " + dtoParameter.getName() + " ) {" );


            writerGenerator.setView( viewParameter, viewType );
            writerGenerator.setDto( dtoParameter, dtoType );
            writerGenerator.generate( sw );

            sw.println( "}" );
        }
    }

    private void generateFields( SourceWriter sw ) throws Exception {
        sw.println( "private final java.util.logging.Logger log = java.util.logging.Logger.getLogger( \"" + typeInfo.getQualifiedSourceName() + "\" );\n" );
    }

    private void error( String error ) throws Exception {
        log.log( TreeLogger.Type.ERROR, error );
        throw new UnsupportedOperationException( error );
    }

    TreeLogger log;
    GeneratorContext context;
    String typeName;

    JClassType typeInfo;
    JClassType markerTypeInfo;

    String packageName;
    String simpleName;

    ArrayList<JMethod> eventHandlers = new ArrayList<JMethod>();
    ArrayList<JMethod> routeHandlers = new ArrayList<JMethod>();
    ArrayList<JField> urlFields = new ArrayList<JField>();
    ArrayList<JField> placeHistory = new ArrayList<JField>();

    DataReaderGenerator readerGenerator = new DataReaderGenerator();
    ArrayList<JMethod> dataReaders = new ArrayList<JMethod>();

    DataWriterGenerator writerGenerator = new DataWriterGenerator();
    ArrayList<JMethod> dataWriters = new ArrayList<JMethod>();
}
