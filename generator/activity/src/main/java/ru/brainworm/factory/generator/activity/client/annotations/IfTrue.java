package ru.brainworm.factory.generator.activity.client.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Условия срабатывания события
 */
@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
public @interface IfTrue {
    /**
     * Условие срабатывания события.
     * Строка, может включать макроподстановки ${event}
     */
    String value() default "";
}
