package ru.brainworm.factory.generator.activity.client.activity;

import ru.brainworm.factory.generator.place.client.PlaceEvent;

/**
 * Интерфейс для активити
 */
public interface Activity {

    /**
     * Вызываем когда надо послать событие в другую активити
     * @param value    значение события
     * @param <T>      тип события
     */
    <T> void fireEvent( T value );

    /**
     * Возвращает последнее place событие
     */
    PlaceEvent getLastPlaceEvent();
}
