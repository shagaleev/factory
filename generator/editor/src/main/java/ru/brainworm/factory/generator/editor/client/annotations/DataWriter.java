package ru.brainworm.factory.generator.editor.client.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Аннотация для генерируемого метода активити, которые выполняет запись данных в DTO из вьюхи
 */
@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
public @interface DataWriter {
}
