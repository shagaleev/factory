package ru.brainworm.factory.generator.editor.client.annotations;

import ru.brainworm.factory.generator.editor.client.util.*;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Аннотация для метода вьюхи связанного с полем DTO
 */
@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
public @interface DataField {

    /**
     * Название поля в DTO
     */
    String name() default "";

    /**
     * Событие которое надо послать с тем, чтобы отобразить дочернюю форму
     */
    Class<?> showEvent() default Unused.class;

    /**
     * Событие которое надо изловить с тем, чтобы увидеть изменения в дочерней форме
     */
    Class<?> changedEvent() default Unused.class;

    /**
     * Конвертер для преобразования из DTO в объект, передаваемый в событие
     */
    Class<? extends AbstractConverter> fromConverter() default EmptyConverter.class;

    /**
     * Конвертер для преобразования из объекта в событии, в поля DTO
     */
    Class<? extends AbstractMerger> toMerger() default EmptyMerger.class;
}
