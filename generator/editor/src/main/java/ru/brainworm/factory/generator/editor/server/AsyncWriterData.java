package ru.brainworm.factory.generator.editor.server;

import com.google.gwt.core.ext.typeinfo.JMethod;
import com.google.gwt.core.ext.typeinfo.JType;

/**
 * Структура данных для временного хранения распарсенных значений для заполнения в бине полей
 * из вложенных компонент
 */
public class AsyncWriterData {

    public AsyncWriterData(
            String dtoPart, JType dtoFieldType, Class<?> changedEventType, JMethod viewMethod, String mergerPart
    ) {
        this.dtoPart = dtoPart;
        this.dtoFieldType = dtoFieldType;
        this.changedEventType = changedEventType;
        this.viewMethod = viewMethod;
        this.mergerPart = mergerPart;
    }

    @Override
    public String toString() {
        return "AsyncWriterData{" +
                "dtoPart='" + dtoPart + '\'' +
                ", dtoFieldType=" + dtoFieldType +
                ", changedEventType=" + changedEventType +
                ", viewMethod=" + viewMethod +
                ", mergerPart='" + mergerPart + '\'' +
                '}';
    }

    /**
     * Часть (имя поля или название метода() ) которую нужно написать чтобы записать значение в DTO
     */
    public String dtoPart;

    /**
     * Тип значения записываемого в DTO
     */
    public JType dtoFieldType;

    /**
     * Тип события changed
     */
    public Class<?> changedEventType;

    /**
     * Метод во вьюхе, который возвращает контейнер для вложенной формы
     */
    public JMethod viewMethod;

    /**
     * Кусок кода, выполняющий преобразование из переданного в него параметра в DTO
     */
    public String mergerPart;
}
