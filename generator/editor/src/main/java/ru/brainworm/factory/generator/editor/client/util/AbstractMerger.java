package ru.brainworm.factory.generator.editor.client.util;

/**
 * Created by shagaleev on 3/17/15.
 */
public interface AbstractMerger<OBJ, DTO> {
    public void merge( OBJ from, DTO to );
}
