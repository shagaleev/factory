package ru.brainworm.factory.generator.editor.server;

import com.google.gwt.core.ext.typeinfo.JMethod;

/**
 * Структура данных для временного хранения распарсенных значений для посылки события, для отображения
 * вложенных компонент
 */
public class AsyncReaderData {

    public AsyncReaderData(
            Class<?> eventClass, JMethod viewMethod, String dtoPart, Class<?> changedEventClass, String converterPart
    ) {
        this.eventClass = eventClass;
        this.viewMethod = viewMethod;
        this.dtoPart = dtoPart;
        this.changedEventClass = changedEventClass;
        this.converterPart = converterPart;
    }

    /**
     * Класс события, которое нужно послать
     */
    public Class<?> eventClass;

    /**
     * Метод, который нужно вызвать во вьюхе, чтобы получить контейнер
     */
    public JMethod viewMethod;

    /**
     * Часть (имя поля или название метода() ) которую нужно написать чтобы получить значение из DTO
     */
    public String dtoPart;

    /**
     * Класс события changed;
     */
    public Class<?> changedEventClass;

    /**
     * Кусок кода с использованием конвертера, который получает данные из DTO
     */
    public String converterPart;
}
