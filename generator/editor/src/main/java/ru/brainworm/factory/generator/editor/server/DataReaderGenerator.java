package ru.brainworm.factory.generator.editor.server;

import com.google.gwt.core.ext.typeinfo.*;
import com.google.gwt.user.rebind.SourceWriter;
import ru.brainworm.factory.generator.editor.client.annotations.DataField;
import ru.brainworm.factory.generator.editor.client.util.AbstractConverter;
import ru.brainworm.factory.generator.editor.client.util.EmptyConverter;
import ru.brainworm.factory.generator.editor.client.util.Unused;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Генератор текста метода чтения данных из DTO во VIEW
 */
public class DataReaderGenerator {
    public void setDto( JParameter dtoParameter, JClassType dtoType ) {
        this.dtoParameter = dtoParameter;
        this.dtoType = dtoType;
    }

    public void setView( JParameter viewParameter, JClassType viewType ) {
        this.viewParameter = viewParameter;
        this.viewType = viewType;
    }

    public void generate( SourceWriter sw ) {
        parse();

        for ( Map.Entry<String, JMethod> entry : fieldPartToViewMethod.entrySet() ) {
            sw.print( viewParameter.getName() + "." + entry.getValue().getName() + "( " );
            sw.println( dtoParameter.getName() + "." + entry.getKey() + ");" );
        }

        for ( Map.Entry<String, JMethod> entry : converterPartToViewMethod.entrySet() ) {
            sw.println( viewParameter.getName()+"."+entry.getValue().getName()+"("+entry.getKey()+");" );
        }

        for ( AsyncReaderData data : readerDatas ) {
            String valuePart = data.converterPart;
            if ( data.dtoPart != null ) {
                valuePart = dtoParameter.getName() + "." + data.dtoPart;
            }

            if ( !Unused.class.equals( data.changedEventClass ) ) {
                sw.print( "  map_"+data.changedEventClass.getCanonicalName().replace( ".", "_" )+".put(" );
                sw.print( " "+viewParameter.getName()+"."+data.viewMethod.getName()+"()," );

                sw.println( "  " + valuePart + " );" );
            }

            sw.print( "  fireEvent( new " + data.eventClass.getCanonicalName() + "( " );
            sw.print( viewParameter.getName()+"."+data.viewMethod.getName()+"(), " );
            sw.println( valuePart + " ) );" );

        }
    }

    private void parse() {
        // parse all methods of view
        fieldPartToViewMethod.clear();
        readerDatas.clear();
        for ( JMethod method : viewType.getInheritableMethods() ) {
            DataField dataField = method.getAnnotation( DataField.class );
            if ( dataField == null ) {
                continue;
            }

            boolean returnsVoid = JPrimitiveType.VOID.equals( method.getReturnType().isPrimitive() );
            boolean startsWithSet = method.getName().startsWith( "set" );
            boolean hasOneParameter = method.getParameters().length == 1;

            if ( returnsVoid && startsWithSet && hasOneParameter ) {
                String fieldPart = findFieldPart( dataField.name(), method.getParameters()[ 0 ].getType() );
                if ( fieldPart != null ) {
                    fieldPartToViewMethod.put( fieldPart, method );
                }

                String converterPart = makeConverterPart( dataField.fromConverter() );
                if ( converterPart != null ) {
                    converterPartToViewMethod.put( converterPart, method );
                }
                continue;
            }

            Class<?> showEventClass = dataField.showEvent();
            Class<?> changedEventClass = dataField.changedEvent();
            if ( !Unused.class.equals( showEventClass ) ) {
                String fieldPart = findFieldPart( dataField.name(), null );
                String converterPart = makeConverterPart( dataField.fromConverter() );

                if ( fieldPart != null || converterPart != null ) {
                    readerDatas.add( new AsyncReaderData( showEventClass, method, fieldPart, changedEventClass, converterPart ) );
                }
            }
        }
    }

    private String makeConverterPart( Class<? extends AbstractConverter > fromConverter ) {
        if ( EmptyConverter.class.equals( fromConverter ) ) {
            return null;
        }
        return "new "+fromConverter.getCanonicalName()+"().convert( "+dtoParameter.getName()+" )";
    }

    private String findFieldPart( String fieldName, JType fieldType ) {
        if ( fieldName == null ) {
            return null;
        }
        // check all fields
        for ( JField field : dtoType.getFields() ) {
            if ( !field.isPublic() ) {
                continue;
            }

            if ( fieldType == null && fieldName.equals( field.getName() ) ) {
                return fieldName;
            }

            if ( fieldName.equals( field.getName() ) && field.getType().equals( fieldType ) ) {
                return fieldName;
            }
        }

        // check all methods
        for ( JMethod method : dtoType.getInheritableMethods() ) {
            if ( !method.isPublic() ) {
                continue;
            }

            if ( fieldType != null && !method.getReturnType().equals( fieldType ) ) {
                continue;
            }

            if ( method.getParameters().length != 0 ) {
                continue;
            }

            if ( method.getName().equalsIgnoreCase( "get"+fieldName ) ) {
                return method.getName() + "()";
            }

            if ( method.getName().equalsIgnoreCase( "is"+fieldName ) ) {
                return method.getName() + "()";
            }
        }

        return null;
    }

    Map< String, JMethod > fieldPartToViewMethod = new HashMap<String, JMethod>();
    Map< String, JMethod > converterPartToViewMethod = new HashMap<String, JMethod>();
    List<AsyncReaderData> readerDatas = new ArrayList<AsyncReaderData>();

    JParameter dtoParameter;
    JClassType dtoType;
    JParameter viewParameter;
    JClassType viewType;
}
