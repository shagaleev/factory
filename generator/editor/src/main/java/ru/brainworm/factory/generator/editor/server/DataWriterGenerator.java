package ru.brainworm.factory.generator.editor.server;

import com.google.gwt.core.ext.typeinfo.*;
import com.google.gwt.user.rebind.SourceWriter;
import ru.brainworm.factory.generator.editor.client.annotations.DataField;
import ru.brainworm.factory.generator.editor.client.util.AbstractMerger;
import ru.brainworm.factory.generator.editor.client.util.EmptyMerger;
import ru.brainworm.factory.generator.editor.client.util.Unused;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Генератор текста метода чтения данных из VIEW в DTO
 */
public class DataWriterGenerator {
    public void setView( JParameter viewParameter, JClassType viewType ) {
        this.viewParameter = viewParameter;
        this.viewType = viewType;
    }

    public void setDto( JParameter dtoParameter, JClassType dtoType ) {
        this.dtoParameter = dtoParameter;
        this.dtoType = dtoType;
    }

    public void generate( SourceWriter sw ) {
        parse();

        for ( Map.Entry<String, JMethod> entry : fieldPartToViewMethod.entrySet() ) {
            sw.println( dtoParameter.getName() + "." + MessageFormat.format( entry.getKey(), viewParameter.getName()+"."+entry.getValue().getName()+"()" ) + ";" );
        }

        for ( Map.Entry<String, JMethod> entry : mergerPartToViewMethod.entrySet() ) {
            sw.println( MessageFormat.format( entry.getKey(), viewParameter.getName()+"."+entry.getValue().getName()+"()" ) + ";" );
        }

        for ( AsyncWriterData data : writerDatas ) {
            String rightPart = "map_" + data.changedEventType.getCanonicalName().replace( ".", "_" ) + ".get( "+viewParameter.getName()+"."+data.viewMethod.getName()+"() )";

            if ( data.dtoPart != null ) {
                sw.println( dtoParameter.getName() + "." + MessageFormat.format( data.dtoPart, rightPart ) + ";" );
            }
            else if ( data.mergerPart != null ) {
                sw.println( MessageFormat.format( data.mergerPart, rightPart ) + ";" );
            }
        }
    }

    private void parse() {
        // parse all methods of view
        fieldPartToViewMethod.clear();
        writerDatas.clear();
        for ( JMethod method : viewType.getInheritableMethods() ) {
            DataField dataField = method.getAnnotation( DataField.class );
            if ( dataField == null ) {
                continue;
            }

            boolean hasNoneParameters = method.getParameters().length == 0;
            boolean startsWithGetOrIs = method.getName().startsWith( "get" ) || method.getName().startsWith( "is" );
            Class<?> changedEventClass = dataField.changedEvent();

            if ( startsWithGetOrIs && hasNoneParameters && Unused.class.equals( changedEventClass ) ) {
                String fieldPart = findFieldPart( dataField.name(), method.getReturnType() );
                if ( fieldPart != null ) {
                    fieldPartToViewMethod.put( fieldPart, method );
                }

                String mergerPart = makeMergerPart( dataField.toMerger() );
                if ( mergerPart != null ) {
                    mergerPartToViewMethod.put( mergerPart, method );
                }
                continue;
            }

            if ( hasNoneParameters && !Unused.class.equals( changedEventClass ) ) {
                String fieldPart = findFieldPart( dataField.name(), null );
                String mergerPart = makeMergerPart( dataField.toMerger() );

                JType fieldType = findFieldType( dataField.name() );
                JType mergeObjectType = findFieldType( dataField.toMerger() );

                if (
                        ( fieldPart != null && fieldType != null )
                        || ( mergerPart != null && mergeObjectType != null )
                ) {
                    writerDatas.add( new AsyncWriterData(
                            fieldPart, fieldType == null ? mergeObjectType : fieldType,
                            changedEventClass, method, mergerPart
                    ) );
                }
            }
        }
    }

    private String makeMergerPart( Class<? extends AbstractMerger> toMerger ) {
        if ( EmptyMerger.class.equals( toMerger ) ) {
            return null;
        }
        return "new "+toMerger.getCanonicalName()+"().merge( {0}, " + dtoParameter.getName() + " )";
    }

    private String findFieldPart( String fieldName, JType fieldType ) {
        if ( fieldName == null ) {
            return null;
        }

        // check all fields
        for ( JField field : dtoType.getFields() ) {
            if ( !field.isPublic() ) {
                continue;
            }

            if ( fieldName.equals( field.getName() ) && fieldType == null ) {
                return fieldName + " = {0}";
            }

            if ( fieldName.equals( field.getName() ) && field.getType().equals( fieldType ) ) {
                return fieldName + " = {0}";
            }
        }

        // check all methods
        for ( JMethod method : dtoType.getInheritableMethods() ) {
            if ( !method.isPublic() ) {
                continue;
            }

            if ( !JPrimitiveType.VOID.equals( method.getReturnType().isPrimitive() ) ) {
                continue;
            }

            if ( method.getParameters().length != 1 ) {
                continue;
            }

            if ( fieldType != null && !fieldType.equals( method.getParameters()[0].getType() ) ) {
                continue;
            }

            if ( method.getName().equalsIgnoreCase( "set"+fieldName ) ) {
                return method.getName() + "( {0} )";
            }
        }

        return null;
    }

    private JType findFieldType( String fieldName ) {
        // check all fields
        for ( JField field : dtoType.getFields() ) {
            if ( !field.isPublic() ) {
                continue;
            }

            if ( fieldName.equals( field.getName() ) ) {
                return field.getType();
            }
        }

        // check all methods
        for ( JMethod method : dtoType.getInheritableMethods() ) {
            if ( !method.isPublic() ) {
                continue;
            }

            if ( !JPrimitiveType.VOID.equals( method.getReturnType().isPrimitive() ) ) {
                continue;
            }

            if ( method.getParameters().length != 1 ) {
                continue;
            }

            if ( method.getName().equalsIgnoreCase( "set"+fieldName ) ) {
                return method.getParameters()[ 0 ].getType();
            }
        }

        return null;
    }

    private JType findFieldType( Class<? extends AbstractMerger> toMerger ) {
        JClassType mergerType = viewType.getOracle().findType( toMerger.getCanonicalName() );
        for ( JMethod method : mergerType.getInheritableMethods() ) {
            if ( !method.getName().equals( "merge" ) ) {
                continue;
            }

            JParameter[] parameters = method.getParameters();
            if ( parameters.length != 2 ) {
                continue;
            }

            return parameters[0].getType();
        }

        return null;
    }

    Map< String, JMethod > fieldPartToViewMethod = new HashMap<String, JMethod>();
    Map< String, JMethod> mergerPartToViewMethod = new HashMap<String, JMethod>();
    public List< AsyncWriterData > writerDatas = new ArrayList<AsyncWriterData>();

    JParameter dtoParameter;
    JClassType dtoType;
    JParameter viewParameter;
    JClassType viewType;
}
