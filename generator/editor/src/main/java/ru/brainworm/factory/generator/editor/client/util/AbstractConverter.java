package ru.brainworm.factory.generator.editor.client.util;

/**
 * Created by shagaleev on 3/17/15.
 */
public interface AbstractConverter<FROM, TO> {
        public TO convert( FROM obj );
}
