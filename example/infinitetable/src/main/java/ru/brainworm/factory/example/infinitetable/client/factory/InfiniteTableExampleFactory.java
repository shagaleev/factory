package ru.brainworm.factory.example.infinitetable.client.factory;

import com.google.gwt.inject.client.GinModules;
import ru.brainworm.factory.example.infinitetable.client.activity.app.AppActivity;
import ru.brainworm.factory.generator.injector.client.FactoryInjector;
import ru.brainworm.factory.widget.table.client.pager.factory.PagerClientModule;


/**
 * Factory
 */
@GinModules({InfiniteTableExampleModule.class, PagerClientModule.class})
public interface InfiniteTableExampleFactory
        extends FactoryInjector
{
    /**
     * Активити приложения
     * @return
     */
    AppActivity getAppActivity();

}

