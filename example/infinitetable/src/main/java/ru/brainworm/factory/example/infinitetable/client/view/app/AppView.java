package ru.brainworm.factory.example.infinitetable.client.view.app;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.google.inject.Inject;
import ru.brainworm.factory.example.infinitetable.client.activity.app.AbstractAppActivity;
import ru.brainworm.factory.example.infinitetable.client.activity.app.AbstractAppView;
import ru.brainworm.factory.example.infinitetable.client.view.separator.SeparatorWidget;
import ru.brainworm.factory.example.infinitetable.shared.TableRecord;
import ru.brainworm.factory.widget.table.client.InfiniteTableWidget;
import ru.brainworm.factory.widget.table.client.helper.SelectionColumn;
import ru.brainworm.factory.widget.table.client.helper.StaticColumn;

import java.util.List;

/**
 * Вью приложения
 */
public class AppView extends Composite implements AbstractAppView {

    @Inject
    public AppView() {

        initWidget( ourUiBinder.createAndBindUi( this ) );

        initTable();
        addSeparator( new SeparatorWidget() );
    }

    @Override
    public void setActivity( AbstractAppActivity activity ) {
        this.activity = activity;

        table.setLoadHandler( activity );
        table.setPagerListener( activity );
    }

    @Override
    public void addRecords( List<TableRecord> records ) {
        for (TableRecord record : records) {
            table.addRow( record );
        }
    }

    @Override
    public void clearRecords() {
        selectionColumn.clear();
        table.clearRows();

        resetPager( 0, 0 );
    }

    @Override
    public void clearRows() {
        table.clearRows();
    }

    @Override
    public void updateRow( TableRecord row ) {
        table.updateRow( row );
    }

    @Override
    public void setTotalRecords( int totalRecords ) {
        table.setTotalRecords( totalRecords );

        resetPager( totalRecords, DONT_CHANGED );
    }

    @Override
    public void setActivePage( int page, boolean scroll ) {
        resetPager( DONT_CHANGED, page );

        if ( scroll ) {
            table.scrollToPage( page );
        }
    }

    @UiHandler( "currnetPageInput" )
    public void onCurrentPageInputChanged( ValueChangeEvent<Integer> event) {
        activity.onPageClicked( currnetPageInput.getValue() );
    }

    @UiHandler("refresh")
    public void onRefreshClicked( ClickEvent event ) {
        if ( activity != null ) {
            activity.onRefreshClicked();
        }
    }

    @UiHandler( "pixelOffset" )
    public void onPixelOffsetChanged( ValueChangeEvent<Integer> event ) {
        GWT.log( "scroll to " + event.getValue() );
        Window.scrollTo( 0, event.getValue() );
    }

    @UiHandler( "topGapSize" )
    public void onTopGapChangedChanged( ValueChangeEvent<Integer> event ) {
        GWT.log( "set top gap to " + event.getValue() );
//        table.setTopGap( event.getValue() );
    }

    @UiHandler( "bottomGapSize" )
    public void onBottomGapChangedChanged( ValueChangeEvent<Integer> event ) {
        GWT.log( "set bottom gap to " + event.getValue() );
//        table.setBottomGap( event.getValue() );
    }

    private void initTable() {
        table.addColumn( selectionColumn.header, selectionColumn.values );

        table.addColumn( idColumn.header, idColumn.values );
    }

    private void addSeparator( SeparatorWidget separatorWidget ) {
        table.setSeparatorProvider( separatorWidget );
    }

    private void resetPager( int totalRecords, int currentPage ) {
        if ( totalRecords != DONT_CHANGED ) {
            pagelabl.setText( "rows: " + totalRecords );
        }
        if ( currentPage != DONT_CHANGED ) {
            currnetPageInput.setValue( currentPage, false );
        }
    }

    @UiField
    InfiniteTableWidget table;
    @UiField
    Button refresh;

    @UiField
    Label pagelabl;
    @UiField
    IntegerBox currnetPageInput;
    @UiField
    IntegerBox pixelOffset;
    @UiField
    IntegerBox topGapSize;
    @UiField
    IntegerBox bottomGapSize;


    StaticColumn<TableRecord> idColumn = new StaticColumn<TableRecord>( "Id" ) {

        @Override
        public void fillColumnValue( Element cell, TableRecord value ) {
            cell.setInnerHTML( String.valueOf( value.id ) );
        }
    };


    SelectionColumn<TableRecord> selectionColumn = new SelectionColumn<TableRecord>();
    public static final int DONT_CHANGED = -1;

    private AbstractAppActivity activity;

    interface AppFormUiBinder extends UiBinder<Widget, AppView> {
    }

    private static AppFormUiBinder ourUiBinder = GWT.create( AppFormUiBinder.class );
}