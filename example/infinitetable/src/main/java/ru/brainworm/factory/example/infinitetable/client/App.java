package ru.brainworm.factory.example.infinitetable.client;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;
import ru.brainworm.factory.example.infinitetable.client.events.AppEvents;
import ru.brainworm.factory.example.infinitetable.client.factory.InfiniteTableExampleFactory;

public class App implements EntryPoint {
    private final InfiniteTableExampleFactory injector = GWT.create( InfiniteTableExampleFactory.class );

    @Override
    public void onModuleLoad() {
        injector.getAppActivity().fireEvent( new AppEvents.Init( RootPanel.get() ) );

    }
}
