package ru.brainworm.factory.example.infinitetable.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import ru.brainworm.factory.example.infinitetable.shared.TableRecord;

import java.util.List;

@RemoteServiceRelativePath("TableRecordService")
public interface TableRecordService extends RemoteService {

    Integer getRecordsCount();

    Integer refreshRecords();

    List<TableRecord> getRecords( int offset, int limit);
}

