package ru.brainworm.factory.example.infinitetable.client.view.separator;

import com.google.gwt.user.client.Element;
import com.google.inject.Inject;
import ru.brainworm.factory.widget.table.client.InfiniteTableWidget;

/**
 * Разделитель страниц между бесконечной таблицей
 */
public class SeparatorWidget implements InfiniteTableWidget.SeparatorProvider {

    @Inject
    public SeparatorWidget(  ) {

    }

    @Override
    public void fillSeparatorValue( Element separator, int i, InfiniteTableWidget table) {
        separator.addClassName("separator");
        int currentPage = i+1;
        int totalPages = table.getPageCount();
        int totalRecords = table.getTotalRecords();
        int currentPageStartRecordNumber = i*table.getPageSize()+1;
        int currentPageLastRecordNumber = i*table.getPageSize()+table.getPageSize();
        if ( currentPageLastRecordNumber > totalRecords ) {
            currentPageLastRecordNumber = totalRecords;
        }
        separator.setInnerText( "Страница "
                + currentPage + " из " + totalPages
                + "(Записи " + currentPageStartRecordNumber + " - " + currentPageLastRecordNumber
                + " из " + totalRecords + ")"
         );
    }

    public void setTable(InfiniteTableWidget table) {
        this.table = table;
    }

    private InfiniteTableWidget table;

}
