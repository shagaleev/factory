package ru.brainworm.factory.example.infinitetable.shared;

import java.io.Serializable;

/**
 * Один элемент записи в таблице
 */
public class TableRecord implements Serializable {

    public TableRecord() {
    }

    public TableRecord( Integer id, String value ) {
        this.id = id;
        this.value = value;
    }


    public Integer id;
    public String value;
    public boolean hasEmbeeded = true;

    @Override
    public boolean equals( Object o ) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;

        TableRecord that = ( TableRecord ) o;

        if ( hasEmbeeded != that.hasEmbeeded ) return false;
        if ( id != null ? !id.equals( that.id ) : that.id != null ) return false;
        return value != null ? value.equals( that.value ) : that.value == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + ( value != null ? value.hashCode() : 0 );
        result = 31 * result + ( hasEmbeeded ? 1 : 0 );
        return result;
    }

    @Override
    public String toString() {
        return "TableRecord{" +
                "id=" + id +
                ", value='" + value + '\'' +
                '}';
    }
}
