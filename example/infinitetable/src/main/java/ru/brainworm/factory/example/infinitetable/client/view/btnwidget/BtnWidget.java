package ru.brainworm.factory.example.infinitetable.client.view.btnwidget;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 * Вью приложения
 */
public class BtnWidget extends Composite {

    public BtnWidget() {
        initWidget(ourUiBinder.createAndBindUi(this));

        clickMe.addClickHandler( new ClickHandler() {
            @Override
            public void onClick( ClickEvent event ) {
                Window.alert( "Click from clickHandler!" );
            }
        } );
    }

    @UiHandler( "clickMe" )
    public void onClick( ClickEvent event ) {
        Window.alert( "Clicked!" );
    }

    @UiField
    Button clickMe;

    interface AppFormUiBinder extends UiBinder<Widget, BtnWidget > {}
    private static AppFormUiBinder ourUiBinder = GWT.create( AppFormUiBinder.class );

}