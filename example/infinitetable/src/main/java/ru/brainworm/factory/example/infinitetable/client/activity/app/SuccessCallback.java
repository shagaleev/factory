package ru.brainworm.factory.example.infinitetable.client.activity.app;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.logging.Logger;

public abstract class SuccessCallback<T> implements AsyncCallback<T> {
    @Override
    public void onFailure( Throwable throwable ) {
        log( "onFailure(): ", throwable );
        onError( throwable );
    }

    protected  void onError( Throwable throwable ){
        //ignore
    }


    private void log( String str, Throwable t ) {
        int stop = 0;
        GWT.log( "ShortCallback: " + str, t );
        log.severe( t.getMessage() );
    }

    private static final Logger log = Logger.getLogger( SuccessCallback.class.getName() );
}
