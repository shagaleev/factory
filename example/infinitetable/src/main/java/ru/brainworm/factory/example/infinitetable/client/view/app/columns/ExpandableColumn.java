package ru.brainworm.factory.example.infinitetable.client.view.app.columns;

import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import ru.brainworm.factory.widget.table.client.ColumnHeader;
import ru.brainworm.factory.widget.table.client.ColumnValue;
import ru.brainworm.factory.widget.table.client.helper.AbstractColumnHandler;
import ru.brainworm.factory.widget.table.client.helper.RowModeColumnProvider;

/**
 * Стандартная колонка с тектовым заполнением c возможностью выбора по клику
 */
public abstract class ExpandableColumn<T> {

    public interface Handler<T> extends AbstractColumnHandler<T> {
        void onExpand( T value );

        void onCollapse( T value );
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName( String name ) {
        columnName = name;
    }

    public ColumnHeader header = new ColumnHeader() {
        @Override
        public void handleEvent( Event event ) {}

        @Override
        public void fillHeader( Element columnHeader ) {
            fillColumnHeader( columnHeader );
        }
    };


    public ColumnValue<T> values = new ColumnValue<T>() {

        @Override
        public void handleEvent( Event event, T value ) {
            if ( !"click".equalsIgnoreCase( event.getType() ) ) {
                return;
            }

            event.preventDefault();
            provider.changeRowMode( value );
            if ( handler != null ) {
                if ( provider.isExpandedRow( value ) ) {
                    handler.onExpand( value );
                } else {
                    handler.onCollapse( value );
                }
            }
        }

        @Override
        public void fillValue( Element cell, T value ) {
            provider.initRowMode( value );
            fillColumnValue( cell, value );
        }
    };


    protected abstract void fillColumnHeader(Element columnHeader);
    public abstract void fillColumnValue( Element cell, T value );

    public void setHandler( Handler<T> handler ) {
        this.handler = handler;
    }

    public void setRowModeProvider( RowModeColumnProvider< T > provider ) {
        this.provider = provider;
    }


    String columnName;
    Handler<T> handler;
    RowModeColumnProvider< T > provider;
}
