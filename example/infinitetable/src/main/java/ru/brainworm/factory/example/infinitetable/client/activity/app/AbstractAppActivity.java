package ru.brainworm.factory.example.infinitetable.client.activity.app;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.brainworm.factory.example.infinitetable.shared.TableRecord;
import ru.brainworm.factory.widget.table.client.InfiniteLoadHandler;
import ru.brainworm.factory.widget.table.client.InfiniteTableWidget;

import java.util.List;

/**
 * Абстрактный активити приложения
 */
public interface AbstractAppActivity extends InfiniteLoadHandler<TableRecord>, InfiniteTableWidget.PagerListener {


    void onRefreshClicked();


    @Override
    void loadData( int i, int i1, AsyncCallback<List<TableRecord>> asyncCallback );

    @Override
    void onPageChanged( int page );

    void onPageClicked( Integer page );
}
