package ru.brainworm.factory.example.infinitetable.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.brainworm.factory.example.infinitetable.shared.TableRecord;

import java.util.List;

public interface TableRecordServiceAsync {
    void getRecordsCount( AsyncCallback<Integer> async );

    void refreshRecords( AsyncCallback<Integer> async );

    void getRecords( int offset, int limit, AsyncCallback<List<TableRecord>> successCallback );
}
