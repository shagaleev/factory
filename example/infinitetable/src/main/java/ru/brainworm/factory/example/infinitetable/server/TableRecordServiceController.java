package ru.brainworm.factory.example.infinitetable.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import ru.brainworm.factory.example.infinitetable.client.service.TableRecordService;
import ru.brainworm.factory.example.infinitetable.shared.TableRecord;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TableRecordServiceController extends RemoteServiceServlet implements TableRecordService {

    public TableRecordServiceController() {
        tableRecords = generateRecords( INITIAL_RECORD_COUNT );
    }

    @Override
    public Integer getRecordsCount() {
        info( "getRecordsCount(): " + tableRecords.size() );

        return tableRecords.size();
    }

    @Override
    public Integer refreshRecords() {
        info( "refreshRecords(): " );
        tableRecords.addAll(generateRecords( INITIAL_RECORD_COUNT ));
        return tableRecords.size();
    }

    @Override
    public synchronized List<TableRecord> getRecords( int offset, int limit ) {
        info("getRecords(): offset="+offset + " limit="+limit);
        if( offset > (tableRecords.size()-1) ) return Collections.emptyList();
        if( offset + limit > tableRecords.size() ) limit = tableRecords.size() - offset;
        try {
            Thread.sleep( 3000 );
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            List<TableRecord> tr = this.tableRecords.subList(offset, offset + limit);
            info("getRecords(): sz="+tr.size() + " first:" + tr.iterator().next()+ " last:" + tr.get( tr.size()-1 ));
            return new ArrayList<TableRecord>(tr);
        }
    }


    private void info( String message ) {
        System.out.println( "TableRecordServiceController " + message );
    }

    private  List<TableRecord> generateRecords( int count ) {
        List<TableRecord> records = new ArrayList<TableRecord>();
        for (int counter = tableRecords.size(); counter < (count+tableRecords.size()); counter++) {
            records.add( new TableRecord( counter, "value_" + counter ) );
        }
        return records;
    }

    public static final int INITIAL_RECORD_COUNT = 20000;
    ExecutorService service = Executors.newSingleThreadExecutor();
    List<TableRecord> tableRecords = new ArrayList<TableRecord>( INITIAL_RECORD_COUNT );
}
