package ru.brainworm.factory.example.infinitetable.client.activity.app;


import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import ru.brainworm.factory.example.infinitetable.client.events.AppEvents;
import ru.brainworm.factory.example.infinitetable.client.service.TableRecordServiceAsync;
import ru.brainworm.factory.example.infinitetable.shared.TableRecord;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;

import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Logger.getLogger;

/**
 * Активити приложения
 */
public abstract class AppActivity
        implements Activity,
        AbstractAppActivity
{

    @Event
    public void onInit( AppEvents.Init event ) {
        view.setActivity( this );

        event.parent.clear();
        event.parent.add( view.asWidget() );

        recordsService.getRecordsCount( new SuccessCallback<Integer>() {

            @Override
            public void onSuccess( Integer integer ) {
                log( "onInit(): recors=" + integer );
                view.setTotalRecords( integer );
            }
        } );
    }


    @Override
    public void onRefreshClicked() {
        recordsService.refreshRecords( new SuccessCallback<Integer>() {
            @Override
            public void onSuccess( Integer integer ) {
                log( "onRefreshClicked(): recors=" + integer );
                view.clearRecords();

                view.setTotalRecords( integer );
            }
        } );
    }

    @Override
    public void loadData( final int offset, final int limit, final AsyncCallback<List<TableRecord>> handler ) {

        recordsService.getRecords( offset, limit, new AsyncCallback<List<TableRecord>>() {
            @Override
            public void onFailure( Throwable throwable ) {
                handler.onFailure( throwable );
            }

            @Override
            public void onSuccess( List<TableRecord> records ) {
                log( "loadData(): recors=" + records.size() );

                handler.onSuccess( records );
            }
        } );
    }

    @Override
    public void onPageChanged( int page ) {
        view.setActivePage( page, false );
    }

    @Override
    public void onPageClicked( Integer page ) {
        view.setActivePage( page, true );
    }

    private void log( String s ) {
        log.info( "log(): " + s );
    }

    private void log( String str, Throwable t ) {
        int stop = 0;
        GWT.log( "AppActivity error: " + str, t );
        log.severe( str + ": " + t.getMessage() );
    }

    @Inject
    AbstractAppView view;
    @Inject
    TableRecordServiceAsync recordsService;

    private static final Logger log = getLogger( AppActivity.class.getName() );
}
