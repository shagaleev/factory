package ru.brainworm.factory.example.infinitetable.client.factory;


import com.google.gwt.inject.client.AbstractGinModule;
import ru.brainworm.factory.example.infinitetable.client.activity.app.AbstractAppView;
import ru.brainworm.factory.example.infinitetable.client.activity.app.AppActivity;
import ru.brainworm.factory.example.infinitetable.client.view.app.AppView;

/**
 * Сама фабрика
 */
public class InfiniteTableExampleModule extends AbstractGinModule {

    protected void configure() {
        bind( AppActivity.class ).asEagerSingleton();
        bind( AbstractAppView.class ).to( AppView.class );
    }
}