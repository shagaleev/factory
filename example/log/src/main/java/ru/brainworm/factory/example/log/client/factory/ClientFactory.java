package ru.brainworm.factory.example.log.client.factory;

import com.google.gwt.inject.client.GinModules;
import ru.brainworm.factory.example.log.client.activity.PageActivity;
import ru.brainworm.factory.generator.injector.client.FactoryInjector;

/**
 * Сама фабрика
 */
@GinModules({
        ClientModule.class
})
public interface ClientFactory
        extends FactoryInjector
{
    PageActivity getPageActivity();
}
