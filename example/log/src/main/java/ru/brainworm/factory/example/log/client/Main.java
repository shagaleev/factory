package ru.brainworm.factory.example.log.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;
import ru.brainworm.factory.example.log.client.events.PageEvents;
import ru.brainworm.factory.example.log.client.factory.ClientFactory;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Main implements EntryPoint {

    public void onModuleLoad() {
        ClientFactory factory = GWT.create( ClientFactory.class );

        factory.getPageActivity().fireEvent( new PageEvents.Init( RootPanel.get() ) );
        factory.getPageActivity().fireEvent( new PageEvents.Show() );
    }
}
