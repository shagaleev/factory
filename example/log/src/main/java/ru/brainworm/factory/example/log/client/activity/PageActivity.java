package ru.brainworm.factory.example.log.client.activity;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;
import ru.brainworm.factory.example.log.client.events.PageEvents;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;

import java.util.Date;
import java.util.logging.Logger;

/**
 * Активность на странице
 */
public abstract class PageActivity implements Activity {

    @Event
    public void onInit( PageEvents.Init event ) {
        this.parent = event.parent;
    }

    @Event
    public void onShow( PageEvents.Show event ) {
        final Label lb = new Label();

        parent.clear();
        parent.add( lb );

        Timer timer = new Timer() {
            @Override
            public void run() {
                String date = new Date().toString();
                lb.setText( date );

                log.fine( date );
            }
        };
        timer.scheduleRepeating( 1000 );
    }

    HasWidgets parent;

    private final static Logger log = Logger.getLogger( PageActivity.class.getName() );
}
