package ru.brainworm.factory.example.log.client.events;

import com.google.gwt.user.client.ui.HasWidgets;

/**
 * Событие инициализации
 */
public class PageEvents {
    /**
     * Событие инициализации
     */
    public static class Init {

        public Init(HasWidgets parent) {
            this.parent = parent;
        }

        public HasWidgets parent;
    }

    /**
    * Событие показать
    */
    public static class Show {
        public Show() {
        }
    }
}
