package ru.brainworm.factory.example.log.client.factory;

import com.google.gwt.inject.client.AbstractGinModule;
import ru.brainworm.factory.example.log.client.activity.PageActivity;

/**
 * Описание классов фабрики
 */
public class ClientModule extends AbstractGinModule {
    @Override
    protected void configure() {
        bind( PageActivity.class ).asEagerSingleton();
    }
}
