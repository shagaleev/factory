package ru.brainworm.factory.example.datetimepicker.client.factory;

import com.google.gwt.inject.client.AbstractGinModule;
import ru.brainworm.factory.example.datetimepicker.client.activity.app.AbstractAppView;
import ru.brainworm.factory.example.datetimepicker.client.activity.app.AppActivity;
import ru.brainworm.factory.example.datetimepicker.client.view.app.AppView;

/**
 * Сама фабрика
 */
public class DatePickerClientModule extends AbstractGinModule {

    protected void configure() {
        bind( AppActivity.class ).asEagerSingleton();

        bind(AbstractAppView.class).to(AppView.class);
        bind(AppActivity.class).asEagerSingleton();
    }
}