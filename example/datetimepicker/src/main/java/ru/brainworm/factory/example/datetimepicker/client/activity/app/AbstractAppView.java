package ru.brainworm.factory.example.datetimepicker.client.activity.app;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;

/**
 * Абстрактная вью  формы приложения
 */
public interface AbstractAppView extends IsWidget {
    void setActivity(AbstractAppActivity appActivity);
}
