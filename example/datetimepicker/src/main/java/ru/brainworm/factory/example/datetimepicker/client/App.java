package ru.brainworm.factory.example.datetimepicker.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;
import ru.brainworm.factory.example.datetimepicker.client.events.AppEvents;
import ru.brainworm.factory.example.datetimepicker.client.factory.DatePickerClientFactory;

public class App implements EntryPoint {
    private final DatePickerClientFactory injector = GWT.create(DatePickerClientFactory.class);

    /**
     * При загрузке приложения
     */
    @Override
    public void onModuleLoad() {
        //  Инициировать (запустить) главное окно
        injector.getAppActivity().fireEvent( new AppEvents.Init( RootPanel.get() ) );

    }
}
