package ru.brainworm.factory.example.datetimepicker.client.view.app;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.google.inject.Inject;
import ru.brainworm.factory.core.datetimepicker.client.view.input.range.RangePicker;
import ru.brainworm.factory.core.datetimepicker.client.view.input.single.SinglePicker;
import ru.brainworm.factory.core.datetimepicker.shared.dto.DateInterval;
import ru.brainworm.factory.example.datetimepicker.client.activity.app.AbstractAppActivity;
import ru.brainworm.factory.example.datetimepicker.client.activity.app.AbstractAppView;

import java.util.Date;


/**
 * Вью приложения
 */
public class AppView extends Composite implements AbstractAppView {

    @Inject
    public void onInit() {
        initWidget(ourUiBinder.createAndBindUi(this));
    }

    @Override
    public void setActivity(AbstractAppActivity appActivity) {
        this.activity = appActivity;
    }

    @UiHandler( {"datePicker", "timePicker", "dateTimePicker", "defaultPicker" } )
    public void onChangeDatePickerValue( ValueChangeEvent<Date > event ) {
        pickerAnswer.setText( event.getValue() == null ? "null" : event.getValue().toString() );
    }

    @UiHandler( {"dateRangePicker", "dateTimeRangePicker", "defaultRangePicker", "dateRangePickerNoMinSec",
            "dateRangePickerOpenDate", "dateRangePickerOpenDateAndFastChoice" } )
    public void onChangeDateRangePickerValue( ValueChangeEvent<DateInterval > event ) {
        pickerAnswer.setText( event.getValue().toString() );
    }

    @UiField
    Label pickerAnswer;
    @Inject
    @UiField ( provided = true )
    SinglePicker defaultPicker;
    @Inject
    @UiField ( provided = true )
    SinglePicker dateTimePicker;
    @Inject
    @UiField ( provided = true )
    SinglePicker timePicker;
    @Inject
    @UiField ( provided = true )
    SinglePicker datePicker;
    @Inject
    @UiField ( provided = true )
    RangePicker defaultRangePicker;
    @Inject
    @UiField ( provided = true )
    RangePicker dateRangePicker;
    @Inject
    @UiField ( provided = true )
    RangePicker dateTimeRangePicker;
    @Inject
    @UiField ( provided = true )
    RangePicker dateRangePickerNoMinSec;
    @Inject
    @UiField ( provided = true )
    RangePicker dateRangePickerOpenDate;
    @Inject
    @UiField ( provided = true )
    RangePicker dateRangePickerOpenDateAndFastChoice;

    private AbstractAppActivity activity;

    interface AntFormUiBinder extends UiBinder<Widget, AppView> {}
    private static AntFormUiBinder ourUiBinder = GWT.create(AntFormUiBinder.class);


}