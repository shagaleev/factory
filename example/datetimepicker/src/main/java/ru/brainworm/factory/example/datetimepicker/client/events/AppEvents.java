package ru.brainworm.factory.example.datetimepicker.client.events;

import com.google.gwt.user.client.ui.HasWidgets;


/**
 * События, относящиеся к форме
 */
public class AppEvents {

    /**
     * Событие инициализации
     */
    public static class Init {
        public Init(HasWidgets parent) {
            this.parent = parent;
        }

        public HasWidgets parent;
    }


}
