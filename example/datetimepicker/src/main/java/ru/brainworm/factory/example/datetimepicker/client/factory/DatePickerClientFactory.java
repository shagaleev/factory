package ru.brainworm.factory.example.datetimepicker.client.factory;

import com.google.gwt.inject.client.GinModules;
import ru.brainworm.factory.generator.injector.client.FactoryInjector;
import ru.brainworm.factory.example.datetimepicker.client.activity.app.AppActivity;

/**
 * Factory
 */
@GinModules({DatePickerClientModule.class})
public interface DatePickerClientFactory
        extends FactoryInjector
{
    AppActivity getAppActivity();
}

