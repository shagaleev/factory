package ru.brainworm.factory.example.datetimepicker.client.activity.app;

import com.google.inject.Inject;
import ru.brainworm.factory.example.datetimepicker.client.events.AppEvents;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;

/**
 * Активити приложения
 */
public abstract class AppActivity
        implements Activity, AbstractAppActivity
{
    @Event
    public void onInit(AppEvents.Init event) {
        view.setActivity( this );

        event.parent.clear();
        event.parent.add(view.asWidget());
    }

    @Inject
    AbstractAppView view;
}
