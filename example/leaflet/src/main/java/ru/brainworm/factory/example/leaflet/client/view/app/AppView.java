package ru.brainworm.factory.example.leaflet.client.view.app;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import com.google.inject.Inject;
import com.gwidgets.api.leaflet.Map;
import ru.brainworm.factory.example.leaflet.client.activity.app.AbstractAppActivity;
import ru.brainworm.factory.example.leaflet.client.activity.app.AbstractAppView;
import ru.brainworm.factory.widget.leaflet.client.LeafletWidget;
import ru.brainworm.factory.widget.leaflet.client.event.MapClickEvent;
import ru.brainworm.factory.widget.leaflet.client.event.MapResourcesReadyEvent;
import ru.brainworm.factory.widget.leaflet.client.event.lasso.LassoSelectionDisabledEvent;
import ru.brainworm.factory.widget.leaflet.client.event.lasso.LassoSelectionEnabledEvent;
import ru.brainworm.factory.widget.leaflet.client.event.lasso.LassoSelectionFinishedEvent;
import ru.brainworm.factory.widget.leaflet.client.plugin.Plugin;

/**
 * Вью приложения
 */
public class AppView extends Composite implements AbstractAppView {

    @Inject
    public void onInit() {
        initWidget( ourUiBinder.createAndBindUi( this ) );
    }

    @Override
    public void setActivity( AbstractAppActivity activity ) {
        this.activity = activity;
    }

    @Override
    public HasValue<String> getLat() {
        return lat;
    }

    @Override
    public HasValue<String> getLng() {
        return lng;
    }

    @Override
    public void setSelected(int amount) {
        selected.setInnerText(String.valueOf(amount));
    }

    @Override
    public boolean containsPlugin(Plugin plugin) {
        return leafletMap.containsPlugin(plugin);
    }

    @Override
    public void setMarkerPoint( double lat, double lng ) {
        leafletMap.setView( lat, lng );
    }

    @Override
    public Map getLeafletMap() {
        return leafletMap.getMapElement();
    }

    @Override
    protected void onAttach(){
        super.onAttach();
        registerHandlers();
    }

    @Override
    protected void onDetach() {
        super.onDetach();
        keyDownHandlerRegistration.removeHandler();
        keyUpHandlerRegistration.removeHandler();
    }


    @UiHandler( "leafletMap" )
    public void onMapClicked( MapClickEvent event ) {
        if ( activity != null ) {
            activity.onMapClicked( event.getLat(), event.getLng() );
        }
    }

    @UiHandler( "leafletMap" )
    public void onSelectionFinished( LassoSelectionFinishedEvent event ) {
        if ( activity != null ) {
            activity.onSelectionFinished( event.getLayers() );
        }
    }

    @UiHandler( "leafletMap" )
    public void onMapResourcesReady( MapResourcesReadyEvent event ) {
        if ( activity != null ) {
            activity.onMapResourcesReady();
        }
    }

    @UiHandler( "leafletMap" )
    public void onSelectionEnabled( LassoSelectionEnabledEvent event ) {
        if ( activity != null ) {
            activity.onSelectionEnabled();
        }
    }

    @UiHandler( "leafletMap" )
    public void onSelectionDisabled( LassoSelectionDisabledEvent event ) {
        if ( activity != null ) {
            activity.onSelectionDisabled();
        }
    }

    @UiHandler( "go" )
    public void onGoClicked( ClickEvent event ) {
        if ( activity != null ) {
            activity.onGoClicked();
        }
    }

    private void registerHandlers() {

        keyDownHandlerRegistration = RootPanel.get().addDomHandler(new KeyDownHandler() {
            @Override
            public void onKeyDown(KeyDownEvent keyDownEvent) {
                if ( activity != null ) {
                    activity.onCtrlKeyPressed(true);
                }
            }
        }, KeyDownEvent.getType());

        keyUpHandlerRegistration = RootPanel.get().addDomHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent keyUpEvent) {
                if ( activity != null ) {
                    activity.onCtrlKeyPressed(false);
                }
            }
        }, KeyUpEvent.getType());
    }

    @UiField
    TextBox lat;

    @UiField
    TextBox lng;

    @UiField
    SpanElement selected;

    @UiField
    LeafletWidget leafletMap;

    private AbstractAppActivity activity;
    private HandlerRegistration keyUpHandlerRegistration;
    private HandlerRegistration keyDownHandlerRegistration;

    interface AppFormUiBinder extends UiBinder<Widget, AppView> {
    }

    private static AppFormUiBinder ourUiBinder = GWT.create( AppFormUiBinder.class );
}