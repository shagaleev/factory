package ru.brainworm.factory.example.leaflet.client.activity.app;

import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IsWidget;
import com.gwidgets.api.leaflet.Map;
import ru.brainworm.factory.widget.leaflet.client.plugin.Plugin;

/**
 * Абстрактная вью формы приложения
 */
public interface AbstractAppView extends IsWidget {
    void setActivity( AbstractAppActivity appActivity );
    HasValue<String> getLat();
    HasValue<String> getLng();

    void setSelected(int amount);

    boolean containsPlugin(Plugin plugin);

    void setMarkerPoint(double lat, double lng );

    Map getLeafletMap();
}
