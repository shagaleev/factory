package ru.brainworm.factory.example.leaflet.client.factory;

import com.google.gwt.inject.client.GinModules;
import ru.brainworm.factory.example.leaflet.client.activity.app.AppActivity;
import ru.brainworm.factory.generator.injector.client.FactoryInjector;


/**
 * Factory
 */
@GinModules( LeafletExampleModule.class )
public interface LeafletExampleFactory
        extends FactoryInjector {
    /**
     * Активити приложения
     */
    AppActivity getAppActivity();

}

