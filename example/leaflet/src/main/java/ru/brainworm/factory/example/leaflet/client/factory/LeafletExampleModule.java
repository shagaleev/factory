package ru.brainworm.factory.example.leaflet.client.factory;


import com.google.gwt.inject.client.AbstractGinModule;
import ru.brainworm.factory.example.leaflet.client.activity.app.AppActivity;
import ru.brainworm.factory.example.leaflet.client.view.app.AppView;
import ru.brainworm.factory.example.leaflet.client.activity.app.AbstractAppView;

/**
 * Сама фабрика
 */
public class LeafletExampleModule extends AbstractGinModule {

    protected void configure() {
        bind( AppActivity.class ).asEagerSingleton();
        bind( AbstractAppView.class ).to( AppView.class );
    }
}