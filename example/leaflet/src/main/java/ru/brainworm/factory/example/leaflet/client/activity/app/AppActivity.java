package ru.brainworm.factory.example.leaflet.client.activity.app;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.inject.Inject;
import com.gwidgets.api.leaflet.L;
import com.gwidgets.api.leaflet.LatLng;
import com.gwidgets.api.leaflet.Map;
import com.gwidgets.api.leaflet.Marker;
import com.gwidgets.api.leaflet.events.EventCallback;
import com.gwidgets.api.leaflet.events.EventTypes;
import com.gwidgets.api.leaflet.options.IconOptions;
import com.gwidgets.api.leaflet.options.MarkerOptions;
import com.gwidgets.api.leaflet.options.PopupOptions;
import com.gwidgets.api.leaflet.options.TooltipOptions;
import ru.brainworm.factory.example.leaflet.client.events.AppEvents;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;
import ru.brainworm.factory.widget.leaflet.client.plugin.LPlugin;
import ru.brainworm.factory.widget.leaflet.client.plugin.Plugin;
import ru.brainworm.factory.widget.leaflet.client.plugin.lasso.LassoControl;
import ru.brainworm.factory.widget.leaflet.client.plugin.semicircle.SemiCircle;
import ru.brainworm.factory.widget.leaflet.client.plugin.semicircle.SemiCircleOptions;

import java.util.logging.Logger;

/**
 * Активити приложения
 */
public abstract class AppActivity
        implements Activity,
        AbstractAppActivity
{

    @Event
    public void onInit( AppEvents.Init event ) {
        view.setActivity( this );

        event.parent.clear();
        event.parent.add( view.asWidget() );

        view.setSelected(0);
    }

    @Override
    public void onMapClicked( Double lat, Double lng ) {
        view.getLat().setValue( lat.toString() );
        view.getLng().setValue( lng.toString() );
    }

    @Override
    public void onGoClicked() {
        try {
            double lat = Double.parseDouble( view.getLat().getValue() );
            double lng = Double.parseDouble( view.getLng().getValue() );
            view.setMarkerPoint( lat, lng );
        } catch ( NumberFormatException ignored ) {}
    }

    @Override
    public void onSelectionFinished(Marker[] markers) {

        selectMarkers(markers);
    }

    @Override
    public void onSelectionEnabled() {
        lassoControl.getContainer().className = lassoControl.getContainer().className + LASSO_CONTROL_ENABLED;
        doUnselectMarkers(markers);
    }

    @Override
    public void onSelectionDisabled() {
        lassoControl.getContainer().className = lassoControl.getContainer().className.replaceAll(LASSO_CONTROL_ENABLED,"");
    }

    @Override
    public void onMapResourcesReady() {
        initMapItems();
    }

    @Override
    public void onCtrlKeyPressed(boolean isCtrlKeyPressed) {
        isCtrlPressed = isCtrlKeyPressed;
    }

    @Override
    public void initMapItems() {

        markers = new Marker[5];
        Map map = view.getLeafletMap();

        if (view.containsPlugin(Plugin.LASSO)){
            lassoControl = LPlugin.LControlPlugin.lasso();
            lassoControl.addTo(map);
        }

        TooltipOptions tooltipOptions = new TooltipOptions.Builder().sticky(true).direction("left").build();
        PopupOptions popupOptions = new PopupOptions.Builder().minWidth(20.0).autoClose(true).build();

        LatLng latLng1 = L.latLng(59.9639475857657,30.382347106933597);
        Marker m1 = L.marker(latLng1, new MarkerOptions.Builder().title("Marker1").build());
        m1.addTo(map);
        markers[0] = m1;
        m1.on(EventTypes.MarkerEvents.CLICK, markerClickCallback(m1));

        m1.bindTooltip(createTooltipContent(m1, "Marker1"), tooltipOptions);

        LatLng latLng2 = L.latLng(59.9386300,30.3141300);
        Marker m2 = L.marker(latLng2, new MarkerOptions.Builder().title("Marker2").build());
        m2.addTo(map);
        markers[1] = m2;
        m2.on(EventTypes.MarkerEvents.CLICK, markerClickCallback(m2));

        m2.bindTooltip(createTooltipContent(m1, "Marker2"), tooltipOptions);

        LatLng latLng3 = L.latLng(59.91786018266457,30.423889160156254);
        Marker m3 = L.marker(latLng3, new MarkerOptions.Builder().title("Marker3").build());
        m3.addTo(map);
        markers[2] = m3;
        m3.on(EventTypes.MarkerEvents.CLICK, markerClickCallback(m3));

        m3.bindTooltip(createTooltipContent(m1, "Marker3"), tooltipOptions);

        LatLng latLng4 = L.latLng(59.89995826181929,30.31539916992188);
        Marker m4 = L.marker(latLng4, new MarkerOptions.Builder().title("Marker4").build());
        m4.addTo(map);
        markers[3] = m4;
        m4.on(EventTypes.MarkerEvents.CLICK, markerClickCallback(m4));

        m4.bindTooltip(createTooltipContent(m1, "Marker4"), tooltipOptions);

        LatLng latLng5 = L.latLng(59.886180984146236,30.418395996093754);
        Marker m5 = L.marker(latLng5, new MarkerOptions.Builder().title("Marker5").build());
        m5.addTo(map);
        markers[4] = m5;

        m5.on(EventTypes.MarkerEvents.CLICK, markerClickCallback(m5));

        m5.bindTooltip(createTooltipContent(m1, "Marker5"), tooltipOptions);

        LatLng semiCircleMarker5 = L.latLng(59.886180984146236,30.418395996093754);
        SemiCircle semiCircle = LPlugin.semiCircle(semiCircleMarker5, new SemiCircleOptions.Builder()
                .radius(5200.0)
                .startAngle(63.0)
                .stopAngle(94.0)
                .color("red")
                .opacity(1.0)
                .weight(2.0)
                .renderer(L.svg(null))
                .build());

        semiCircle.addTo(map);

        semiCircle.bindTooltip("SemiCircle_tooltip", tooltipOptions);
        semiCircle.bindPopup("SemiCircle_popup", popupOptions);
    }

    private String createTooltipContent(Marker marker, String name ) {
        return name + "<br>Lat: " + marker.getLatLng().lat + "<br>" +
                "Lng: " + marker.getLatLng().lng;
    }

    private EventCallback markerClickCallback( Marker marker) {
        return new EventCallback() {
            @Override
            public void call(com.gwidgets.api.leaflet.events.Event event) {

                if(!isCtrlPressed){
                    return;
                }

                if (isAlreadySelected(marker.getLatLng())){
                    setUnselectIcon(marker);
                    selectedList = removeFromArray(selectedList, marker.getLatLng());
                    printSelected();
                    view.setSelected(selectedList == null ? 0 : selectedList.length );
                    return;
                }

                setSelectIcon(marker);
                selectedList = addToArray( selectedList, marker.getLatLng() );
                printSelected();
                view.setSelected(selectedList == null ? 0 : selectedList.length );
            }
        };
    }

    private LatLng[] removeFromArray(LatLng[] array, LatLng element) {

        int i;
        int length = array.length;
        for (i = 0; i < length; i++)
            if (array[i] == element)
                break;

        int indexOfRemovable = i;

        LatLng[] result = new LatLng[length - 1];
        System.arraycopy(array, 0, result, 0, indexOfRemovable);
        if (array.length != indexOfRemovable) {
            System.arraycopy(array, indexOfRemovable + 1, result, indexOfRemovable, array.length - indexOfRemovable - 1);
        }

        return result;
    }

    private LatLng[] addToArray(LatLng array[], LatLng newElement) {

        if (array == null || array.length == 0){
            array = new LatLng[1];
            array[0] = newElement;
            return array;
        }

        int length = array.length;

        LatLng newArray[] = new LatLng[length + 1];

        System.arraycopy(array, 0, newArray, 0, length);

        newArray[length] = newElement;

        return newArray;
    }

    private void printSelected() {
        log.info( "--------------------------------------------->>>> Selected markers amount: " + selectedList.length );

        for (LatLng aSelectedList : selectedList) {
            log.info("Marker selected: " + aSelectedList);
        }
    }

    private void setSelectIcon(Marker marker) {
        marker.setIcon(L.icon(new IconOptions.Builder(LASSO_MARKER_ICON_URL).className(LASSO_MARKER_ICON_SELECTED).build()));
    }

    private void setUnselectIcon(Marker marker) {
        marker.setIcon(L.icon(new IconOptions.Builder(LASSO_MARKER_ICON_URL).className(LASSO_MARKER_ICON).build()));
    }

    private boolean isAlreadySelected(LatLng current) {

        if (selectedList == null || selectedList.length == 0){
            return false;
        }

        for (LatLng latLng : selectedList){
            if (latLng != null && latLng.equals(current)){
                return true;
            }
        }
        return false;
    }

    private void selectMarkers(Marker[] markers) {
        if (markers == null){
            return;
        }

        int lenght = markers.length;
        selectedList = new LatLng[lenght];

        for (int i = 0; i < lenght; i++){
            selectedList[i] = markers[i].getLatLng();
            setSelectIcon(markers[i]);
        }

        printSelected();
        view.setSelected(markers == null ? 0 : markers.length );
    }

    private void doUnselectMarkers(Marker[] markers) {

        log.info( "--------------------------------------------->>>> Selected markers amount: 0" );
        view.setSelected(0);
        if (markers == null){
            return;
        }

        for ( Marker marker : markers ){
            setUnselectIcon(marker);
        }
    }

    @Inject
    AbstractAppView view;
    private final static Logger log = Logger.getLogger( AppActivity.class.toString() );

    private Marker[] markers;
    private LatLng[] selectedList;
    private LassoControl lassoControl;

    private boolean isCtrlPressed = false;

    private final String LASSO_CONTROL_ENABLED = " lasso-control-enabled";
    private final String LASSO_MARKER_ICON_SELECTED = "selected";
    private final String LASSO_MARKER_ICON = "leaflet-marker-icon leaflet-zoom-animated leaflet-interactive";
    private final String LASSO_MARKER_ICON_URL = "App/leaflet/images/marker-icon.png";
}
