package ru.brainworm.factory.example.leaflet.client.activity.app;

import com.gwidgets.api.leaflet.Marker; /**
 * Абстрактный активити приложения
 */
public interface AbstractAppActivity {
    void onMapClicked( Double lat, Double lng );
    void onGoClicked();
    void initMapItems();

    void onSelectionFinished(Marker[] layers);

    void onSelectionEnabled();

    void onSelectionDisabled();

    void onMapResourcesReady();

    void onCtrlKeyPressed(boolean isCtrlKeyPressed);
}
