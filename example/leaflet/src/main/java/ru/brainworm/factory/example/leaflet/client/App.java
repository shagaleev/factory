package ru.brainworm.factory.example.leaflet.client;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;
import ru.brainworm.factory.example.leaflet.client.events.AppEvents;
import ru.brainworm.factory.example.leaflet.client.factory.LeafletExampleFactory;

public class App implements EntryPoint {
    private final LeafletExampleFactory injector = GWT.create( LeafletExampleFactory.class );

    @Override
    public void onModuleLoad() {
        injector.getAppActivity().fireEvent( new AppEvents.Init( RootPanel.get() ) );

    }
}
