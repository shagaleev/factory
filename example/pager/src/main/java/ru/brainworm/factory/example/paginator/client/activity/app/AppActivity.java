package ru.brainworm.factory.example.paginator.client.activity.app;


import com.google.gwt.user.client.ui.HasWidgets;
import com.google.inject.Inject;
import ru.brainworm.factory.example.paginator.client.events.AppEvents;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;
import ru.brainworm.factory.widget.table.client.pager.events.PagerEvents;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Активити приложения
 */
public abstract class AppActivity
        implements Activity,
        AbstractAppActivity
{

    @Event
    public void onInit( AppEvents.Init event ) {
        view.setActivity( this );
        log.log( Level.FINEST, "onInit(): event.getParent()=" + event.parent );
        parent = event.parent;

        view.addElements( 0, 10 );

        fireEvent( new PagerEvents.Show( view.getPagerContainer(), 10, total ) );
        fireEvent( new PagerEvents.Show( view.getPagerContainer2(), 5, total2 ) );

        parent.add( view.asWidget() );
    }

    @Event
    void oRangeChanged( PagerEvents.RangeChanged event ) {
        if ( event.parent != null ) {
            int t;
            if ( event.parent == view.getPagerContainer() ) {
                t = (total += 5);
            } else {
                t = (total2 += 2);
            }

            fireEvent( new PagerEvents.Change( event.parent, event.start, t ) );
            view.showPickerAnswer( "Start=" + (event.start + 1)
                            + " : Limit=" + event.limit + " (on page)"
                            + " : Total=" + t
            );

            view.addElements( event.start, event.limit );

        } else {
            view.showPickerAnswer( "" );
        }
    }


    @Inject
    AbstractAppView view;

    HasWidgets parent;

    Integer total = 55;
    Integer total2 = 22;

    private final static Logger log = Logger.getLogger( "PagerActivity" );
}
