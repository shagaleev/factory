package ru.brainworm.factory.example.paginator.client;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;
import ru.brainworm.factory.example.paginator.client.events.AppEvents;
import ru.brainworm.factory.example.paginator.client.factory.PagerExampleFactory;

import java.util.logging.Level;
import java.util.logging.Logger;

public class App implements EntryPoint {
    private final PagerExampleFactory injector = GWT.create( PagerExampleFactory.class );

    /**
     * При загрузке приложения
     */
    @Override
    public void onModuleLoad() {
        log.log( Level.FINEST, "onModuleLoad():." );
        //  Инициировать (запустить) главное окно
        injector.getAppActivity().fireEvent( new AppEvents.Init( RootPanel.get() ) );

    }

    private final static Logger log = Logger.getLogger( "EntryPoint" );
}
