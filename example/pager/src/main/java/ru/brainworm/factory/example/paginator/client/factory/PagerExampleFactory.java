package ru.brainworm.factory.example.paginator.client.factory;


import com.google.gwt.inject.client.GinModules;
import ru.brainworm.factory.example.paginator.client.activity.app.AppActivity;
import ru.brainworm.factory.generator.injector.client.FactoryInjector;
import ru.brainworm.factory.widget.table.client.pager.factory.PagerClientModule;


/**
 * Factory
 */
@GinModules({PagerExampleModule.class, PagerClientModule.class})
public interface PagerExampleFactory
        extends FactoryInjector
{

    AppActivity getAppActivity();

}

