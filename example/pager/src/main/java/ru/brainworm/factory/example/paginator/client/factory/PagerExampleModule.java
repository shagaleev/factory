package ru.brainworm.factory.example.paginator.client.factory;


import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;
import ru.brainworm.factory.example.paginator.client.activity.app.AbstractAppView;
import ru.brainworm.factory.example.paginator.client.activity.app.AppActivity;
import ru.brainworm.factory.example.paginator.client.view.app.AppView;

/**
 * Сама фабрика
 */
public class PagerExampleModule extends AbstractGinModule {

    protected void configure() {
        bind(SimpleEventBus.class).in(Singleton.class);

        bind( AppActivity.class ).asEagerSingleton();

        //  App (приложение)
        //      not a Singleton (multiple view)
        bind(AbstractAppView.class).to(AppView.class);
        bind(AppActivity.class).asEagerSingleton();


    }
}