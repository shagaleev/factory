package ru.brainworm.factory.example.paginator.client.activity.app;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;

/**
 * Абстрактная вью  формы приложения
 */
public interface AbstractAppView extends IsWidget {

    /**
     * Получить контейнер для размещения пикера
     *
     * @return контейнер для размещения пикера
     */
    public HasWidgets getPagerContainer();

    public HasWidgets getPagerContainer2();

    /**
     * Установить активити для приложения
     *
     * @param appActivity
     */
    void setActivity( AbstractAppActivity appActivity );

    /**
     * Отобразить ответ от пикера
     *
     * @param timeStr время  как строка
     */
    void showPickerAnswer( String timeStr );

    /**
     * Сгенерировать елементы
     *
     * @param start
     * @param limit
     */
    public void addElements( int start, int limit );
}
