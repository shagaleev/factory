package ru.brainworm.factory.example.paginator.client.view.app;


import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;
import ru.brainworm.factory.example.paginator.client.activity.app.AbstractAppActivity;
import ru.brainworm.factory.example.paginator.client.activity.app.AbstractAppView;

/**
 * Вью приложения
 */
public class AppView extends Composite implements AbstractAppView {


    public AppView() {
        initWidget(ourUiBinder.createAndBindUi(this));
    }

    @Override
    public HasWidgets getPagerContainer() {
        return pagerContainer;
    }

    @Override
    public HasWidgets getPagerContainer2() {
        return pagerContainer2;
    }

    @Override
    public void setActivity(AbstractAppActivity appActivity) {
        this.activity = appActivity;
    }

    @Override
    public void showPickerAnswer( String timeStr ) {
        pickerAnswer.setText( timeStr );
    }

    @Override
    public void addElements(int start, int limit){
        elements.clear();
        for (int i = start+1; i <(start+limit+1); i++) {
            elements.add( new Label( "Element     "+i ) );
        }
    }

    @UiField
    Label pickerAnswer;
    @UiField
    SimplePanel pagerContainer;
    @UiField
    SimplePanel pagerContainer2;
    @UiField
    HTMLPanel elements;

    private AbstractAppActivity activity;

    interface AntFormUiBinder extends UiBinder<Widget, AppView> {
    }

    private static AntFormUiBinder ourUiBinder = GWT.create( AntFormUiBinder.class );


}