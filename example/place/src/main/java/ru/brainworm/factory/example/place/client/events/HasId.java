package ru.brainworm.factory.example.place.client.events;

import ru.brainworm.factory.generator.place.client.DefaultPlaceEvent;

/**
 * Класс для событий с id
 */
public class HasId extends DefaultPlaceEvent {

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Long id;
}
