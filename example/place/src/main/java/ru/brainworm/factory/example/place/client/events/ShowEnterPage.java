package ru.brainworm.factory.example.place.client.events;

import ru.brainworm.factory.generator.place.client.DefaultPlaceEvent;
import ru.brainworm.factory.generator.place.client.Omit;
import ru.brainworm.factory.generator.place.client.StackInfo;
import ru.brainworm.factory.generator.router.client.annotations.Url;

/**
 * Место для перехода на страницу ввода
 */
@Url( "enter" )
public class ShowEnterPage
        extends HasId
{
    public ShowEnterPage() {
    }

    public ShowEnterPage(Long id) {
        this.id = id;
    }

    @Override
    public Long getOperationId() {
        return opId;
    }

    @Override
    public StackInfo getStackInfo() {
        return stackInfo;
    }

    @Override
    public void setStackInfo( StackInfo info ) {
        stackInfo = info;
    }

    @Omit
    public StackInfo stackInfo;

    public Long opId;

}
