package ru.brainworm.factory.example.place.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import ru.brainworm.factory.example.place.client.events.ShowEnterPage;
import ru.brainworm.factory.example.place.client.factory.ClientFactory;
import ru.brainworm.factory.example.place.client.events.Init;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Main implements EntryPoint {

    public void onModuleLoad() {
        ClientFactory factory = GWT.create( ClientFactory.class );

        factory.getEnterPageActivity().fireEvent( new Init( RootLayoutPanel.get() ) );
        factory.getEnterPageActivity().fireEvent( new ShowEnterPage() );
    }
}
