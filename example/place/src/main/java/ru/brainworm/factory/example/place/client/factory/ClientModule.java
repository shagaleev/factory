package ru.brainworm.factory.example.place.client.factory;

import com.google.gwt.inject.client.AbstractGinModule;
import ru.brainworm.factory.example.place.client.activity.DisplayPageActivity;
import ru.brainworm.factory.example.place.client.activity.EnterPageActivity;

/**
 * Описание классов фабрики
 */
public class ClientModule extends AbstractGinModule {
    @Override
    protected void configure() {
        bind( EnterPageActivity.class ).asEagerSingleton();
        bind( DisplayPageActivity.class ).asEagerSingleton();
    }
}
