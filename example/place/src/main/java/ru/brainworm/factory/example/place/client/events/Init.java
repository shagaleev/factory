package ru.brainworm.factory.example.place.client.events;

import com.google.gwt.user.client.ui.HasWidgets;

/**
 * Событие инициализации
 */
public class Init {
    public Init( HasWidgets parent ) {
        this.parent = parent;
    }

    public HasWidgets parent;
}
