package ru.brainworm.factory.example.place.client.events;

import ru.brainworm.factory.example.place.client.activity.EnterPageActivity;
import ru.brainworm.factory.generator.place.client.DefaultPlaceEvent;
import ru.brainworm.factory.generator.router.client.annotations.Name;
import ru.brainworm.factory.generator.router.client.annotations.Url;

import java.util.Date;

/**
 * Событие — показать страницу с заполненной формой
 */
@Url( "display" )
public class ShowDisplayPage extends DefaultPlaceEvent {

    public ShowDisplayPage() {
    }

    public ShowDisplayPage( Long opId, Integer id, String name, Date date, EnterPageActivity.ENUM enm ) {
        this.opId = opId;
        this.id = id;
        this.name = name;
        this.date = date;
        this.enm = enm;
    }

    @Name( "number" )
    public Integer id;

    public String name;

    public Date date;

    @Name( "e" )
    public EnterPageActivity.ENUM enm;

}
