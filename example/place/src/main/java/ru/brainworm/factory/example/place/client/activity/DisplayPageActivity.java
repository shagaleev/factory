package ru.brainworm.factory.example.place.client.activity;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import ru.brainworm.factory.example.place.client.events.HasId;
import ru.brainworm.factory.example.place.client.events.Init;
import ru.brainworm.factory.example.place.client.events.ShowDisplayPage;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;
import ru.brainworm.factory.generator.injector.client.PostConstruct;
import ru.brainworm.factory.generator.place.client.GoBackPlaceEvent;

/**
 * Активность на странице 2
 */
public abstract class DisplayPageActivity implements Activity {

    @PostConstruct
    public void onActivate() {
        Window.alert( "DisplayPageActivity created" );
    }

    @Event
    public void onInit( Init event ) {
        this.parent = event.parent;
    }

    @Event
    public void onShowEnterPage( HasId event ) {
        this.lastEvent = event;
    }

    @Event
    public void onShow( final ShowDisplayPage event ) {
        parent.clear();

        VerticalPanel panel = new VerticalPanel();
        parent.add( panel );

        panel.add( new Label( String.valueOf( event.id ) ) );
        panel.add( new Label( event.name ) );
        panel.add( new Label( event.date.toString() ) );
        panel.add( new Label( event.enm.name() ) );


        Button back = new Button( "Назад" );
        back.addClickHandler( new ClickHandler() {
            @Override
            public void onClick( ClickEvent clickEvent ) {
                fireEvent( new GoBackPlaceEvent( event.getOperationId(), null ) );
            }
        } );
        panel.add( back );
    }

    HasWidgets parent;
    HasId lastEvent;
}
