package ru.brainworm.factory.example.place.client.factory;

import com.google.gwt.inject.client.GinModules;
import ru.brainworm.factory.example.place.client.activity.EnterPageActivity;
import ru.brainworm.factory.generator.injector.client.FactoryInjector;

/**
 * Сама фабрика
 */
@GinModules({
        ClientModule.class
})
public interface ClientFactory
        extends FactoryInjector
{
    EnterPageActivity getEnterPageActivity();
}
