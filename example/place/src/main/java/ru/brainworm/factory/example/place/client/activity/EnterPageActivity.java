package ru.brainworm.factory.example.place.client.activity;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;
import ru.brainworm.factory.example.place.client.events.Init;
import ru.brainworm.factory.example.place.client.events.ShowDisplayPage;
import ru.brainworm.factory.example.place.client.events.ShowEnterPage;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;

import java.util.Date;

/**
 * Активность на странице ввода данных
 */
public abstract class EnterPageActivity implements Activity {

    public static enum ENUM {
        T1,
        T2
    }

    @Event
    public void onInit( Init event ) {
        this.parent = event.parent;
    }

    @Event
    public void onShow( ShowEnterPage event ) {
        if ( event.getStackInfo().index < event.getStackInfo().stackSize-1 ) {
            return;
        }

        parent.clear();

        VerticalPanel panel = new VerticalPanel();
        parent.add( panel );

        panel.add( new Label( "Введите имя ангийскими символами" ) );

        final TextBox name = new TextBox();
        panel.add( name );

        final Button submit = new Button( "Отправить" );
        submit.addClickHandler( new ClickHandler() {
            @Override
            public void onClick( ClickEvent event ) {
                fireEvent( new ShowDisplayPage( new Date().getTime(), ++id, name.getText(), new Date(), ENUM.T2 ) );
            }
        } );
        panel.add( submit );
    }

    HasWidgets parent;
    Integer id = 0;
}
