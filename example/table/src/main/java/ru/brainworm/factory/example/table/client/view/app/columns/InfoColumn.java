package ru.brainworm.factory.example.table.client.view.app.columns;

import com.google.gwt.user.client.Element;
import com.google.inject.Inject;
import ru.brainworm.factory.example.table.client.view.btnwidget.BtnWidget;
import ru.brainworm.factory.example.table.shared.TableRecord;
import ru.brainworm.factory.widget.table.client.helper.DetailsColumn;

/**
 * Колонка c деревом
 */
public class InfoColumn extends DetailsColumn<TableRecord> {

    @Inject
    public InfoColumn( BtnWidget widget ) {
        this.widget = widget;
    }

    @Override
    public void render(TableRecord data, final Element element) {
        element.setInnerText( "Данные детализации" );
    }

    BtnWidget widget;

}
