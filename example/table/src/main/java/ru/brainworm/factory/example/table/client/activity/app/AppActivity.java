package ru.brainworm.factory.example.table.client.activity.app;


import com.google.inject.Inject;
import ru.brainworm.factory.example.table.client.events.AppEvents;
import ru.brainworm.factory.example.table.shared.TableRecord;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

/**
 * Активити приложения
 */
public abstract class AppActivity
        implements Activity,
        AbstractAppActivity
{

    @Event
    public void onInit( AppEvents.Init event ) {
        view.setActivity( this );

        event.parent.clear();
        event.parent.add( view.asWidget() );

        List<TableRecord> records = new ArrayList<TableRecord>();
        for ( counter = 0; counter < 20; counter++ ) {
            records.add( new TableRecord( counter, "value_" + counter ) );
        }
        this.records = records;
        view.addRecords( records );

    }

    @Override
    public void onExpand( TableRecord value ) {

        Comparator<TableRecord> comparator = new Comparator<TableRecord>() {
            @Override
            public int compare(TableRecord o1, TableRecord o2) {
                return o1.equals(o2) ? 0 : -1;
            }
        };

        view.updateRow( value, comparator );
    }

    @Override
    public void onCollapse( TableRecord value ) {
        view.updateRow( value );
    }

    @Override
    public void onRefreshClicked() {
        view.clearRecords();

        List<TableRecord> records = new ArrayList<TableRecord>();
        for ( counter = 0; counter < 30; counter++ ) {
            records.add( new TableRecord( counter, "new_value_" + counter ) );
        }
        view.addRecords( records );
        this.records = records;
    }

    @Override
    public void onAddRowClicked() {
        records.add( new TableRecord( counter, "new_value_" + counter ) );
        view.clearRows();
        view.addRecords( records );
        counter++;
    }

    @Override
    public void onRemoveLastRowClicked() {
        counter--;
        records.remove( counter );
        view.clearRows();
        view.addRecords( records );
    }

    @Inject
    AbstractAppView view;

    private List<TableRecord> records = new ArrayList<TableRecord>();
    private int counter = 0;

    private final static Logger log = Logger.getLogger( "PagerActivity" );
}
