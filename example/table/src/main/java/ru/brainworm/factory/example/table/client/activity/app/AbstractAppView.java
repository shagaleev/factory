package ru.brainworm.factory.example.table.client.activity.app;

import com.google.gwt.user.client.ui.IsWidget;
import ru.brainworm.factory.example.table.shared.TableRecord;

import java.util.Comparator;
import java.util.List;

/**
 * Абстрактная вью  формы приложения
 */
public interface AbstractAppView extends IsWidget {
    void setActivity( AbstractAppActivity appActivity );

    void addRecords( List<TableRecord> records );

    void clearRecords();

    void clearRows();

    void updateRow( TableRecord value );

    void updateRow(TableRecord row, Comparator comparator);
}
