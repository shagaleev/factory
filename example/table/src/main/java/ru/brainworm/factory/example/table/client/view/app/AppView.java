package ru.brainworm.factory.example.table.client.view.app;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.*;
import com.google.inject.Inject;
import ru.brainworm.factory.example.table.client.activity.app.AbstractAppActivity;
import ru.brainworm.factory.example.table.client.activity.app.AbstractAppView;
import ru.brainworm.factory.example.table.client.view.app.columns.InfoColumn;
import ru.brainworm.factory.example.table.shared.TableRecord;
import ru.brainworm.factory.widget.table.client.TableWidget;
import ru.brainworm.factory.example.table.client.view.app.columns.ExpandableColumn;
import ru.brainworm.factory.widget.table.client.helper.RowModeColumnProvider;
import ru.brainworm.factory.widget.table.client.helper.SelectionColumn;

import java.util.Comparator;
import java.util.List;

/**
 * Вью приложения
 */
public class AppView extends Composite implements AbstractAppView {

    @Inject
    public AppView( InfoColumn infoColumn ) {
        this.infoColumn = infoColumn;

        initWidget(ourUiBinder.createAndBindUi(this));

        initTable();
    }

    @Override
    public void setActivity(AbstractAppActivity activity) {
        this.activity = activity;

        idColumn.setHandler( activity );
        valueColumn.setHandler( activity );
    }

    @Override
    public void addRecords( List<TableRecord> records ) {
        for ( TableRecord record : records ) {
            table.addRow( record );
        }
        getTableScreenShot();
    }

    @Override
    public void clearRecords() {
        selectionColumn.clear();
        table.clearRows();
    }

    @Override
    public void clearRows() {
        table.clearRows();
    }

    @Override
    public void updateRow(TableRecord row) {
        table.updateRow( row );
    }

    @Override
    public void updateRow(TableRecord row, Comparator comparator) {
        table.updateRow( row, comparator );
    }

    @UiHandler( "refresh" )
    public void onRefreshClicked(ClickEvent event) {
        if ( activity != null ) {
            activity.onRefreshClicked();
        }
    }

    @UiHandler( "add" )
    public void onAddClicked(ClickEvent event) {
        if ( activity != null ) {
            activity.onAddRowClicked();
        }
    }

    @UiHandler( "remove" )
    public void onRemoveClicked(ClickEvent event) {
        if ( activity != null ) {
            activity.onRemoveLastRowClicked();
        }
    }

    private void initTable() {
        table.addColumn( selectionColumn.header, selectionColumn.values );
        table.addColumn( idColumn.header, idColumn.values );
        table.addColumn( valueColumn.header, valueColumn.values );
        table.addColumn( infoColumn.header, infoColumn.values );

        infoColumn.setRowModeProvider( rowModeColumnProvider );
        idColumn.setRowModeProvider( rowModeColumnProvider );
        valueColumn.setRowModeProvider( rowModeColumnProvider );
    }

    private void getTableScreenShot() {
        GWT.log( "headers:" + table.getHeaderValues() );
        GWT.log( "rows:" + table.getRowsValues() );
    }

    @UiField
    TableWidget table;
    @UiField
    Button refresh;
    @UiField
    Button add;
    @UiField
    Button remove;

    ExpandableColumn<TableRecord> idColumn = new ExpandableColumn<TableRecord>() {
        @Override
        protected void fillColumnHeader( Element element ) {
            element.setInnerText( "id" );
        }

        @Override
        public void fillColumnValue( Element element, TableRecord tableRecord ) {
            element.setInnerText( String.valueOf( tableRecord.id ) );
        }
    };

    ExpandableColumn<TableRecord> valueColumn = new ExpandableColumn<TableRecord>() {
        @Override
        protected void fillColumnHeader( Element element ) {
            element.setInnerText( "value" );
        }

        @Override
        public void fillColumnValue( Element element, TableRecord tableRecord ) {
            element.setInnerText( tableRecord.value );
        }
    };

    InfoColumn infoColumn;
    SelectionColumn<TableRecord> selectionColumn = new SelectionColumn<TableRecord>();
    RowModeColumnProvider<TableRecord> rowModeColumnProvider = new RowModeColumnProvider< TableRecord >();

    private AbstractAppActivity activity;

    interface AppFormUiBinder extends UiBinder<Widget, AppView> {}
    private static AppFormUiBinder ourUiBinder = GWT.create( AppFormUiBinder.class );
}