package ru.brainworm.factory.example.table.client.factory;

import com.google.gwt.inject.client.GinModules;
import ru.brainworm.factory.example.table.client.activity.app.AppActivity;
import ru.brainworm.factory.generator.injector.client.FactoryInjector;
import ru.brainworm.factory.widget.table.client.pager.factory.PagerClientModule;


/**
 * Factory
 */
@GinModules({TableExampleModule.class, PagerClientModule.class})
public interface TableExampleFactory
        extends FactoryInjector
{
    /**
     * Активити приложения
     * @return
     */
    AppActivity getAppActivity();

}

