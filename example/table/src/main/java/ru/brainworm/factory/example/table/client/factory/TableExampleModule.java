package ru.brainworm.factory.example.table.client.factory;


import com.google.gwt.inject.client.AbstractGinModule;
import ru.brainworm.factory.example.table.client.activity.app.AbstractAppView;
import ru.brainworm.factory.example.table.client.activity.app.AppActivity;
import ru.brainworm.factory.example.table.client.view.app.AppView;

/**
 * Сама фабрика
 */
public class TableExampleModule extends AbstractGinModule {

    protected void configure() {
        bind( AppActivity.class ).asEagerSingleton();
        bind( AbstractAppView.class ).to( AppView.class );
    }
}