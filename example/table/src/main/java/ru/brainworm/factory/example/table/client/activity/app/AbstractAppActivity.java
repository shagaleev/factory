package ru.brainworm.factory.example.table.client.activity.app;

import ru.brainworm.factory.example.table.client.view.app.columns.ExpandableColumn;
import ru.brainworm.factory.example.table.shared.TableRecord;
import ru.brainworm.factory.widget.table.client.helper.TreeColumn;

/**
 * Абстрактный активити приложения
 */
public interface AbstractAppActivity extends ExpandableColumn.Handler<TableRecord> {

    @Override
    void onExpand( TableRecord row );

    @Override
    void onCollapse( TableRecord row );

    void onRefreshClicked();

    void onAddRowClicked();

    void onRemoveLastRowClicked();
}
