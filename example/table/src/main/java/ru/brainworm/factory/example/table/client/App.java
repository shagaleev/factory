package ru.brainworm.factory.example.table.client;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;
import ru.brainworm.factory.example.table.client.events.AppEvents;
import ru.brainworm.factory.example.table.client.factory.TableExampleFactory;

public class App implements EntryPoint {
    private final TableExampleFactory injector = GWT.create( TableExampleFactory.class );

    @Override
    public void onModuleLoad() {
        injector.getAppActivity().fireEvent( new AppEvents.Init( RootPanel.get() ) );

    }
}
