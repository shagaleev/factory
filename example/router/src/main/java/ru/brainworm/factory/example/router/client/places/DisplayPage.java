package ru.brainworm.factory.example.router.client.places;

import ru.brainworm.factory.example.router.client.activity.EnterPageActivity;
import ru.brainworm.factory.example.router.client.converter.DateConverter;
import ru.brainworm.factory.generator.router.client.annotations.Converter;
import ru.brainworm.factory.generator.router.client.annotations.Name;
import ru.brainworm.factory.generator.router.client.annotations.Url;

import java.util.Date;

/**
 * Описатель переходов на страницу отображения
 */
public class DisplayPage {
    /**
     * Место для перехода на страницу отображения
     */
    @Url( "display_page" )
    public static interface Place extends ru.brainworm.factory.generator.router.client.place.Place {
        void go(
                @Name( "number" ) Integer id,
                String name,
                /*@Converter( DateConverter.class )*/ Date date,
                EnterPageActivity.ENUM e
        );

        String makeUrl( @Name( "number" ) Integer id, String name );
    }

    /**
     * Параметры
     */
    public static interface Params extends ru.brainworm.factory.generator.router.client.params.Params {
        @Name( "number" )
        Integer getId();

        @Name( "name" )
        String getName();

//        @Converter( DateConverter.class )
        Date getDate();

        @Name( "e" )
        EnterPageActivity.ENUM getEnum();
    }
}
