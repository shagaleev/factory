package ru.brainworm.factory.example.router.client.activity;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.inject.Inject;
import ru.brainworm.factory.example.router.client.events.Init;
import ru.brainworm.factory.example.router.client.places.DisplayPage;
import ru.brainworm.factory.example.router.client.places.EnterPage;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;
import ru.brainworm.factory.generator.router.client.annotations.Route;

/**
 * Активность на странице 2
 */
public abstract class DisplayPageActivity implements Activity {
    @Event
    public void onInit( Init event ) {
        this.parent = event.parent;
    }

    @Route( DisplayPage.Place.class )
    public void onShow( DisplayPage.Params params ) {
        parent.clear();

        VerticalPanel panel = new VerticalPanel();
        parent.add( panel );

        panel.add( new Label( String.valueOf( params.getId() ) ) );
        panel.add( new Label( params.getName() ) );
        panel.add( new Label( params.getDate().toString() ) );
        panel.add( new Label( displayPagePlace.makeUrl( params.getId(), params.getName() ) ) );
        panel.add( new Label( params.getEnum().name() ) );


        Button back = new Button( "Назад" );
        back.addClickHandler( new ClickHandler() {
            @Override
            public void onClick( ClickEvent event ) {
                enterPagePlace.go();
            }
        } );
        panel.add( back );
    }

    HasWidgets parent;

    @Inject
    EnterPage.Place enterPagePlace;

    @Inject
    DisplayPage.Place displayPagePlace;
}
