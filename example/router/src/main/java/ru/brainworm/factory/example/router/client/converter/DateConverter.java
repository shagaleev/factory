package ru.brainworm.factory.example.router.client.converter;

import com.google.gwt.i18n.client.DateTimeFormat;
import ru.brainworm.factory.generator.router.client.converter.AbstractParamTypeConverter;

import java.util.Date;

/**
 * Конвертер для дат
 */
public class DateConverter implements AbstractParamTypeConverter<Date>{
    @Override
    public String marshall( Date value ) {
        return value == null ? null : format.format( value );
    }

    @Override
    public Date unmarshall( String value ) {
        return value == null ? null : format.parse( value );
    }

    DateTimeFormat format = DateTimeFormat.getFormat( "dd-MM-yyyy" );
}
