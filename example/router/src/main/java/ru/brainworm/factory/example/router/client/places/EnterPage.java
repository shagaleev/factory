package ru.brainworm.factory.example.router.client.places;

import ru.brainworm.factory.generator.router.client.annotations.Url;

/**
 * Место для перехода на страницу ввода
 */
public class EnterPage {
    @Url( "enter_page" )
    public static interface Place extends ru.brainworm.factory.generator.router.client.place.Place {
        void go();
    }
}
