package ru.brainworm.factory.example.router.client.activity;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;
import com.google.inject.Inject;
import ru.brainworm.factory.example.router.client.events.Init;
import ru.brainworm.factory.example.router.client.places.DisplayPage;
import ru.brainworm.factory.example.router.client.places.EnterPage;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;
import ru.brainworm.factory.generator.router.client.annotations.DefaultPlace;
import ru.brainworm.factory.generator.router.client.annotations.Route;
import ru.brainworm.factory.generator.router.client.place.PlaceHistory;

import java.util.Date;

/**
 * Активность на странице 1
 */
public abstract class EnterPageActivity implements Activity {

    public static enum ENUM {
        T1,
        T2
    }

    @Event
    public void onInit( Init event ) {
        this.parent = event.parent;
    }

    @Route( EnterPage.Place.class )
    public void onShow() {
        parent.clear();

        VerticalPanel panel = new VerticalPanel();
        parent.add( panel );

        panel.add( new Label( "Введите имя ангийскими символами" ) );

        final TextBox name = new TextBox();
        panel.add( name );

        final Button submit = new Button( "Отправить" );
        submit.addClickHandler( new ClickHandler() {
            @Override
            public void onClick( ClickEvent event ) {
                displayPagePlace.go( ++id, name.getText(), new Date(), ENUM.T2 );
            }
        } );
        panel.add( submit );
    }

    HasWidgets parent;
    Integer id = 0;

    @Inject
    DisplayPage.Place displayPagePlace;

    @DefaultPlace( EnterPage.Place.class )
    public PlaceHistory enterHistory;
}
