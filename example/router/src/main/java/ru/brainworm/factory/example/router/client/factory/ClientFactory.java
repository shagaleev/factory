package ru.brainworm.factory.example.router.client.factory;

import com.google.gwt.inject.client.GinModules;
import ru.brainworm.factory.example.router.client.activity.EnterPageActivity;
import ru.brainworm.factory.example.router.client.places.EnterPage;
import ru.brainworm.factory.generator.injector.client.FactoryInjector;

/**
 * Сама фабрика
 */
@GinModules({
        ClientModule.class
})
public interface ClientFactory
        extends FactoryInjector
{
    EnterPageActivity getEnterPageActivity();
    EnterPage.Place getEnterPagePlace();
}
