package ru.brainworm.factory.example.router.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import ru.brainworm.factory.example.router.client.events.Init;
import ru.brainworm.factory.example.router.client.factory.ClientFactory;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Main implements EntryPoint {

    public void onModuleLoad() {
        ClientFactory factory = GWT.create( ClientFactory.class );
        factory.getEnterPageActivity().fireEvent( new Init( RootLayoutPanel.get() ) );
        factory.getEnterPagePlace().go();
    }
}
