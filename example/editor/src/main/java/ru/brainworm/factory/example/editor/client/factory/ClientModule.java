package ru.brainworm.factory.example.editor.client.factory;

import com.google.gwt.inject.client.AbstractGinModule;
import ru.brainworm.factory.example.editor.client.activity.ChildActivity;
import ru.brainworm.factory.example.editor.client.activity.TestActivity;

/**
 * Описание классов фабрики
 */
public class ClientModule extends AbstractGinModule {
    @Override
    protected void configure() {
        bind( TestActivity.class ).asEagerSingleton();
        bind( ChildActivity.class ).asEagerSingleton();
    }
}
