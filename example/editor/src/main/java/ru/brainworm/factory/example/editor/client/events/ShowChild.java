package ru.brainworm.factory.example.editor.client.events;

import com.google.gwt.user.client.ui.HasWidgets;
import ru.brainworm.factory.example.editor.client.DTO;

/**
 * Событие отображения
 */
public class ShowChild {
    public ShowChild( HasWidgets parent, DTO.ChildItem childItem ) {
        this.parent = parent;
        this.childItem = childItem;
    }

    public HasWidgets parent;
    public DTO.ChildItem childItem;
}
