package ru.brainworm.factory.example.editor.client.factory;

import com.google.gwt.inject.client.GinModules;
import ru.brainworm.factory.example.editor.client.activity.TestActivity;
import ru.brainworm.factory.generator.injector.client.FactoryInjector;

/**
 * Сама фабрика
 */
@GinModules({
        ClientModule.class
})
public interface ClientFactory
        extends FactoryInjector
{
    TestActivity getActivity();
}
