package ru.brainworm.factory.example.editor.client.views;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import ru.brainworm.factory.example.editor.client.events.ChildChanged;
import ru.brainworm.factory.example.editor.client.events.ShowChild;
import ru.brainworm.factory.generator.editor.client.annotations.DataField;

import java.util.Date;

/**
 * Класс вьюхи с аннотированными методами
 */
public class WriteView {
    @DataField( name = "methodDate" )
    public void setDate( Date date ) {
        RootPanel.get().add( new Label( "date: " + date.toString() ) );
    }

    @DataField( name = "fieldString" )
    public void setName( String value ) {
        RootPanel.get().add( new Label( "name: " + value ) );
    }

    @DataField( name = "fieldInteger" )
    public void setInt( Integer value ) {
        RootPanel.get().add( new Label( "int: " + Integer.toString( value ) ) );
    }

    @DataField( name = "methodLong" )
    public void setLong( Long value ) {
        RootPanel.get().add( new Label( "long: " + Long.toString( value ) ) );
    }

    @DataField( name = "methodBoolean" )
    public void setLong( Boolean value ) {
        RootPanel.get().add( new Label( "boolean: " + Boolean.toString( value ) ) );
    }

    @DataField( name = "childItem", showEvent = ShowChild.class, changedEvent = ChildChanged.class )
    public HasWidgets getChildContainer() {
        FlowPanel panel = new FlowPanel();
        RootPanel.get().add( panel );
        return panel;
    }
}
