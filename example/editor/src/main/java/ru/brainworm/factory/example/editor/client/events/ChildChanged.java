package ru.brainworm.factory.example.editor.client.events;

import com.google.gwt.user.client.ui.HasWidgets;
import ru.brainworm.factory.example.editor.client.DTO;

/**
 * Событие изменения
 */
public class ChildChanged {
    public ChildChanged( HasWidgets parent, DTO.ChildItem childItem ) {
        this.parent = parent;
        this.value = childItem;
    }

    public HasWidgets parent;
    public DTO.ChildItem value;
}
