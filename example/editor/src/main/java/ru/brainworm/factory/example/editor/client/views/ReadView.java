package ru.brainworm.factory.example.editor.client.views;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import ru.brainworm.factory.example.editor.client.activity.TestActivity;
import ru.brainworm.factory.generator.editor.client.annotations.DataField;

import java.util.Date;

/**
 * Класс вьюхи с аннотированными методами
 */
public class ReadView {

    public void setActivity( TestActivity activity ) {
        this.activity = activity;
    }

    public void show() {
        text = new TextBox();
        RootPanel.get().add( text );

        final Button button = new Button( "Показать" );
        button.addClickHandler( new ClickHandler() {
            @Override
            public void onClick( ClickEvent event ) {
                activity.onShowClicked();
            }
        } );
        RootPanel.get().add( button );
    }

    @DataField( name = "fieldString" )
    public String getName() {
        return text.getText();
    }

    @DataField( name = "methodDate" )
    public Date getDate() {
        return new Date();
    }

    TestActivity activity;
    TextBox text;
}
