package ru.brainworm.factory.example.editor.client.activity;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.inject.Inject;
import ru.brainworm.factory.example.editor.client.DTO;
import ru.brainworm.factory.example.editor.client.events.ChildChanged;
import ru.brainworm.factory.example.editor.client.events.ShowChild;
import ru.brainworm.factory.example.editor.client.views.ChildView;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;
import ru.brainworm.factory.generator.editor.client.annotations.DataReader;
import ru.brainworm.factory.generator.editor.client.annotations.DataWriter;
import ru.brainworm.factory.generator.injector.client.PostConstruct;

/**
 * Дочерняя активность
 */
public abstract class ChildActivity implements Activity {

    @PostConstruct
    public void onInit() {
        view.setActivity( this );
    }

    @Event
    public void onEvent( ShowChild showEvent ) {
        parent = showEvent.parent;
        dto = showEvent.childItem;

        view.setParent( showEvent.parent );
        fillView( showEvent.childItem, view );
    }

    public void onTextChanged() {
        fillDto( view, dto );
        fireEvent( new ChildChanged( parent, dto ) );
    }

    @DataReader
    public abstract void fillView( DTO.ChildItem dto, ChildView view );

    @DataWriter
    public abstract void fillDto( ChildView view, DTO.ChildItem dto );

    @Inject
    ChildView view;

    HasWidgets parent;
    DTO.ChildItem dto;
}
