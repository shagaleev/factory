package ru.brainworm.factory.example.editor.client;

import java.util.Date;

/**
 * Пример Data Transfer Object
 */
public class DTO {

    public static class ChildItem {
        public ChildItem( Integer counter ) {
            this.counter = counter;
        }

        public Integer counter;
    }

    public DTO( String fieldString, Integer fieldInteger, Date date, Long fieldLong, Boolean fieldBoolean ) {
        this.fieldString = fieldString;
        this.fieldInteger = fieldInteger;
        this.date = date;
        this.fieldLong = fieldLong;
        this.fieldBoolean = fieldBoolean;
    }

    public Date getMethodDate() { return date; }
    public void setMethodDate( Date date ) { this.date = date; }

    public Long getMethodLong() { return fieldLong; }

    public Boolean isMethodBoolean() { return fieldBoolean; }

    public String fieldString;
    public Integer fieldInteger;

    private Date date;
    private Long fieldLong;
    private Boolean fieldBoolean;
    public ChildItem childItem;
}
