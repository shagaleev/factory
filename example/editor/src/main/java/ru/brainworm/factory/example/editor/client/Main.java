package ru.brainworm.factory.example.editor.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import ru.brainworm.factory.example.editor.client.events.ShowEvent;
import ru.brainworm.factory.example.editor.client.factory.ClientFactory;

import java.util.Date;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Main implements EntryPoint {

    public void onModuleLoad() {
        ClientFactory factory = GWT.create( ClientFactory.class );

        ShowEvent event = new ShowEvent( new DTO( "Vasya", 8, new Date(), 10L, true ) );
        event.dto.childItem = new DTO.ChildItem( 10 );
        factory.getActivity().fireEvent( event );
    }
}
