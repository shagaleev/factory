package ru.brainworm.factory.example.editor.client.events;

import ru.brainworm.factory.example.editor.client.DTO;

/**
 * Событие отображения
 */
public class ShowEvent {
    public ShowEvent( DTO dto ) {
        this.dto = dto;
    }

    public DTO dto;
}
