package ru.brainworm.factory.example.editor.client.activity;

import com.google.gwt.user.client.ui.RootPanel;
import com.google.inject.Inject;
import ru.brainworm.factory.example.editor.client.DTO;
import ru.brainworm.factory.example.editor.client.views.ReadView;
import ru.brainworm.factory.example.editor.client.events.ShowEvent;
import ru.brainworm.factory.example.editor.client.views.WriteView;
import ru.brainworm.factory.generator.activity.client.activity.Activity;
import ru.brainworm.factory.generator.activity.client.annotations.Event;
import ru.brainworm.factory.generator.editor.client.annotations.DataReader;
import ru.brainworm.factory.generator.editor.client.annotations.DataWriter;
import ru.brainworm.factory.generator.injector.client.PostConstruct;

/**
 * Активность
 */
public abstract class TestActivity implements Activity {

    @PostConstruct
    public void onInit() {
        readView.setActivity( this );
    }

    @Event
    public void onEvent( ShowEvent showEvent ) {
        dto = showEvent.dto;
        placeWidgets();
    }

    public void onShowClicked() {
        fillDto( readView, dto );
        placeWidgets();
    }

    private void placeWidgets() {
        RootPanel.get().clear();
        readView.show();
        fillView( dto, writeView );
    }

    @DataReader
    public abstract void fillView( DTO dto, WriteView writeView );

    @DataWriter
    public abstract void fillDto( ReadView readView, DTO dto );

    @DataWriter
    public abstract void fillDto( WriteView readView, DTO dto );

    @Inject
    ReadView readView;

    @Inject
    WriteView writeView;

    DTO dto;
}
