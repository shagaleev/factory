package ru.brainworm.factory.example.editor.client.views;

import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import ru.brainworm.factory.example.editor.client.activity.ChildActivity;
import ru.brainworm.factory.generator.editor.client.annotations.DataField;

/**
 * Класс дочерней вьюхи
 */
public class ChildView {

    public void setParent( HasWidgets parent ) {
        this.parent = parent;
    }

    public void setActivity( ChildActivity activity ) {
        this.activity = activity;
    }

    @DataField( name = "counter" )
    public void setCounter( Integer value ) {
        parent.clear();

        HorizontalPanel panel = new HorizontalPanel();
        panel.add( new Label( "counter: " ) );

        box = new TextBox();
        box.setText( Integer.toString( value ) );
        box.addKeyDownHandler( new KeyDownHandler() {
            @Override
            public void onKeyDown( KeyDownEvent event ) {
                activity.onTextChanged();
            }
        } );
        panel.add( box );

        parent.add( panel );
    }

    @DataField( name = "counter" )
    public Integer getCounter() {
        try {
            return Integer.parseInt( box.getText() );
        }
        catch ( NumberFormatException e ) {
            return null;
        }
    }

    HasWidgets parent;
    TextBox box;

    ChildActivity activity;
}
